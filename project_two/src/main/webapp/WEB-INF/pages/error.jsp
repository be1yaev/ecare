<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 13.09.2014
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Error
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%-- Error message block --%>
                <c:if test="${!empty errorReason}">
                    <p class="bg-danger" style="padding: 20px;">
                        ${errorReason}
                    </p>
                </c:if>
                <c:if test="${empty errorReason}">
                    <p class="bg-danger" style="padding: 20px;">
                        Unknown error on page
                    </p>
                </c:if>
                <p style="font-size: 30px; margin-top: 40px;">Error</p>
                <p style="font-size: 70px;  margin: 20px;">:(</p>
            </div>
        </div>
    </div>
</body>
</html>
