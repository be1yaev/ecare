<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 25.07.2014
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:url value="/user/login" var="loginUrl" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Welcome to eCare</title>
    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%@ include file="/WEB-INF/pages/messages.jsp" %>
                <h2 style="margin: 100px;">
                    <%--Hello! This is first page for eCare project.--%>
                </h2>
            </div>
        </div>
    </div>
</body>
</html>
