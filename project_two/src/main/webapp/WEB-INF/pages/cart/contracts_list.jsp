<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 29.09.2014
  Time: 21:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Shopping cart
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/cart/contracts_list_page.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="#newContracts" data-toggle="tab">New contracts (${fn:length(newContractsList)})</a></li>
                <li><a href="#changedContracts" data-toggle="tab">Changed contracts (${fn:length(changedContractsList)})</a></li>
                <li><a href="#removedContracts" data-toggle="tab">Removed contracts (${fn:length(removedContractsList)})</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="newContracts">
                    <h3 style="margin-top: 20px">New contracts:</h3>
                    <table id="newContractsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Client</th>
                                <th>Phone number</th>
                                <th>Tariff</th>
                                <th>Blocked by client</th>
                                <th>Blocked by manager</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${newContractsList}" var="contract">
                                <tr>
                                    <td>${contract.user.surname} ${contract.user.firstname} ${contract.user.patronymic}</td>
                                    <td>${contract.phoneNumber}</td>
                                    <td>${contract.tariff.title}</td>
                                    <td>${contract.blockedByClient}</td>
                                    <td>${contract.blockedByManager}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                           href="${pageContext.request.contextPath}/cart/save/new/${contract.phoneNumber}">
                                            Apply
                                        </a>
                                        <a class="btn btn-warning btn-sm"
                                           href="${pageContext.request.contextPath}/cart/edit/new/${contract.phoneNumber}">
                                            Edit
                                        </a>
                                        <a class="btn btn-danger btn-sm"
                                           href="${pageContext.request.contextPath}/cart/remove/new/${contract.phoneNumber}">
                                            Remove from cart
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="changedContracts">
                    <h3 style="margin-top: 20px">Changed contracts:</h3>
                    <table id="changedContractsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Client</th>
                            <th>Phone number</th>
                            <th>Tariff</th>
                            <th>Blocked by client</th>
                            <th>Blocked by manager</th>
                            <th>Operations</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${changedContractsList}" var="contract">
                            <tr>
                                <td>${contract.id}</td>
                                <td>${contract.user.surname} ${contract.user.firstname} ${contract.user.patronymic}</td>
                                <td>${contract.phoneNumber}</td>
                                <td>${contract.tariff.title}</td>
                                <td>${contract.blockedByClient}</td>
                                <td>${contract.blockedByManager}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm"
                                       href="${pageContext.request.contextPath}/cart/save/changed/${contract.id}">
                                        Apply
                                    </a>
                                    <a class="btn btn-warning btn-sm"
                                       href="${pageContext.request.contextPath}/cart/edit/changed/${contract.id}">
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm"
                                       href="${pageContext.request.contextPath}/cart/remove/changed/${contract.id}">
                                        Remove from cart
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="removedContracts">
                    <h3 style="margin-top: 20px">Removed contracts:</h3>
                    <table id="removedContractsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Client</th>
                            <th>Phone number</th>
                            <th>Tariff</th>
                            <th>Blocked by client</th>
                            <th>Blocked by manager</th>
                            <th>Operations</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${removedContractsList}" var="contract">
                            <tr>
                                <td>${contract.id}</td>
                                <td>${contract.user.surname} ${contract.user.firstname} ${contract.user.patronymic}</td>
                                <td>${contract.phoneNumber}</td>
                                <td>${contract.tariff.title}</td>
                                <td>${contract.blockedByClient}</td>
                                <td>${contract.blockedByManager}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm"
                                       href="${pageContext.request.contextPath}/cart/save/removed/${contract.id}">
                                        Apply removing
                                    </a>
                                    <a class="btn btn-danger btn-sm"
                                       href="${pageContext.request.contextPath}/cart/remove/removed/${contract.id}">
                                        Remove from cart
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
