<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Editing contract information
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>


            <div class="control-buttons">
                <a class="btn btn-primary"
                   href="${pageContext.request.contextPath}/cart/add_contract">
                    Save into Cart
                </a>
                <a class="btn btn-default"
                   href="${pageContext.request.contextPath}/contract/edit/cancel">
                    Cancel all changes
                </a>
            </div>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                    <li><a href="${pageContext.request.contextPath}/contract/edit/select_user">Client</a></li>
                </sec:authorize>
                <li class="active"><a href="${pageContext.request.contextPath}/contract/edit/info">Information</a></li>
                <li><a href="${pageContext.request.contextPath}/contract/edit/select_tariff">Tariff</a></li>
                <li><a href="${pageContext.request.contextPath}/contract/edit/select_options">Connected options</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <h3>Complete the fields:</h3>
                <div class="editing-form">
                    <form:form method="post" action="${pageContext.request.contextPath}/contract/edit/info/save" commandName="contract">
                        <form:errors path="*" cssClass="bg-danger" element="p"/>
                        <form:hidden path="id" cssErrorClass="error"/>
                        <div class="form-group">
                            <label for="phoneNumber">Phone number:</label>
                            <form:input path="phoneNumber" cssClass="form-control" required="true" cssErrorClass="form-control has-error"/>
                        </div>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                            <div style="text-align: center;">
                                <label>
                                    <form:checkbox path="blockedByClient" cssErrorClass="form-control has-error" disabled="true"/>
                                    Blocked by client:
                                </label>
                            </div>
                            <div style="text-align: center;">
                                <label>
                                    <form:checkbox path="blockedByManager" cssErrorClass="form-control has-error"/>
                                    Blocked by manager:
                                </label>
                            </div>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_CLIENT')">
                            <div style="text-align: center;">
                                <label>
                                    <form:checkbox path="blockedByClient" cssErrorClass="form-control has-error"/>
                                    Blocked by client:
                                </label>
                            </div>
                            <div style="text-align: center;">
                                <label>
                                    <form:checkbox path="blockedByManager" cssErrorClass="form-control has-error"  disabled="true"/>
                                    Blocked by manager:
                                </label>
                            </div>
                        </sec:authorize>
                        <div class="control-buttons">
                            <input type="submit" class="btn btn-primary" value="Save"/>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
