<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Editing dependent and incompatible options
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/contract/select_user_page.js"></script>
</head>
<body>

<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>


            <div class="control-buttons">
                <a class="btn btn-primary"
                   href="${pageContext.request.contextPath}/cart/add_contract">
                    Save into Cart
                </a>
                <a class="btn btn-default"
                   href="${pageContext.request.contextPath}/contract/edit/cancel">
                    Cancel all changes
                </a>
            </div>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                    <li class="active"><a href="${pageContext.request.contextPath}/contract/edit/select_user">Client</a></li>
                </sec:authorize>
                <li><a href="${pageContext.request.contextPath}/contract/edit/info">Information</a></li>
                <li><a href="${pageContext.request.contextPath}/contract/edit/select_tariff">Tariff</a></li>
                <li><a href="${pageContext.request.contextPath}/contract/edit/select_options">Connected options</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <c:choose>
                    <c:when test="${!empty currentUser}">
                        <h3 style="margin-top: 20px">
                            <small>
                                Current client of this contract:
                            </small>
                            ${currentUser.surname} ${currentUser.firstname} ${currentUser.patronymic}
                        </h3>
                    </c:when>
                </c:choose>

                <h3 style="margin-top: 20px">All users list:</h3>
                <table id="allUsersTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>E-mail</th>
                            <th>Surname</th>
                            <th>Firstname</th>
                            <th>Patronymic</th>
                            <th>Passport number</th>
                            <%--<th>Passport description</th>--%>
                            <th>Country</th>
                            <th>City</th>
                            <%--<th>Address</th>--%>
                            <th>Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${usersList}" var="user">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.email}</td>
                            <td>${user.surname}</td>
                            <td>${user.firstname}</td>
                            <td>${user.patronymic}</td>
                            <td>${user.passportNumber}</td>
                            <%--<td>${user.passportDescription}</td>--%>
                            <td>${user.country}</td>
                            <td>${user.city}</td>
                            <%--<td>${user.address}</td>--%>
                            <td>
                                <a type="button" class="btn btn-primary btn-sm"
                                   href="${pageContext.request.contextPath}/contract/edit/select_user/${user.id}">
                                    Select
                                </a>
                                <a class="btn btn-info btn-sm"
                                   href="${pageContext.request.contextPath}/user/info/${user.id}">
                                    info
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
