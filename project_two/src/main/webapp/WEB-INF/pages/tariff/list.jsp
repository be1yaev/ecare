<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 29.09.2014
  Time: 1:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        List of tariffs
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/tariff/tariff_list_page.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <h3 style="margin-top: 20px">List of tariffs:</h3>
            <table id="allTariffsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th style="min-width: 160px;">Operations</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${tariffsList}" var="tariff">
                        <tr>
                            <td>${tariff.id}</td>
                            <td>${tariff.title}</td>
                            <td>${tariff.description}</td>
                            <td>
                                <a class="btn btn-info btn-sm"
                                   href="${pageContext.request.contextPath}/tariff/info/${tariff.id}">
                                    info
                                </a>
                                <a class="btn btn-warning btn-sm"
                                   href="${pageContext.request.contextPath}/tariff/edit/${tariff.id}/info">
                                    edit
                                </a>
                                <a class="btn btn-danger btn-sm"
                                   href="${pageContext.request.contextPath}/tariff/remove/${tariff.id}">
                                    remove
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>

