<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Editing possible options
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/tariff/editing_options.js"></script>
</head>
<body>

<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <div class="control-buttons">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/tariff/info/${currentTariff.id}">
                    Tariff info
                </a>
                <a class="btn btn-default"
                   href="${pageContext.request.contextPath}/tariff/remove/${currentTariff.id}">
                    Remove tariff
                </a>
            </div>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li><a href="${pageContext.request.contextPath}/tariff/edit/${currentTariff.id}/info">Information</a></li>
                <li class="active"><a href="${pageContext.request.contextPath}/tariff/edit/${currentTariff.id}/options">Possible options</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="item">
                                <h3 style="margin-top: 20px">All options list:</h3>
                                <table id="allOptionsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <%--<th>Price</th>--%>
                                            <%--<th>Connection cost</th>--%>
                                            <th>Operations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${allOptionsList}" var="option">
                                            <tr>
                                                <td>${option.id}</td>
                                                <td>${option.title}</td>
                                                <td>${option.description}</td>
                                                <%--<td>${option.price}</td>--%>
                                                <%--<td>${option.connectionCost}</td>--%>
                                                <td>
                                                    <a class="btn btn-info btn-sm"
                                                       href="${pageContext.request.contextPath}/option/info/${option.id}">
                                                        info
                                                    </a>
                                                    <a class="btn btn-default btn-sm"
                                                            href="${pageContext.request.contextPath}/tariff/edit/${currentTariff.id}/possible_options/add/${option.id}">
                                                        to possible options
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="item">
                                <h3 style="margin-top: 20px">Possible options list:</h3>
                                <table id="possibleOptionsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <%--<th>Price</th>--%>
                                            <%--<th>Connection cost</th>--%>
                                            <th>Operations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${currentTariff.possibleOptions}" var="option">
                                            <tr>
                                                <td>${option.id}</td>
                                                <td>${option.title}</td>
                                                <td>${option.description}</td>
                                                <%--<td>${option.price}</td>--%>
                                                <%--<td>${option.connectionCost}</td>--%>
                                                <td>
                                                    <a class="btn btn-info btn-sm"
                                                       href="${pageContext.request.contextPath}/option/info/${option.id}">
                                                        info
                                                    </a>
                                                    <a class="btn btn-default btn-sm"
                                                       href="${pageContext.request.contextPath}/tariff/edit/${currentTariff.id}/possible_options/remove/${option.id}">
                                                        remove
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>