<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Tariff information
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/tariff/tariff_info.js"></script>
</head>
<body>

<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                <div class="control-buttons">
                    <a class="btn btn-warning"
                       href="${pageContext.request.contextPath}/tariff/edit/${currentTariff.id}/info">
                        Edit tariff
                    </a>
                    <a class="btn btn-default"
                       href="${pageContext.request.contextPath}/tariff/remove/${currentTariff.id}">
                        Remove tariff
                    </a>
                </div>
            </sec:authorize>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="#info" data-toggle="tab">Information</a></li>
                <li><a href="#options" data-toggle="tab">Possible options</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="info">
                    <h3>Information:</h3>
                    <table class="table table-bordered" style="max-width: 350px;"align="center">
                        <tr>
                            <td>Id:</td>
                            <td>${currentTariff.id}</td>
                        </tr>
                        <tr>
                            <td>Title:</td>
                            <td>${currentTariff.title}</td>
                        </tr>
                        <tr>
                            <td>Description:</td>
                            <td>${currentTariff.description}</td>
                        </tr>
                    </table>
                </div>
                <div class="tab-pane" id="options">
                    <h3 style="margin-top: 20px">Possible options list:</h3>
                    <table id="possibleOptionsTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Connection cost</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${currentTariff.possibleOptions}" var="option">
                            <tr>
                                <td>${option.id}</td>
                                <td>${option.title}</td>
                                <td>${option.description}</td>
                                <td>${option.price}</td>
                                <td>${option.connectionCost}</td>
                                <td>
                                    <a class="btn btn-info btn-sm"
                                       href="${pageContext.request.contextPath}/option/info/${option.id}">
                                        info
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
