<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 14.09.2014
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@page session="true"%>

<c:url value="/user/login" var="loginUrl" />
<c:url value="/j_spring_security_logout" var="logoutUrl" />
<c:url value="/user/registration" var="registrationUrl" />
<c:url value="/j_spring_security_check" var="securityCheckUrl" />

<%--<div id="popoverDiv" rel="popover"--%>
     <%--data-content="Message: ${message}<br/>--%>
     <%--Error: ${errorMessage}"--%>
     <%--data-original-title='<button onclick="closePopoverDiv();">Close</button>'>--%>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">1</span>
                <span class="icon-bar">2</span>
                <span class="icon-bar">3</span>
            </button>
            <a href="${pageContext.request.contextPath}" class="navbar-brand">eCare</a>
            <%--<a href="#" class="navbar-brand" href="#">eCare</a>--%>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <%--<li><a href="#">Welcome</a></li>--%>
                <sec:authorize access="isAuthenticated()">
                    <li>
                        <a
                            <sec:authorize access="hasAnyRole('ROLE_CLIENT', 'ROLE_MANAGER')">
                                href="${pageContext.request.contextPath}/user/info"
                            </sec:authorize>>
                            ${pageContext.request.userPrincipal.name}
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a href="${pageContext.request.contextPath}${logoutUrl}">
                            Logout
                        </a>
                    </li>
                </sec:authorize>
                <sec:authorize access="isAnonymous()">
                    <c:if test="${not fn:containsIgnoreCase(pageContext.request.requestURI, 'user/login')}">
                        <form class="navbar-form navbar-right" role="form" method="post" action="${securityCheckUrl}">
                            <div class="form-group">
                                <input type="text" name="username" placeholder="E-mail" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Password" class="form-control input-small" required>
                            </div>
                            <button type="submit" class="btn btn-success">Log In</button>
                        </form>
                        <li class="divider-vertical"></li>
                        <li><a href="${pageContext.request.contextPath}/user/registration">Sign Up</a></li>
                    </c:if>
                </sec:authorize>
            </ul>
            <%--<form class="navbar-form navbar-right">--%>
                <%--<input type="text" class="form-control" placeholder="Search...">--%>
            <%--</form>--%>
        </div>
    </div>
</div>
<%--</div>--%>

<%--<div style="background: #ddd">--%>
    <%--&lt;%&ndash;<c:if test="${pageContext.request.userPrincipal}">&ndash;%&gt;--%>
        <%--&lt;%&ndash;${pageContext.request.userPrincipal.name}&ndash;%&gt;--%>
    <%--&lt;%&ndash;</c:if>&ndash;%&gt;--%>
    <%--<sec:authorize access="isRememberMe()">--%>
        <%--RememberMe:--%>
        <%--${pageContext.request.userPrincipal.principal}--%>
    <%--</sec:authorize>--%>
    <%--<br>--%>
    <%--<sec:authorize access="isFullyAuthenticated()">--%>
        <%--FullyAuthenticated:--%>
        <%--${pageContext.request.userPrincipal.name}--%>
    <%--</sec:authorize>--%>
    <%--<br>--%>
    <%--<br>--%>
    <%--<a href="${loginUrl}">Login</a>--%>
    <%--<br>--%>
    <%--<a href="${logoutUrl}">Logout</a>--%>
    <%--<br>--%>
    <%--<a href="${registrationUrl}">Registration</a>--%>
<%--</div>--%>
