<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 30.09.2014
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUser">Me</a>
            </h4>
        </div>
        <%--<div id="collapseUser" class="panel-collapse collapse in">--%>
        <div id="collapseUser" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="sidebar-offcanvas" role="navigation">
                    <div class="list-group nav nav-tabs nav-stacked">
                        <a href="${pageContext.request.contextPath}/user/info" class="list-group-item">Your info</a>
                        <a href="${pageContext.request.contextPath}/user/edit" class="list-group-item">Edit your info</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseContract">Contracts</a>
            </h4>
        </div>
        <div id="collapseContract" class="panel-collapse collapse">
            <div class="panel-body">
                <a href="${pageContext.request.contextPath}/contract/list" class="list-group-item">Your contracts</a>
                <a href="${pageContext.request.contextPath}/contract/create" class="list-group-item">Create new</a>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTariff">Tariffs</a>
            </h4>
        </div>
        <div id="collapseTariff" class="panel-collapse collapse">
            <div class="panel-body">\
                <a href="${pageContext.request.contextPath}/tariff/list" class="list-group-item">Our tariffs</a>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOption">Options</a>
            </h4>
        </div>
        <div id="collapseOption" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="sidebar-offcanvas" role="navigation">
                    <div class="list-group nav nav-tabs nav-stacked">
                        <a href="${pageContext.request.contextPath}/option/list" class="list-group-item">Our options</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
