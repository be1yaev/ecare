<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 14.09.2014
  Time: 23:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<div class="sidebar-offcanvas" id="sidebar" role="navigation">
    <sec:authorize access="isAnonymous()">
        <%@ include file="/WEB-INF/pages/menu/guest_menu.jsp" %>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_CLIENT')">
        <%@ include file="/WEB-INF/pages/menu/client_menu.jsp" %>
    </sec:authorize>
    <sec:authorize access="hasAnyRole('ROLE_MANAGER', 'ROLE_ADMIN')">
        <%@ include file="/WEB-INF/pages/menu/manager_menu.jsp" %>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <%@ include file="/WEB-INF/pages/menu/cart.jsp" %>
    </sec:authorize>
</div>
