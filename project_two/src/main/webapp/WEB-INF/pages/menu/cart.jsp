<%@ page import="com.tsystems.ecare.util.Constants" %>
<%@ page import="com.tsystems.ecare.util.ShoppingCart" %>
<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 30.09.2014
  Time: 16:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<div class="shoppingCart">
    <h4>Shopping Cart</h4>

    <div style="margin-top: 20px;">
        <%
            ShoppingCart cart;
            cart = (ShoppingCart) session.getAttribute(Constants.SESSION_ATTR_SHOPPING_CART);

            out.println("Contracts:");
            out.println("<br>New: " + cart.getNewContracts().size());
            out.println("<br>Changed: " + cart.getChangedContracts().size());
            out.println("<br>Removed: " + cart.getRemovedContracts().size());
        %>
    </div>

    <a class="btn btn-default btn-sm" style="margin-top: 30px;"
       href="${pageContext.request.contextPath}/cart/">
        Show
    </a>
</div>
