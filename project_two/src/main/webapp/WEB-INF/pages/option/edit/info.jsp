<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        Editing option information
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <div class="control-buttons">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/option/info/${currentOption.id}">
                    Option info
                </a>
                <a class="btn btn-default"
                   href="${pageContext.request.contextPath}/option/remove/${currentOption.id}">
                    Remove option
                </a>
            </div>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="${pageContext.request.contextPath}/option/edit/${currentOption.id}/info">Information</a></li>
                <li><a href="${pageContext.request.contextPath}/option/edit/${currentOption.id}/options">Dependent and incompatible options</a></li>
                <li><a href="${pageContext.request.contextPath}/option/edit/${currentOption.id}/tariffs">Possible tariffs</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <h3>Complete the fields:</h3>
                <div class="editing-form">
                    <form:form method="post" action="${pageContext.request.contextPath}/option/update" commandName="currentOption">
                        <form:errors path="*" cssClass="bg-danger" element="p"/>
                        <form:hidden path="id" cssErrorClass="error"/>
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <form:input path="title" cssClass="form-control" required="true" cssErrorClass="form-control has-error"/>
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <form:textarea path="description" cssClass="form-control" required="true" rows="5" cols="30"  cssErrorClass="form-control has-error"/>
                        </div>
                        <div class="form-group">
                            <label for="price">Price ($, in month):</label>
                            <form:input path="price" cssClass="form-control" required="true" cssErrorClass="form-control has-error"/>
                        </div>
                        <div class="form-group">
                            <label for="connectionCost">Connection cost ($):</label>
                            <form:input path="connectionCost" required="true" cssClass="form-control" cssErrorClass="form-control has-error"/>
                        </div>
                        <div class="control-buttons">
                            <input type="submit" class="btn btn-primary" value="Save"/>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
