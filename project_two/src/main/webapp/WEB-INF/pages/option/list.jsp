<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 27.09.2014
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        List of options
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/option/options_list_page.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
            <%@ include file="/WEB-INF/pages/messages.jsp" %>

            <h3 style="margin-top: 20px">List of options:</h3>
            <table id="allOptionsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Connection cost</th>
                        <th>Operations</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${optionsList}" var="option">
                        <tr>
                            <td>${option.id}</td>
                            <td>${option.title}</td>
                            <td>${option.description}</td>
                            <td>${option.price}</td>
                            <td>${option.connectionCost}</td>
                            <td>
                                <a class="btn btn-info btn-sm"
                                   href="${pageContext.request.contextPath}/option/info/${option.id}">
                                    info
                                </a>
                                <a class="btn btn-warning btn-sm"
                                   href="${pageContext.request.contextPath}/option/edit/${option.id}/info">
                                    edit
                                </a>
                                <a class="btn btn-danger btn-sm"
                                   href="${pageContext.request.contextPath}/option/remove/${option.id}">
                                    remove
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
