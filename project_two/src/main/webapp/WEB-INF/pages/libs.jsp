<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 15.09.2014
  Time: 2:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<script type="text/javascript">
    window.contextPath = "${pageContext.request.contextPath}";
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.dataTables.css" media="all">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css" media="all">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/changed_bootstrap.css" media="all">
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>--%>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.autocomplete.min.js"></script>--%>

<%--<script type="text/javascript">--%>
    <%--function closePopoverDiv() {--%>
        <%--$('#popoverDiv').popover('hide')--%>
    <%--}--%>

    <%--$(document).ready(function(){--%>
        <%--$("#popoverDiv").popover({--%>
            <%--placement : 'bottom',--%>
            <%--trigger: 'manual',--%>
            <%--html : true--%>
        <%--});--%>

        <%--$('#popoverDiv').popover('show')--%>
    <%--});--%>
<%--</script>--%>

<%--<script type="text/javascript">--%>
    <%--function test() {--%>
        <%--$.ajax({--%>
            <%--url: '${pageContext.request.contextPath}/user/ajaxtest',--%>
            <%--success: function (data) {--%>
                <%--alert("");--%>
            <%--}--%>
        <%--});--%>
    <%--}--%>
<%--</script>--%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/test.css" media="all">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messagesStyle.css" media="all">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/dataBlock.css" media="all">
<style type="text/css">
    td, th {
        padding: 7px 10px 5px 10px;
    }
</style>
