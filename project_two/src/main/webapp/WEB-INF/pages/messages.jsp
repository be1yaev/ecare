<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 26.09.2014
  Time: 23:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>
    <%-- Success message block --%>
    <c:if test="${!empty successMessage}">
        <p class="bg-success" style="padding: 20px;">
            ${successMessage}
        </p>
    </c:if>

    <%-- Information message block --%>
        <c:if test="${!empty infoMessage}">
            <p class="bg-info" style="padding: 20px;">
                ${infoMessage}
            </p>
        </c:if>

    <%-- Error message block --%>
    <c:if test="${!empty errorMessage}">
        <p class="bg-danger" style="padding: 20px;">
            ${errorMessage}
        </p>
    </c:if>
</div>
