<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 13.09.2014
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        User information
    </title>
    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/user/users_info_page.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%@ include file="/WEB-INF/pages/messages.jsp" %>

                    <div class="control-buttons">
                        <a class="btn btn-warning"
                           href="${pageContext.request.contextPath}/user/edit/${user.id}">
                            Edit info
                        </a>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                            <a class="btn btn-default"
                               href="${pageContext.request.contextPath}/user/remove/${user.id}">
                                Remove user
                            </a>
                        </sec:authorize>
                    </div>

                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#info" data-toggle="tab">Information</a></li>
                    <li><a href="#contracts" data-toggle="tab">Contracts</a></li>
                </ul>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="info">
                        <h3>Information:</h3>
                        <table class="table table-bordered" style="max-width: 350px;"align="center">
                            <tr>
                                <td>Id</td>
                                <td>${user.id}</td>
                            </tr>
                            <tr>
                                <td>E-mail</td>
                                <td>${user.email}</td>
                            </tr>
                            <tr>
                                <td>Surname</td>
                                <td>${user.surname}</td>
                            </tr>
                            <tr>
                                <td>Firstname</td>
                                <td>${user.firstname}</td>
                            </tr>
                            <tr>
                                <td>Patronymic</td>
                                <td>${user.patronymic}</td>
                            </tr>
                            <tr>
                                <td>Passport number</td>
                                <td>${user.passportNumber}</td>
                            </tr>
                            <tr>
                                <td>Passport description</td>
                                <td>${user.passportDescription}</td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td>${user.country}</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>${user.city}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>${user.address}</td>
                            </tr>
                            <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                                <tr>
                                    <td>Role</td>
                                    <td>${user.role}</td>
                                </tr>
                            </sec:authorize>
                        </table>
                    </div>
                    <div class="tab-pane" id="contracts">
                        <h3 style="margin-top: 20px">List of contracts:</h3>
                        <table id="allContractsTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                            <tr>
                                <th>Id</th>
                                <th>Phone number</th>
                                <th>Tariff</th>
                                <th>Status</th>
                                <th>Operations</th>
                            </tr>
                            <c:forEach items="${user.contracts}" var="contract">
                                <tr>
                                    <td>${contract.id}</td>
                                    <td>${contract.phoneNumber}</td>
                                    <td>${contract.tariff.title}</td>
                                    <td>${contract.blockedByClient} ${contract.blockedByManager}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm"
                                           href="${pageContext.request.contextPath}/contract/info/${contract.id}">
                                            info
                                        </a>
                                        <a class="btn btn-warning btn-sm"
                                           href="${pageContext.request.contextPath}/contract/edit/${contract.id}">
                                            edit
                                        </a>
                                        <a class="btn btn-danger btn-sm"
                                           href="${pageContext.request.contextPath}/cart/remove_contract/${contract.id}">
                                            remove
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
