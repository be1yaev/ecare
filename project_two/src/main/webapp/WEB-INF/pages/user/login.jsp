<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 06.09.2014
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>
<head>
    <title>Login Form</title>
    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>
    <%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%@ include file="/WEB-INF/pages/messages.jsp" %>

                <!--Body content-->
                <%--<h2>Login Here</h2>--%>
                <%--<div class="container">--%>
                <form class="form-signin" role="form" method="post" action="${securityCheckUrl}">
                    <h2 class="form-signin-heading">Please Log In
                        <small>
                            <br><br>
                            or
                            <a href="/eCare/user/registration">Sign Up</a>
                            <br><br>
                        </small>
                    </h2>
                    <input type="email" name="username" class="form-control" placeholder="E-mail address" required autofocus>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    <%--<label class="checkbox">--%>
                        <%--<input type="checkbox" value="remember-me"> Remember me--%>
                    <%--</label>--%>
                    <button class="btn btn-lg btn-primary btn-block" type="submit" style="margin-top: 20px;">Log In</button>
                </form>
                <%--</div>--%>
            </div>
        </div>
    </div>
</body>
</html>
