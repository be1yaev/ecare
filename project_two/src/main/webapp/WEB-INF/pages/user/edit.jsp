<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 13.09.2014
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>User information editing</title>
    <%@ include file="/WEB-INF/pages/libs.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%@ include file="/WEB-INF/pages/messages.jsp" %>

                <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                    <div class="control-buttons">
                        <a class="btn btn-info"
                           href="${pageContext.request.contextPath}/user/info/${user.id}">
                            User info
                        </a>
                        <a class="btn btn-default"
                           href="${pageContext.request.contextPath}/user/remove/${user.id}">
                            Remove user
                        </a>
                    </div>
                </sec:authorize>

                <%-- Edit form --%>
                <h3 style="margin-top: 20px;">Complete the fields:</h3>
                <div class="editing-form">
                    <form:form method="post" action="${pageContext.request.contextPath}/user/update" commandName="user">
                        <form:errors path="*" cssClass="bg-danger" element="p"/>
                        <form:hidden path="id"/>
                        <div class="form-group">
                            <label for="surname">Surname:</label>
                            <form:input path="surname" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="firstname">Firstname:</label>
                            <form:input path="firstname" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="patronymic">Patronymic:</label>
                            <form:input path="patronymic" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="passportNumber">Passport number:</label>
                            <form:input path="passportNumber" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="passportDescription">Passport description:</label>
                            <form:textarea path="passportDescription" cssClass="form-control" rows="5" cols="30" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="country">Country:</label>
                            <form:input path="country" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="city">City:</label>
                            <form:input path="city" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <form:textarea path="address" cssClass="form-control" rows="5" cols="30" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail:</label>
                            <form:input type="email" path="email" cssClass="form-control" cssErrorClass="form-control has-error" required="true"/>
                        </div>
                        <sec:authorize access="hasRole('ROLE_CLIENT')">
                            <div class="form-group">
                                <label for="passwordConfirmation">Current password:</label>
                                <input type="password" id="passwordConfirmation" name="passwordConfirmation" class="form-control" autocomplete="off" required="true"/>
                            </div>
                        </sec:authorize>
                            <div class="form-group">
                                <label for="password">New password:</label>
                                <form:password path="password" cssClass="form-control" cssErrorClass="form-control has-error"/>
                            </div>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <div class="form-group">
                                <label for="role">Role:</label>
                                <form:select path="role" cssClass="form-control" cssErrorClass="form-control has-error" required="true">
                                    <form:option value="CLIENT" label="Client" />
                                    <form:option value="MANAGER" label="Manager" />
                                </form:select>
                            </div>
                        </sec:authorize>
                        <div class="control-buttons">
                            <input type="submit" class="btn btn-primary" value="Save"/>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                            <input type="button" class="btn btn-danger" onClick="location.href='${pageContext.request.contextPath}/user/remove/${user.id}'" value='Remove user'>
                        </sec:authorize>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
