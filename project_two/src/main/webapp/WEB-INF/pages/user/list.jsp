<%--
  Created by IntelliJ IDEA.
  User: Andrew
  Date: 13.09.2014
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>
        List of users
    </title>

    <%@ include file="/WEB-INF/pages/libs.jsp" %>

    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="${pageContext.request.contextPath}/js/user/users_list_page.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/pages/header.jsp" %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <%@ include file="/WEB-INF/pages/menu/menu.jsp" %>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="position:relative; text-align: center;">
                <%@ include file="/WEB-INF/pages/messages.jsp" %>

                <h3 style="margin-top: 20px">List of users:</h3>
                <table id="allUsersTable" class="table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>E-mail</th>
                            <th>Surname</th>
                            <th>Firstname</th>
                            <th>Patronymic</th>
                            <th>Passport number</th>
                            <%--<th>Passport description</th>--%>
                            <%--<th>Country</th>--%>
                            <%--<th>City</th>--%>
                            <%--<th>Address</th>--%>
                            <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                                <th>Role</th>
                            </sec:authorize>
                            <th style="min-width: 160px;">Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${usersList}" var="user">
                            <tr>
                                <td>${user.id}</td>
                                <td>${user.email}</td>
                                <td>${user.surname}</td>
                                <td>${user.firstname}</td>
                                <td>${user.patronymic}</td>
                                <td>${user.passportNumber}</td>
                                <%--<td>${user.passportDescription}</td>--%>
                                <%--<td>${user.country}</td>--%>
                                <%--<td>${user.city}</td>--%>
                                <%--<td>${user.address}</td>--%>
                                <sec:authorize access="hasAnyRole('ROLE_ADMIN')">
                                    <td>${user.role}</td>
                                </sec:authorize>
                                <td>
                                    <a class="btn btn-info btn-sm"
                                       href="${pageContext.request.contextPath}/user/info/${user.id}">
                                        info
                                    </a>
                                    <a class="btn btn-warning btn-sm"
                                       href="${pageContext.request.contextPath}/user/edit/${user.id}">
                                        edit
                                    </a>
                                    <a class="btn btn-danger btn-sm"
                                       href="${pageContext.request.contextPath}/user/remove/${user.id}">
                                        remove
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
