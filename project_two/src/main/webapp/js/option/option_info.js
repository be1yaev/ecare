$(document).ready(function() {
    $('#tabs').tab();

    $('#possibleTariffsTable').dataTable();
    $('#incompatibleOptionsTable').dataTable();
    $('#dependentOptionsTable').dataTable();
});