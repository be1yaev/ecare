package com.tsystems.ecare.domain;

import com.tsystems.ecare.dto.TariffDto;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="table_tariff")
public class Tariff {
    @Id
    @Column(name="tariff_id")
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "Field \"Tariff title\" can not be empty")
    @Length(min = 1, max = 50, message = "Length of \"Tariff title\" must be between {2} and {1} characters")
    @Column(name = "title", unique = true)
    private String title;

    @NotEmpty(message = "Field \"Tariff description\" can not be empty")
    @Length(min = 1, max = 250, message = "Length of \"Tariff description\" must be between {2} and {1} characters")
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "tariff", cascade = CascadeType.MERGE)
    private List<Contract> contracts;

    @ManyToMany
    @JoinTable(name="relation_tariff_and_possible_options",
            joinColumns=@JoinColumn(name="tariff_id"),
            inverseJoinColumns=@JoinColumn(name="possible_option_id"))
    private List<Option> possibleOptions;

    /**
     * Adding new possible option and connecting tariff with option
     * @param possibleOption new possible option object
     */
    public void addContract(Option possibleOption) {
        if (possibleOption.getPossibleTariffs() == null)
            possibleOption.setPossibleTariffs(new ArrayList<Tariff>());

        possibleOption.getPossibleTariffs().add(this);

        if (possibleOptions == null)
            possibleOptions = new ArrayList<Option>();

        possibleOptions.add(possibleOption);
    }


    public void addPossibleOption(Option possibleOption) {
        if (possibleOptions == null)
            possibleOptions = new ArrayList<Option>();

        if (!possibleOptions.contains(possibleOption))
            possibleOptions.add(possibleOption);

        List<Tariff> possibleTariffsInOption = possibleOption.getPossibleTariffs();
        if (possibleTariffsInOption == null) {
            possibleTariffsInOption = new ArrayList<Tariff>();
        }

        if (!possibleTariffsInOption.contains(this))
            possibleTariffsInOption.add(this);

        possibleOption.setPossibleTariffs(possibleTariffsInOption);
    }

    public void addPossibleOptions(List<Option> options) {
        for (Option o : options) {
            addPossibleOption(o);
        }
    }

    public Tariff() {}

    public Tariff(String title,
                  String description,
                  List<Contract> contracts,
                  List<Option> possibleOptions) {

        this.title = title;
        this.description = description;
        this.contracts = contracts;
        this.possibleOptions = possibleOptions;
    }

    public TariffDto toDTO() {
        return new TariffDto(
                this.id,
                this.title,
                this.description
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public List<Option> getPossibleOptions() {
        return possibleOptions;
    }

    public void setPossibleOptions(List<Option> possibleOptions) {
        this.possibleOptions = possibleOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tariff tariff = (Tariff) o;

        if (contracts != null ? !contracts.equals(tariff.contracts) : tariff.contracts != null) return false;
        if (description != null ? !description.equals(tariff.description) : tariff.description != null) return false;
        if (id != null ? !id.equals(tariff.id) : tariff.id != null) return false;
        if (possibleOptions != null ? !possibleOptions.equals(tariff.possibleOptions) : tariff.possibleOptions != null)
            return false;
        if (title != null ? !title.equals(tariff.title) : tariff.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (contracts != null ? contracts.hashCode() : 0);
        result = 31 * result + (possibleOptions != null ? possibleOptions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", contracts.size=" + (contracts != null ? contracts.size() : 0) +
                ", possibleOptions.size=" + (possibleOptions != null ? possibleOptions.size() : 0) +
                '}';
    }

    public static Tariff getFromDTO(TariffDto tariffDto) {
        return new Tariff(
                tariffDto.getTitle(),
                tariffDto.getDescription(),
                null,
                null
        );
    }

    public void update(TariffDto dto) {
        title = dto.getTitle();
        description = dto.getDescription();
    }

    public void removePossibleOption(Option option) {

        if (possibleOptions == null)
            possibleOptions = new ArrayList<Option>();

        if (possibleOptions.contains(option))
            possibleOptions.remove(option);

        List<Tariff> possibleTariffsInOption = option.getPossibleTariffs();
        if (possibleTariffsInOption == null)
            possibleTariffsInOption = new ArrayList<Tariff>();

        if (possibleTariffsInOption.contains(this))
            possibleTariffsInOption.remove(this);

        option.setPossibleTariffs(possibleTariffsInOption);
    }

    public void removePossibleOptions(List<Option> options) {
        for (Option o : options)
            removePossibleOption(o);
    }
}
