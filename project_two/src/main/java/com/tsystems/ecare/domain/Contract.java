package com.tsystems.ecare.domain;

import com.tsystems.ecare.dto.ContractDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="table_contract")
public class Contract {
    @Id
    @Column(name="contract_id")
    //@Min(value = 1, message = "Incorrect ID, please input numeric value")
    @GeneratedValue
    private Long id;

    @NotNull(message = "Field \"Phone numbere\" can not be empty")
    @Size(min = 5, max = 20, message = "Length of \"Phone number\" must be between {2} and {1} characters")
    @Column(name = "phone_number", unique = true)
    private String phoneNumber;

    //@NotNull(message = "Field \"Tariff\" can not be empty")
    @ManyToOne
    //@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "tariff_id")
    private Tariff tariff;

    @Column(name = "blocked_by_client")
    private Boolean blockedByClient;

    @Column(name = "blocked_by_manager")
    private Boolean blockedByManager;

    //@NotNull(message = "Field \"ClientId\" can not be empty")
    @ManyToOne()
    //@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name="user_id")
    private User client;

    @ManyToMany
    @JoinTable(name="relation_contract_and_options",
            joinColumns=@JoinColumn(name="contract_id"),
            inverseJoinColumns=@JoinColumn(name="option_id"))
    private List<Option> connectedOptions;

    @PrePersist
    /** Setting default value of blocking */
    void preInsert() {
        if (blockedByClient == null)
            blockedByClient = false;
        if (blockedByManager == null)
            blockedByManager = false;
        connectedOptions = new ArrayList<Option>();
    }

    /**
     * Adding new tariff and connecting tariff with contract
     * @param tariff new tariff
     */
    public void addTariff(Tariff tariff) {
        if (tariff.getContracts() == null)
            tariff.setContracts(new ArrayList<Contract>());

        tariff.getContracts().add(this);
        this.setTariff(tariff);
    }

    public void addClient(User user) {
        if (user.getContracts() == null)
            user.setContracts(new ArrayList<Contract>());

        user.getContracts().add(this);
        this.setClient(user);
    }

    public Contract() {}

    public Contract(String phoneNumber,
                    Tariff tariff,
                    Boolean blockedByClient,
                    Boolean blockedByManager,
                    User client,
                    List<Option> connectedOptions) {

        this.phoneNumber = phoneNumber;
        this.tariff = tariff;
        this.blockedByClient = blockedByClient;
        this.blockedByManager = blockedByManager;
        this.client = client;
        this.connectedOptions = connectedOptions;
    }

    public ContractDto toDTO() {
        return new ContractDto(
                this.id,
                this.phoneNumber,
                this.blockedByClient,
                this.blockedByManager
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public Boolean getBlockedByClient() {
        return blockedByClient;
    }

    public void setBlockedByClient(Boolean blockedByClient) {
        this.blockedByClient = blockedByClient;
    }

    public Boolean getBlockedByManager() {
        return blockedByManager;
    }

    public void setBlockedByManager(Boolean blockedByManager) {
        this.blockedByManager = blockedByManager;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        if (blockedByClient != null ? !blockedByClient.equals(contract.blockedByClient) : contract.blockedByClient != null)
            return false;
        if (blockedByManager != null ? !blockedByManager.equals(contract.blockedByManager) : contract.blockedByManager != null)
            return false;
        if (client != null ? !client.equals(contract.client) : contract.client != null) return false;
        if (connectedOptions != null ? !connectedOptions.equals(contract.connectedOptions) : contract.connectedOptions != null)
            return false;
        if (id != null ? !id.equals(contract.id) : contract.id != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(contract.phoneNumber) : contract.phoneNumber != null)
            return false;
        if (tariff != null ? !tariff.equals(contract.tariff) : contract.tariff != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (tariff != null ? tariff.hashCode() : 0);
        result = 31 * result + (blockedByClient != null ? blockedByClient.hashCode() : 0);
        result = 31 * result + (blockedByManager != null ? blockedByManager.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (connectedOptions != null ? connectedOptions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", tariff_id=" + (tariff != null ? tariff.getId() : "null") +
                ", blockedByClient=" + blockedByClient +
                ", blockedByManager=" + blockedByManager +
                ", client_id=" + (client != null ? client.getId() : "null") +
                ", connectedOptions.size=" + (connectedOptions != null ? connectedOptions.size() : 0) +
                '}';
    }

    public List<Option> getConnectedOptions() {
        return connectedOptions;
    }

    public void setConnectedOptions(List<Option> connectedOptions) {
        this.connectedOptions = connectedOptions;
    }


    public static Contract getFromDTO(ContractDto contractDto) {
        return new Contract(
                contractDto.getPhoneNumber(),
                null,
                contractDto.getBlockedByClient(),
                contractDto.getBlockedByManager(),
                null,
                null
        );
    }

    public void update(ContractDto updatedContractDto) {
        this.phoneNumber = updatedContractDto.getPhoneNumber();
        this.blockedByClient = updatedContractDto.getBlockedByClient();
        this.blockedByManager = updatedContractDto.getBlockedByManager();
    }

    public void addConnectedOption(Option option) {
        if (connectedOptions == null)
            connectedOptions = new ArrayList<Option>();

        if (!connectedOptions.contains(option))
            connectedOptions.add(option);

        List<Contract> connectedContractsFromClient = option.getConnectedContracts();
        if (connectedContractsFromClient == null) {
            connectedContractsFromClient = new ArrayList<Contract>();
        }

        if (!connectedContractsFromClient.contains(this))
            connectedContractsFromClient.add(this);

        option.setConnectedContracts(connectedContractsFromClient);
    }

    public void addConnectedOptions(List<Option> options) {
        for (Option o : options) {
            addConnectedOption(o);
        }
    }

    public void removeConnectedOptions(List<Option> options) {
        for (Option o : options)
            removeConnectedOption(o);
    }

    private void removeConnectedOption(Option option) {
        if (connectedOptions == null)
            connectedOptions = new ArrayList<Option>();

        if (connectedOptions.contains(option))
            connectedOptions.remove(option);

        List<Contract> connectedContractsFromClient = option.getConnectedContracts();
        if (connectedContractsFromClient == null)
            connectedContractsFromClient = new ArrayList<Contract>();

        if (connectedContractsFromClient.contains(this))
            connectedContractsFromClient.remove(this);

        option.setConnectedContracts(connectedContractsFromClient);
    }
}
