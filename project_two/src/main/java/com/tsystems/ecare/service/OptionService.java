package com.tsystems.ecare.service;

import com.tsystems.ecare.domain.Option;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public interface OptionService {

    /**
     * Adding new option to DB
     * @param option DTO with option data
     */
    public Long add(OptionDto option);

    /**
     * Getting Option DTO by option id
     *
     * @param optionId option id
     * @param withLists if is true, in DTO will be added lists of dependent
     *                  and incompatible options and possible tariffs
     *
     * @return Option DTO
     */
    public OptionDto getOptionById(Long optionId, boolean withLists);

    /**
     * Getting all options
     * @return List of Option DTO's
     */
    public List<OptionDto> getAllOptions();

    boolean isOptionsCompatible(List<Option> options, StringBuilder result);

    /**
     * Checking whether an option 2 in the tree-incompatible options from option 1
     * @param currentOption root of dependency tree
     * @param optionForSearch option for checking
     * @return true, if tree contains option for checking, else otherwise
     */
    public boolean incompatibleOptionsTreeContains(Option currentOption, Option optionForSearch);

    /**
     * Checking whether an option 2 in the tree-dependent options from option 1
     * @param currentOption root of dependency tree
     * @param optionForSearch option for checking
     * @return true, if tree contains option for checking, else otherwise
     */
    public boolean dependentOptionsTreeContains(Option currentOption, Option optionForSearch);

    /**
     *
     * @param targetOptionId
     * @param dependentOptionId
     * @return
     *
     * @throws IncompatibleOptionsException
     */
    public boolean addDependentOption(Long targetOptionId, Long dependentOptionId) throws IncompatibleOptionsException;

    /**
     *
     * @param targetOptionId
     * @param incompatibleOptionId
     * @return
     *
     * @throws IncompatibleOptionsException
     */
    public boolean addIncompatibleOption(Long targetOptionId, Long incompatibleOptionId) throws IncompatibleOptionsException;

    /**
     *
     * @param targetOptionId
     * @param dependentOptionId
     * @return
     */
    public boolean removeDependentOption(Long targetOptionId, Long dependentOptionId);

    /**
     *
     * @param targetOptionId
     * @param incompatibleOptionId
     * @return
     */
    public boolean removeIncompatibleOption(Long targetOptionId, Long incompatibleOptionId);

    /**
     * Getting list of options possible for option with id in args
     * @param optionId option id
     * @return List of Option DTO's
     */
    public List<OptionDto> getAllPossibleOptions(Long optionId);

    public void addPossibleTariff(Long optionId, Long tariffId);

    public void removePossibleTariff(Long optionId, Long tariffId);

    public boolean updateInformation(OptionDto optionDto);

    public boolean remove(Long optionId);
}
