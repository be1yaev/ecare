package com.tsystems.ecare.service;

import com.tsystems.ecare.dao.OptionDao;
import com.tsystems.ecare.dao.TariffDao;
import com.tsystems.ecare.domain.Tariff;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.mapper.OptionMapper;
import com.tsystems.ecare.mapper.TariffMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class TariffServiceImpl implements TariffService {

    private static final Logger logger = Logger.getLogger(TariffServiceImpl.class.getName());

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private SecurityService securityService;

    @Override
    @Transactional
    public Long add(TariffDto tariff) {

        Tariff newTariff;
        newTariff = new Tariff();

        TariffMapper.fromDtoToEntity(tariff, newTariff);

        return tariffDao.addNewTariff(newTariff);
    }

    @Override
    @Transactional
    public TariffDto getTariffById(Long tariffId, boolean withPossibleOptions) {
        Tariff tariff;
        tariff = tariffDao.getTariffById(tariffId);

        if (tariff == null)
            return null;

        TariffDto tariffDto;
        tariffDto = new TariffDto();

        TariffMapper.fromEntityToDto(tariffDto, tariff);

        if (withPossibleOptions) {
            List<OptionDto> possibleOptions = new ArrayList<>();
            OptionMapper.fromListEntityesToDtos(possibleOptions, tariff.getPossibleOptions());
            tariffDto.setPossibleOptions(possibleOptions);
        }

        return tariffDto;
    }

    @Override
    @Transactional
    public List<TariffDto> getAllTariffs() {

        List<TariffDto> resultList;
        resultList = new ArrayList<>();

        TariffMapper.fromListEntityesToDtos(resultList, tariffDao.getAllTariffs());

        return resultList;
    }

    @Override
    @Transactional
    public boolean updateInformation(TariffDto tariffDto) {
        Tariff tariff;
        tariff = tariffDao.getTariffById(tariffDto.getId());

        if (tariff == null)
            return false;

        TariffMapper.fromDtoToEntity(tariffDto, tariff);
        return tariffDao.update(tariff);
    }

    @Override
    @Transactional
    public boolean remove(Long tariffId) throws IllegalStateException {
        Tariff tariff;
        tariff = tariffDao.getTariffById(tariffId);

        if (tariff == null)
            return false;

        if (tariff.getContracts().size() != 0)
            throw new IllegalStateException("Tariff can not be removed, because have "
                    + tariff.getContracts().size() + " contract(s) associated with it");

        tariffDao.removeOption(tariff);
        return true;
    }
}
