package com.tsystems.ecare.service;

import com.tsystems.ecare.dao.OptionDao;
import com.tsystems.ecare.dao.TariffDao;
import com.tsystems.ecare.domain.Option;
import com.tsystems.ecare.domain.Tariff;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.mapper.OptionMapper;
import com.tsystems.ecare.mapper.TariffMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class OptionServiceImpl implements OptionService {

    private static final Logger logger = Logger.getLogger(OptionServiceImpl.class.getName());

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private SecurityService securityService;

    @Override
    @Transactional
    public Long add(OptionDto option) {

        Option newOption;
        newOption = new Option();

        OptionMapper.fromDtoToEntity(option, newOption);

        return optionDao.addNewOption(newOption);
    }

    @Override
    @Transactional
    public OptionDto getOptionById(Long optionId, boolean withLists) {
        Option option;
        option = optionDao.getOptionById(optionId);

        if (option == null)
            return null;

        OptionDto optionDto;
        optionDto = new OptionDto();

        OptionMapper.fromEntityToDto(optionDto, option);

        if (withLists) {
            List<OptionDto> dependentOptionsDtos = new ArrayList<>();
            List<OptionDto> incompatibleOptionsDtos = new ArrayList<>();
            List<TariffDto> possibleTariffsDtos = new ArrayList<>();

            OptionMapper.fromListEntityesToDtos(dependentOptionsDtos, option.getDependentOptions());
            OptionMapper.fromListEntityesToDtos(incompatibleOptionsDtos, option.getIncompatibleOptions());
            TariffMapper.fromListEntityesToDtos(possibleTariffsDtos, option.getPossibleTariffs());

            optionDto.setDependentOptions(dependentOptionsDtos);
            optionDto.setIncompatibleOptions(incompatibleOptionsDtos);
            optionDto.setPossibleTariffs(possibleTariffsDtos);
        }

        return optionDto;
    }

    @Override
    @Transactional
    public List<OptionDto> getAllOptions() {

        List<OptionDto> resultList;
        resultList = new ArrayList<>();

        OptionMapper.fromListEntityesToDtos(resultList, optionDao.getAllOptions());

        return resultList;
    }

    @Override
    public boolean isOptionsCompatible(List<Option> options, StringBuilder result) {

        boolean isCompatible = true;

        for (Option option1 : options) {
            // Check for incompatibility
            for (Option option2 : options) {
                if (option1.equals(option2)) {
                    continue;
                }

                if (option1.getIncompatibleOptions().contains(option2)) {
                    result.append(", option id{" + option1.getId() + "} incompatible with option id{"
                            + option2.getId() + "}");
                    isCompatible = false;
                }
            }

            // Check for dependency
            for (Option dependency : option1.getDependentOptions()) {
                if (!options.contains(dependency)) {
                    result.append(", not enough according with id{" + dependency.getId() + "} for option id{"
                            + option1.getId() + "}");
                    isCompatible = false;
                }
            }
        }

        return isCompatible;
    }

    @Override
    public boolean incompatibleOptionsTreeContains(Option currentOption, Option optionForSearch) {

        // TODO: N+1 select

        if (currentOption.getIncompatibleOptions().contains(optionForSearch))
            return true;

        for (Option o : currentOption.getDependentOptions()) {
            if (incompatibleOptionsTreeContains(o, optionForSearch))
                return true;
        }

        return false;
    }

    @Override
    public boolean dependentOptionsTreeContains(Option currentOption, Option optionForSearch) {

        if (currentOption.equals(optionForSearch))
            return true;

        for (Option o : currentOption.getDependentOptions()) {
            if (dependentOptionsTreeContains(o, optionForSearch))
                return true;
        }

        return false;
    }

    @Override
    @Transactional
    public boolean addDependentOption(Long targetOptionId, Long dependentOptionId) throws IncompatibleOptionsException {

        if (targetOptionId == dependentOptionId)
            throw new IllegalArgumentException("Target and added options are equals");

        Option targetOption;
        targetOption = optionDao.getOptionById(targetOptionId);

        Option dependentOption;
        dependentOption = optionDao.getOptionById(dependentOptionId);

        List<Option> dependentOptions = targetOption.getDependentOptions();
        List<Option> incompatibleOptions = targetOption.getIncompatibleOptions();

        // Check for dependent options not contains added option
        if (dependentOptions.contains(dependentOption)) {
            return true;
        }

        if (incompatibleOptionsTreeContains(targetOption, dependentOption)) {
            throw new IncompatibleOptionsException("option id{" + dependentOption.getId()
                    + "} already in incompatible options");
        }

        for (Option option : dependentOption.getDependentOptions()) {
            if (dependentOptionsTreeContains(option, targetOption)) {
                throw new IncompatibleOptionsException("option id{" + targetOption.getId()
                        + "} already depends on id{" + dependentOption.getId() + "}");
            }
        }

        dependentOptions.add(dependentOption);

        return optionDao.update(targetOption);
    }

    @Override
    @Transactional
    public boolean addIncompatibleOption(Long targetOptionId, Long incompatibleOptionId) throws IncompatibleOptionsException {

        Option targetOption;
        targetOption = optionDao.getOptionById(targetOptionId);

        Option incompatibleOption;
        incompatibleOption = optionDao.getOptionById(incompatibleOptionId);

        List<Option> incompatibleOptions = targetOption.getIncompatibleOptions();

        // Check for incompatible options not contains added option
        if (targetOption.getIncompatibleOptions().contains(incompatibleOption)
                && incompatibleOption.getIncompatibleOptions().contains(targetOption)) {
            return true;
        }

        // Check for dependent options tree not contains added option
        if (dependentOptionsTreeContains(targetOption, incompatibleOption)) {
            throw new IncompatibleOptionsException("option id{" + targetOption.getId()
                    + "} already depends on id{" + incompatibleOption.getId() + "}");
        }

        if (dependentOptionsTreeContains(incompatibleOption, targetOption)) {
            throw new IncompatibleOptionsException("option id{" + incompatibleOption.getId()
                    + "} already depends on id{" + targetOption.getId() + "}");
        }

        targetOption.getIncompatibleOptions().add(incompatibleOption);
        incompatibleOption.getIncompatibleOptions().add(targetOption);

        return optionDao.update(targetOption) && optionDao.update(incompatibleOption);
    }

    @Override
    @Transactional
    public boolean removeDependentOption(Long targetOptionId, Long dependentOptionId) {
        Option targetOption;
        targetOption = optionDao.getOptionById(targetOptionId);

        Option dependentOption;
        dependentOption = optionDao.getOptionById(dependentOptionId);

        targetOption.getDependentOptions().remove(dependentOption);

        return optionDao.update(targetOption);
    }

    @Override
    @Transactional
    public boolean removeIncompatibleOption(Long targetOptionId, Long incompatibleOptionId) {

        Option targetOption;
        targetOption = optionDao.getOptionById(targetOptionId);

        Option incompatibleOption;
        incompatibleOption = optionDao.getOptionById(incompatibleOptionId);

        targetOption.getIncompatibleOptions().remove(incompatibleOption);
        incompatibleOption.getIncompatibleOptions().remove(targetOption);

        return optionDao.update(targetOption);
    }

    @Override
    @Transactional
    public List<OptionDto> getAllPossibleOptions(Long optionId) {

        List<OptionDto> resultList = new ArrayList<>();
        List<OptionDto> dependentOptions = new ArrayList<>();
        List<OptionDto> incompatibleOptions = new ArrayList<>();

        Option option;
        option = optionDao.getOptionById(optionId);

        OptionDto dto;
        dto = new OptionDto();

        OptionMapper.fromEntityToDto(dto, option);

        OptionMapper.fromListEntityesToDtos(resultList, optionDao.getAllOptions());
        OptionMapper.fromListEntityesToDtos(dependentOptions, option.getDependentOptions());
        OptionMapper.fromListEntityesToDtos(incompatibleOptions, option.getIncompatibleOptions());

        resultList.remove(dto);
        resultList.removeAll(dependentOptions);
        resultList.removeAll(incompatibleOptions);

        return resultList;
    }

    @Override
    @Transactional
    public void addPossibleTariff(Long optionId, Long tariffId) {
        Option option;
        option = optionDao.getOptionById(optionId);

        Tariff tariff;
        tariff = tariffDao.getTariffById(tariffId);

        if (option.getPossibleTariffs().contains(tariff))
            return;

        option.getPossibleTariffs().add(tariff);
        tariff.getPossibleOptions().add(option);

        optionDao.update(option);
    }

    @Override
    @Transactional
    public void removePossibleTariff(Long optionId, Long tariffId) {
        Option option;
        option = optionDao.getOptionById(optionId);

        Tariff tariff;
        tariff = tariffDao.getTariffById(tariffId);

        if (!option.getPossibleTariffs().contains(tariff))
            return;

        option.getPossibleTariffs().remove(tariff);
        tariff.getPossibleOptions().remove(option);
        // TODO: remove this option from possible options in tariff?

        optionDao.update(option);
    }

    @Override
    @Transactional
    public boolean updateInformation(OptionDto optionDto) {
        Option option;
        option = optionDao.getOptionById(optionDto.getId());

        if (option == null)
            return false;

        OptionMapper.fromDtoToEntity(optionDto, option);
        return optionDao.update(option);
    }

    @Override
    @Transactional
    public boolean remove(Long optionId) {
        Option option;
        option = optionDao.getOptionById(optionId);

        if (option == null)
            return false;

        optionDao.removeOption(option);
        return true;
    }
}
