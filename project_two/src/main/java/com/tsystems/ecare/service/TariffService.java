package com.tsystems.ecare.service;

import com.tsystems.ecare.dto.TariffDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public interface TariffService {

    /**
     * Adding new tariff to DB
     * @param tariff DTO with tariff data
     */
    public Long add(TariffDto tariff);

    /**
     * Getting Tariff DTO by id
     *
     * @param tariffId tariff id
     * @param withPossibleOptions possible options for this tariff
     *
     * @return Tariff DTO
     */
    public TariffDto getTariffById(Long tariffId, boolean withPossibleOptions);

    /**
     * Getting all tariffs
     * @return List of Tariff DTO's
     */
    public List<TariffDto> getAllTariffs();

    /**
     * Update information fields in tariff object
     * @param tariffDto tariff DTO
     * @return true, if all is ok
     */
    public boolean updateInformation(TariffDto tariffDto);

    /**
     * Remove tariff from db
     * @param tariffId id of tariff
     * @return result of deleting
     */
    public boolean remove(Long tariffId) throws IllegalStateException;
}
