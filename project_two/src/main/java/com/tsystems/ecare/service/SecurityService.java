package com.tsystems.ecare.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public interface SecurityService extends LogoutSuccessHandler, AuthenticationSuccessHandler, UserDetailsService {

    /**
     * Load user by email
     *
     * @param email user email (login)
     * @return UserDetails object
     */
    @Override
    public UserDetails loadUserByUsername(String email);

    /**
     * Check role of user
     *
     * @param role user role ("ROLE_ADMIN", "ROLE_CLIENT", etc)
     * @return true, if role in params is equals user role
     */
    public boolean hasRole(String role);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication);

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                       HttpServletResponse response,
                       Authentication authentication);

    HttpSession getSession();

    Long getUserId();
}
