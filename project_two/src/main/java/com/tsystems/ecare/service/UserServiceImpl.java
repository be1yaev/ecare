package com.tsystems.ecare.service;

import com.tsystems.ecare.dao.UserDao;
import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.dto.UserDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.mapper.ContractMapper;
import com.tsystems.ecare.mapper.UserMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private UserDao userDao;

    @Autowired
    private SecurityService securityService;

    @Override
    @Transactional
    public boolean add(UserDto newUserDto) {

        User newUser;
        newUser = new User();

        UserMapper.fromDtoToEntity(newUserDto, newUser);
        newUser.setRole(User.Role.CLIENT);

        return userDao.addNewUser(newUser);
    }

    @Override
    @Transactional
    public UserDto getUserById(Long userId, boolean withContracts) {
        User user = userDao.getUserById(userId);
        UserDto userDto = new UserDto();

        UserMapper.fromEntityToDto(userDto, user);
        userDto.setPassword(null);

        if (withContracts) {
            userDto.setContracts(new ArrayList<ContractDto>());
            ContractMapper.fromListEntityesToDtos(userDto.getContracts(), user.getContracts());
        }

        return userDto;
    }

    @Override
    @Transactional
    public List<UserDto> getAllUsers(List<User.Role> roles) {

        List<UserDto> resultList;
        resultList = new ArrayList<>();

        UserDto userDto;
        for (User user : userDao.getAllUsers()) {
            if (!roles.contains(user.getRole()))
                continue;

            userDto = new UserDto();

            UserMapper.fromEntityToDto(userDto, user);
            userDto.setPassword(null);

            resultList.add(userDto);
        }

        return resultList;
    }

    @Override
    @Transactional
    public boolean update(UserDto userForUpdate, String passwordConfirmation) throws AccessDeniedException {

        User userInDb;
        userInDb = userDao.getUserById(userForUpdate.getId());

        if (userInDb != null) {
            if (securityService.hasRole("ROLE_CLIENT")) {
                // Check for correct id and not changed role
                if (securityService.getUserId() != userForUpdate.getId()
                        || !userInDb.getRole().toString().equals(
                                userForUpdate.getRole().toString())) {
                    throw new AccessDeniedException("Client can only change their information");
                }

                // Check for correct password
                if (!userInDb.getPassword().equals(passwordConfirmation)) {
                    // TODO: Exception?
                    return false;
                }


            }

            if (securityService.hasRole("ROLE_MANAGER")) {
                // Manager can only change their or customer information
                if ((userInDb.getRole() != User.Role.CLIENT)
                        && securityService.getUserId().longValue() != userForUpdate.getId().longValue()) {
                    throw new AccessDeniedException("Manager can only change their or customer information");
                }
            }

            if (userForUpdate.getPassword() == null)
                userForUpdate.setPassword(userInDb.getPassword());

            if (userForUpdate.getRole() == null)
                userForUpdate.setRole(UserMapper.getDtoRole(userInDb.getRole()));

            UserMapper.fromDtoToEntity(userForUpdate, userInDb);
            return userDao.update(userInDb);
        }

        return false;
    }

    @Override
    @Transactional
    public boolean remove(Long userId) throws AccessDeniedException {
        User userForRemove;
        userForRemove = userDao.getUserById(userId);

        if (userForRemove == null)
            return false;
            //throw new UserNotFoundException("User with id{" + userId + "} not found in db");

        if ((userForRemove.getRole() == User.Role.MANAGER)
                && (!securityService.hasRole("ROLE_ADMIN"))) {
            throw new AccessDeniedException("User with role 'Manager' can't remove manager.");
        }

        userDao.removeUser(userForRemove);

        return true;
    }
}
