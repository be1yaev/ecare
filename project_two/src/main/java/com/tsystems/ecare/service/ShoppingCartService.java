package com.tsystems.ecare.service;

import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.util.ShoppingCart;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrew on 29.09.2014.
 */
@Service
public interface ShoppingCartService {

    /**
     * Adding changed or edited contract into Cart
     * @param contractDto DTO with contract data
     * @return result of operation
     */
    public boolean addContract(ContractDto contractDto);

    /**
     * Adding removed contract into Cart
     * @param contractId removed contract id
     * @return result of operation
     */
    public boolean addRemovedContract(Long contractId);

    /**
     * Remove changed or deleted contract from Cart
     * @param contractId contract id
     * @return result of operation
     */
    public boolean removeChangedContract(Long contractId);

    /**
     * Remove new contract from Cart
     * @param phoneNumber phone number
     * @return result of operation
     */
    public boolean removeNewContract(String phoneNumber);

    /**
     * Getting shopping cart
     * @return shopping cart object
     */
    public ShoppingCart getCart();

    /**
     *
     * @return
     */
    public List<ContractDto> getNewContracts();

    /**
     *
     * @return
     */
    public List<ContractDto> getChangedContracts();

    /**
     *
     * @return
     */
    public List<ContractDto> getRemovedContracts();

    public ContractDto getNewContract(String phoneNumber);

    public ContractDto getChangedContract(Long id);

    public Long saveNewContract(String phoneNumber)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException;

    public boolean saveChangedContract(Long contractId)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException;

    public boolean saveRemovedContract(Long contractId);

    public boolean removeRemovedContract(Long contractId);
}
