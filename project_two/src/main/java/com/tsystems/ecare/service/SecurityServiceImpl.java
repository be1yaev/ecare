package com.tsystems.ecare.service;

import com.tsystems.ecare.util.Constants;
import com.tsystems.ecare.dao.UserDao;
import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.util.ShoppingCart;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    private static final Logger logger = Logger.getLogger(SecurityServiceImpl.class.getName());

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {

        logger.info("Attempt to login by email{'" + email + "'}");

        String password;
        String role;

        if (email.equals(Constants.ADMIN_USER_NAME)) {
            password = Constants.ADMIN_PASSWORD;
            role = "ROLE_ADMIN";
        } else {
            // Getting user from db
            User user = userDao.getUserByEmail(email);
            if (user == null) {
                logger.info("Login not possible");
                return null;
            }
            password = user.getPassword();
            role = "ROLE_" + user.getRole().toString();
        }

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));

        UserDetails authenticatedUser = new org.springframework.security.core.userdetails.User(
                email,
                password,
                true,
                true,
                true,
                true,
                authorities
        );

        return authenticatedUser;
    }

    @Override
    @Transactional
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) {

        String userEmail = authentication.getName();
        HttpSession session = request.getSession(); // true == allow create

        if (userEmail.equals(Constants.ADMIN_USER_NAME)) {
            logger.info("Admin has been logged in");
        } else {
            User loggedUser = userDao.getUserByEmail(userEmail);

            // Adding id of user into session
            session.setAttribute(Constants.SESSION_ATTR_CURRENT_USER_ID, loggedUser.getId());

            logger.info("Login success by email{'" + loggedUser.getEmail()
                    + "'}, user role: " + loggedUser.getRole());
        }

        ShoppingCart cart;
        cart = new ShoppingCart();
        session.setAttribute(Constants.SESSION_ATTR_SHOPPING_CART, cart);

        SavedRequest savedRequest = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        try {
            if (savedRequest != null) {
                response.sendRedirect(savedRequest.getRedirectUrl());
            } else {
                String referer = request.getHeader("Referer");
                if (referer != null) {
                    response.sendRedirect(referer);
                } else {
                    response.sendRedirect(request.getContextPath());
                }
            }
        } catch (IOException e) {
            logger.error("Error in redirect: " + e.getMessage() + ", redirect url: " + savedRequest.getRedirectUrl());
        }
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response,
                                Authentication authentication) {

        request.getSession().invalidate();

        logger.info("Logout success by user{'" + authentication.getName() + "}");
        request.getSession().removeAttribute("SPRING_SECURITY_SAVED_REQUEST");

        try {
            response.sendRedirect(request.getContextPath());
        } catch (IOException e) {
            logger.error("Error in redirect: " + e.getMessage() + ", redirect url: " + request.getContextPath());
        }
    }

    @Override
    public boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }

    @Override
    public HttpSession getSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(/*true*/);
    }

    @Override
    public Long getUserId() {
        HttpSession session = getSession();
        return (Long) session.getAttribute(Constants.SESSION_ATTR_CURRENT_USER_ID);
    }
}
