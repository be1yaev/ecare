package com.tsystems.ecare.service;

import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.UserDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public interface UserService {

    /**
     * Adding new user to DB
     * @param user DTO with user data
     */
    boolean add(UserDto user);

    UserDto getUserById(Long userId, boolean withContracts);

    public List<UserDto> getAllUsers(List<User.Role> roles);

    boolean update(UserDto user, String passwordConfirmation) throws AccessDeniedException;

    boolean remove(Long userId) throws AccessDeniedException;
}
