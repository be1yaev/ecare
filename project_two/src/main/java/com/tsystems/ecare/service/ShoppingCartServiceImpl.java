package com.tsystems.ecare.service;

import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.util.Constants;
import com.tsystems.ecare.util.ShoppingCart;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private static final Logger logger = Logger.getLogger(ShoppingCartServiceImpl.class.getName());

    @Autowired
    private ContractService contractService;

    @Autowired
    private SecurityService securityService;

    public ShoppingCart getShoppingCart() {
        HttpSession session = securityService.getSession();
        return (ShoppingCart) session.getAttribute(Constants.SESSION_ATTR_SHOPPING_CART);
    }

    @Override
    public boolean addContract(ContractDto contract) {

        ShoppingCart cart = getShoppingCart();

        if (contract == null)
            return false;

        if (contract.getUser() == null)
            return false;

        if (contract.getTariff() == null)
            return false;

        if (contract.getId() != null) {
            cart.addChangedContractToCart(contract);
        } else {
            cart.addNewContractToCart(contract);
        }

        return true;
    }

    @Override
    public boolean addRemovedContract(Long contractId) {
        ShoppingCart cart = getShoppingCart();

        ContractDto contract;
        contract = contractService.getContractById(contractId, false);

        if (contract == null)
            return false;

        cart.addRemovedContractToCart(contract);
        return true;
    }

    @Override
    public boolean removeChangedContract(Long contractId) {

        ShoppingCart cart = getShoppingCart();

        if (cart.getChangedContractById(contractId) != null) {
            cart.deleteChangedContractFromCartById(contractId);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removeRemovedContract(Long contractId) {

        ShoppingCart cart = getShoppingCart();

         if (cart.getRemovedContractById(contractId) != null) {
            cart.deleteRemovedContractFromCartById(contractId);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removeNewContract(String phoneNumber) {

        ShoppingCart cart = getShoppingCart();

        if (cart.getNewContractByPhoneNumber(phoneNumber) != null) {
            cart.deleteNewContractFromCartByPhoneNumber(phoneNumber);
            return true;
        }

        return false;
    }

    @Override
    public ShoppingCart getCart() {
        return getShoppingCart();
    }

    @Override
    public List<ContractDto> getNewContracts() {
        return getShoppingCart().getNewContracts();
    }

    @Override
    public List<ContractDto> getChangedContracts() {
        return getShoppingCart().getChangedContracts();
    }

    @Override
    public List<ContractDto> getRemovedContracts() {
        return getShoppingCart().getRemovedContracts();
    }

    @Override
    public ContractDto getNewContract(String phoneNumber) {
        return getShoppingCart().getNewContractByPhoneNumber(phoneNumber);
    }

    @Override
    public ContractDto getChangedContract(Long id) {
        return getShoppingCart().getChangedContractById(id);
    }

    @Override
    public Long saveNewContract(String phoneNumber)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException {

        ShoppingCart cart = getShoppingCart();

        ContractDto contractDto;
        contractDto = cart.getNewContractByPhoneNumber(phoneNumber);

        if (contractDto == null) {
            throw new NullPointerException("Contract with phone{" + phoneNumber
                    + "} hasn't been found in your shopping cart");
        }

        Long newContractId;
        newContractId = contractService.add(contractDto);

        if (newContractId != null)
            removeNewContract(phoneNumber);

        return newContractId;
    }

    @Override
    public boolean saveChangedContract(Long contractId)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException {

        ShoppingCart cart = getShoppingCart();

        ContractDto contractDto;
        contractDto = cart.getChangedContractById(contractId);

        if (contractDto == null) {
            throw new NullPointerException("Contract with id{" + contractId
                    + "} hasn't been found in your shopping cart");
        }

        boolean result;
        result = contractService.update(contractDto);

        if (result)
            removeChangedContract(contractId);

        return result;
    }

    @Override
    public boolean saveRemovedContract(Long contractId) {
        ShoppingCart cart = getShoppingCart();

        ContractDto contractDto;
        contractDto = cart.getRemovedContractById(contractId);

        if (contractDto == null) {
            throw new NullPointerException("Contract with id{" + contractId
                    + "} hasn't been found in your shopping cart");
        }

        boolean result;
        result = contractService.remove(contractId);

        if (result)
            removeRemovedContract(contractId);

        return result;
    }
}
