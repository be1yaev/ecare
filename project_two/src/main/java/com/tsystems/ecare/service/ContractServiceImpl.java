package com.tsystems.ecare.service;

import com.tsystems.ecare.dao.ContractDao;
import com.tsystems.ecare.dao.OptionDao;
import com.tsystems.ecare.dao.TariffDao;
import com.tsystems.ecare.dao.UserDao;
import com.tsystems.ecare.domain.Contract;
import com.tsystems.ecare.domain.Option;
import com.tsystems.ecare.domain.Tariff;
import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.mapper.ContractMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public class ContractServiceImpl implements ContractService {

    private static final Logger logger = Logger.getLogger(ContractServiceImpl.class.getName());

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private OptionService optionService;

    @Autowired
    private SecurityService securityService;

    @Override
    @Transactional
    public Long add(ContractDto contractDto) throws AccessDeniedException,
            IncompatibleOptionsException,
            NullPointerException {

        Contract contract;
        contract = new Contract();

        if (securityService.hasRole("ROLE_CLIENT")) {
            contractDto.setBlockedByManager(false);
        } else{
            contractDto.setBlockedByClient(false);
        }

        checkAndFillContractFields(contractDto, contract);

        return contractDao.addNewContract(contract);
    }

    @Override
    @Transactional
    public ContractDto getContractById(Long contractId, boolean withLists) {
        Contract contract;
        contract = contractDao.getContractById(contractId);

        if (contract == null)
            return null;

        ContractDto contractDto;
        contractDto = new ContractDto();

        if (withLists) {
            ContractMapper.fromEntityToDtoWithLists(contractDto, contract);
        } else {
            ContractMapper.fromEntityToDto(contractDto, contract);
        }

        return contractDto;
    }

    @Override
    @Transactional
    public List<ContractDto> getAllContracts() {

        List<ContractDto> resultList;
        resultList = new ArrayList<>();

        List<Contract> allContracts;
        allContracts = contractDao.getAllContracts();

        ContractMapper.fromListEntityesToDtosWithLists(resultList, allContracts);

        return resultList;
    }

    @Override
    @Transactional
    public boolean remove(Long contractId) {
        Contract contract;
        contract = contractDao.getContractById(contractId);

        if (contract == null)
            return false;

        contractDao.removeContract(contract);
        return true;
    }

    @Override
    @Transactional
    public List<ContractDto> getUserContracts(Long userId) {
        User user;
        user = userDao.getUserById(userId);

        if (user == null)
            return null;

        List<ContractDto> resultList;
        resultList = new ArrayList<>();

        ContractMapper.fromListEntityesToDtosWithLists(resultList, user.getContracts());
        return resultList;
    }

    private void checkAndFillContractFields(ContractDto contractDto, Contract contract) throws AccessDeniedException, IncompatibleOptionsException {

        if (contractDto.getUser() == null) {
            throw new NullPointerException("User not selected for contract with id{"
                    + contractDto.getId() + "}");
        }

        if (securityService.hasRole("ROLE_CLIENT")) {
            if (contractDto.getUser().getId().longValue() != securityService.getUserId().longValue()
                    || contractDto.getUser().getId().longValue() != contract.getClient().getId().longValue())
                throw new AccessDeniedException("You can change only your contracts");

            if (contract.getBlockedByManager()) {
                throw new AccessDeniedException("Contract has been blocked by manager,"
                        + " editing not possible");
            }
        }

        User newContractClient;
        newContractClient = userDao.getUserById(contractDto.getUser().getId());

        if (newContractClient == null)
            throw new AccessDeniedException("Contract client has not exist");

        if (contract.getClient() != null) {
            contract.getClient().getContracts().remove(contract);
        }
        contract.setClient(newContractClient);
        newContractClient.getContracts().add(contract);

        if (contractDto.getTariff() == null) {
            throw new NullPointerException("Tariff not selected for contract with id{"
                    + contractDto.getId() + "}");
        }

        Tariff newContractTariff;
        newContractTariff = tariffDao.getTariffById(contractDto.getTariff().getId());

        if (newContractTariff == null)
            throw new AccessDeniedException("Contract tariff has not exist");

        if (contract.getTariff() != null) {
            contract.getTariff().getContracts().remove(contract);
        }
        contract.setTariff(newContractTariff);

        if (contract.getConnectedOptions() != null) {
            for (Option option : contract.getConnectedOptions()) {
                option.getConnectedContracts().remove(contract);
            }
        }

        contract.setConnectedOptions(new ArrayList<Option>());

        for (OptionDto optionDto : contractDto.getConnectedOptions()) {
            Option option;
            option = optionDao.getOptionById(optionDto.getId());

            if (option == null) {
                throw new NullPointerException("Option with id{" + contractDto.getId() + "} has not exist");
            }

            option.getConnectedContracts().add(contract);
            contract.getConnectedOptions().add(option);
        }

        StringBuilder resultOfParsing;
        resultOfParsing = new StringBuilder(0);

        if (!optionService.isOptionsCompatible(contract.getConnectedOptions(), resultOfParsing)) {
            throw new IncompatibleOptionsException("Can not save contract" + resultOfParsing.toString());
        }

        ContractMapper.fromDtoToEntity(contractDto, contract);
    }

    @Override
    @Transactional
    public boolean update(ContractDto contractDto) throws AccessDeniedException,
            IncompatibleOptionsException,
            NullPointerException {

        Contract contract;
        contract = contractDao.getContractById(contractDto.getId());

        if (contract == null) {
            throw new NullPointerException("Contract hasn't been exist, updating not possible");
        }

        if (securityService.hasRole("ROLE_CLIENT")) {
            contractDto.setBlockedByManager(contract.getBlockedByManager());
        } else{
            contractDto.setBlockedByClient(contract.getBlockedByClient());
        }

        checkAndFillContractFields(contractDto, contract);

        contractDao.updateContract(contract);
        return true;
    }
}
