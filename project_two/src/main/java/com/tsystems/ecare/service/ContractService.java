package com.tsystems.ecare.service;

import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Service
public interface ContractService {

    /**
     * Adding new contract to DB
     * @param contractDto DTO with contract data
     * @return contract id
     *
     * @throws AccessDeniedException
     * @throws IncompatibleOptionsException
     * @throws NullPointerException
     */
    public Long add(ContractDto contractDto)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException;

    /**
     * Getting Contract DTO by id
     *
     * @param contractId contract id
     * @param withLists if is true, result DTO will be contain all lists from entity
     *
     * @return Contract DTO or null
     */
    public ContractDto getContractById(Long contractId, boolean withLists);

    /**
     * Getting all contracts
     * @return List of Contract DTO's
     */
    public List<ContractDto> getAllContracts();
//
//    /**
//     * Update information fields in contract object
//     * @param contractDto contract DTO
//     * @return true, if all is ok
//     */
//    public boolean updateInformation(ContractDto contractDto);

    /**
     * Remove contract from db
     * @param contractId id of contract
     * @return result of deleting
     */
    public boolean remove(Long contractId);

    /**
     *
     * @param userId
     * @return
     */
    public List<ContractDto> getUserContracts(Long userId);

    /**
     * Update contract object with all lists
     * @param contractDto contract DTO
     * @return true, if all is ok
     */
    public boolean update(ContractDto contractDto)
            throws AccessDeniedException, IncompatibleOptionsException, NullPointerException;
}
