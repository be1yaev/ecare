package com.tsystems.ecare.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.List;

public class TariffDto implements Serializable, Comparable {

    private Long id;

    @NotEmpty(message = "Field \"Tariff title\" can not be empty")
    @Length(min = 1, max = 50, message = "Length of \"Tariff title\" must be between {2} and {1} characters")
    private String title;

    @NotEmpty(message = "Field \"Tariff description\" can not be empty")
    @Length(min = 1, max = 250, message = "Length of \"Tariff description\" must be between {2} and {1} characters")
    private String description;

    private List<OptionDto> possibleOptions;

    public TariffDto() {
    }

    public TariffDto(Long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OptionDto> getPossibleOptions() {
        return possibleOptions;
    }

    public void setPossibleOptions(List<OptionDto> possibleOptions) {
        this.possibleOptions = possibleOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariffDto tariffDto = (TariffDto) o;

        if (description != null ? !description.equals(tariffDto.description) : tariffDto.description != null)
            return false;
        if (id != null ? !id.equals(tariffDto.id) : tariffDto.id != null) return false;
        if (title != null ? !title.equals(tariffDto.title) : tariffDto.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((TariffDto)o).getId());
    }

    @Override
    public String toString() {
        return "TariffDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
