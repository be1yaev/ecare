package com.tsystems.ecare.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class ContractDto implements Serializable, Comparable {

    private Long id;

    @NotNull(message = "Field \"Phone numbere\" can not be empty")
    @Size(min = 5, max = 20, message = "Length of \"Phone number\" must be between {2} and {1} characters")
    private String phoneNumber;

    private Boolean blockedByClient;
    private Boolean blockedByManager;

    private TariffDto tariff;
    private UserDto user;
    private List<OptionDto> connectedOptions;

    public ContractDto() {
    }

    public ContractDto(Long id, String phoneNumber, Boolean blockedByClient, Boolean blockedByManager) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.blockedByClient = blockedByClient;
        this.blockedByManager = blockedByManager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getBlockedByClient() {
        return blockedByClient;
    }

    public void setBlockedByClient(Boolean blockedByClient) {
        this.blockedByClient = blockedByClient;
    }

    public Boolean getBlockedByManager() {
        return blockedByManager;
    }

    public void setBlockedByManager(Boolean blockedByManager) {
        this.blockedByManager = blockedByManager;
    }

    public TariffDto getTariff() {
        return tariff;
    }

    public void setTariff(TariffDto tariff) {
        this.tariff = tariff;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public List<OptionDto> getConnectedOptions() {
        return connectedOptions;
    }

    public void setConnectedOptions(List<OptionDto> connectedOptions) {
        this.connectedOptions = connectedOptions;
    }

    @Override
    public String toString() {
        return "ContractDTO{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", blockedByClient=" + blockedByClient +
                ", blockedByManager=" + blockedByManager +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractDto that = (ContractDto) o;

        if (blockedByClient != null ? !blockedByClient.equals(that.blockedByClient) : that.blockedByClient != null)
            return false;
        if (blockedByManager != null ? !blockedByManager.equals(that.blockedByManager) : that.blockedByManager != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (blockedByClient != null ? blockedByClient.hashCode() : 0);
        result = 31 * result + (blockedByManager != null ? blockedByManager.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((ContractDto)o).getId());
    }
}
