package com.tsystems.ecare.dto;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable, Comparable {
    public enum Role {
        MANAGER,
//        ADMIN,
        CLIENT;
    }

    private Long id;

    @NotNull(message = "Field \"User name\" can not be empty")
    @Size(min = 3, max = 25, message = "Length of \"User name\" must be between {2} and {1} characters")
    private String surname;


    @NotNull(message = "Field \"User firstname\" can not be empty")
    @Size(min = 3, max = 25, message = "Length of \"User firstname\" must be between {2} and {1} characters")
    private String firstname;

    @Size(min = 3, max = 25, message = "Length of \"User patronymic\" must be between {2} and {1} characters")
    private String patronymic;

    @Size(min = 6, max = 20, message = "Length of \"Passport number\" must be between {2} and {1} characters")
    private String passportNumber;

    @Size(min = 1, max = 50, message = "Length of \"Passport description\" must be between {2} and {1} characters")
    private String passportDescription;

    @Size(min = 1, max = 25, message = "Length of \"Country name\" must be between {2} and {1} characters")
    private String country;

    @Size(min = 1, max = 25, message = "Length of \"City name\" must be between {2} and {1} characters")
    private String city;

    @Size(min = 1, max = 100, message = "Length of \"Address\" must be between {2} and {1} characters")
    private String address;

    @Email
    @Size(min = 5, max = 50, message = "Length of \"E-mail\" must be between {2} and {1} characters")
    private String email;

    @Size(min = 1, max = 50, message = "Length of \"Password\" must be between {2} and {1} characters")
    private String password;

    private Role role;

    private List<ContractDto> contracts;

    public UserDto() {
    }

    public UserDto(Long id,
                   String surname,
                   String firstname,
                   String patronymic,
                   String passportNumber,
                   String passportDescription,
                   String country,
                   String city,
                   String address,
                   String email,
                   String password,
                   Role role) {

        this.id = id;
        this.surname = surname;
        this.firstname = firstname;
        this.patronymic = patronymic;
        this.passportNumber = passportNumber;
        this.passportDescription = passportDescription;
        this.country = country;
        this.city = city;
        this.address = address;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportDescription() {
        return passportDescription;
    }

    public void setPassportDescription(String passportDescription) {
        this.passportDescription = passportDescription;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<ContractDto> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractDto> contracts) {
        this.contracts = contracts;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", passportDescription='" + passportDescription + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDto userDto = (UserDto) o;

        if (address != null ? !address.equals(userDto.address) : userDto.address != null) return false;
        if (city != null ? !city.equals(userDto.city) : userDto.city != null) return false;
        if (country != null ? !country.equals(userDto.country) : userDto.country != null) return false;
        if (email != null ? !email.equals(userDto.email) : userDto.email != null) return false;
        if (firstname != null ? !firstname.equals(userDto.firstname) : userDto.firstname != null) return false;
        if (id != null ? !id.equals(userDto.id) : userDto.id != null) return false;
        if (passportDescription != null ? !passportDescription.equals(userDto.passportDescription) : userDto.passportDescription != null)
            return false;
        if (passportNumber != null ? !passportNumber.equals(userDto.passportNumber) : userDto.passportNumber != null)
            return false;
        if (password != null ? !password.equals(userDto.password) : userDto.password != null) return false;
        if (patronymic != null ? !patronymic.equals(userDto.patronymic) : userDto.patronymic != null) return false;
        if (role != userDto.role) return false;
        if (surname != null ? !surname.equals(userDto.surname) : userDto.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
        result = 31 * result + (passportDescription != null ? passportDescription.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((UserDto)o).getId());
    }
}
