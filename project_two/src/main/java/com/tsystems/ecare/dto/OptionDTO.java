package com.tsystems.ecare.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class OptionDto implements Serializable, Comparable {

    private Long id;

    @NotNull(message = "Field \"Option title\" can not be empty")
    @Size(min = 1, max = 50, message = "Length of \"Tariff title\" must be between {2} and {1} characters")
    private String title;

    @NotNull(message = "Field \"Option description\" can not be empty")
    @Size(min = 1, max = 250, message = "Length of \"Tariff description\" must be between {2} and {1} characters")
    private String description;

    private Float price;
    private Float connectionCost;

    private List<OptionDto> dependentOptions;
    private List<OptionDto> incompatibleOptions;

    private List<TariffDto> possibleTariffs;

    public OptionDto() {
    }

    public OptionDto(Long id, String title, String description, Float price, Float connectionCost) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.connectionCost = connectionCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getConnectionCost() {
        return connectionCost;
    }

    public void setConnectionCost(Float connectionCost) {
        this.connectionCost = connectionCost;
    }

    public List<OptionDto> getDependentOptions() {
        return dependentOptions;
    }

    public void setDependentOptions(List<OptionDto> dependentOptions) {
        this.dependentOptions = dependentOptions;
    }

    public List<OptionDto> getIncompatibleOptions() {
        return incompatibleOptions;
    }

    public void setIncompatibleOptions(List<OptionDto> incompatibleOptions) {
        this.incompatibleOptions = incompatibleOptions;
    }

    public List<TariffDto> getPossibleTariffs() {
        return possibleTariffs;
    }

    public void setPossibleTariffs(List<TariffDto> possibleTariffs) {
        this.possibleTariffs = possibleTariffs;
    }

    @Override
    public String toString() {
        return "OptionDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", connectionCost=" + connectionCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptionDto optionDto = (OptionDto) o;

        if (connectionCost != null ? !connectionCost.equals(optionDto.connectionCost) : optionDto.connectionCost != null)
            return false;
        if (description != null ? !description.equals(optionDto.description) : optionDto.description != null)
            return false;
        if (id != null ? !id.equals(optionDto.id) : optionDto.id != null) return false;
        if (price != null ? !price.equals(optionDto.price) : optionDto.price != null) return false;
        if (title != null ? !title.equals(optionDto.title) : optionDto.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (connectionCost != null ? connectionCost.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((OptionDto)o).getId());
    }
}
