package com.tsystems.ecare.util;

/**
 * Created by Andrew on 22.09.2014.
 */
public class Constants {
    public static final String ADMIN_USER_NAME = "admin@ecare.com";
    public static final String ADMIN_PASSWORD = "admin";

    public static final String SESSION_ATTR_CURRENT_USER_ID = "currentUserId";
    public static final String SESSION_ATTR_SHOPPING_CART = "shoppingCart";
    public static final String SESSION_ATTR_EDITABLE_CONTRACT = "editableContract";
}
