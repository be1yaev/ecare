package com.tsystems.ecare.util;

import com.tsystems.ecare.dto.ContractDto;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Andrew on 29.09.2014.
 */
public class ShoppingCart {
    private static final Logger logger = Logger.getLogger(ShoppingCart.class.getName());

    private Map<Long, ContractDto> changedContracts;
    private Map<Long, ContractDto> removedContracts;
    private Map<String, ContractDto> newContracts;

    public ShoppingCart() {

        changedContracts = Collections.synchronizedMap(new HashMap<Long, ContractDto>());
        removedContracts = Collections.synchronizedMap(new HashMap<Long, ContractDto>());
        newContracts = Collections.synchronizedMap(new HashMap<String, ContractDto>());
    }

    public boolean addChangedContractToCart(ContractDto contract) {
//
//        if (changedContracts.containsKey(contract.getId()))
//            return false;

        changedContracts.put(contract.getId(), contract);
        return true;
    }

    public boolean addRemovedContractToCart(ContractDto removedContract) {
//
//        if (removedContracts.containsKey(removedContract.getId()))
//            return false;

        removedContracts.put(removedContract.getId(), removedContract);
        return true;
    }

    public boolean addNewContractToCart(ContractDto contract) {
//
//        if (newContracts.containsKey(contract.getPhoneNumber()))
//            return false;

        newContracts.put(contract.getPhoneNumber(), contract);
        return true;
    }

    public ContractDto getChangedContractById(Long id) {

        if (!changedContracts.containsKey(id))
            return null;

        return changedContracts.get(id);
    }

    public ContractDto getRemovedContractById(Long id) {

        if (!removedContracts.containsKey(id))
            return null;

        return removedContracts.get(id);
    }

    public ContractDto getNewContractByPhoneNumber(String phoneNumber) {

        if (!newContracts.containsKey(phoneNumber))
            return null;

        return newContracts.get(phoneNumber);
    }

    public boolean deleteChangedContractFromCartById(Long id) {

        if (!changedContracts.containsKey(id))
            return false;

        changedContracts.remove(id);
        return true;
    }

    public boolean deleteRemovedContractFromCartById(Long id) {

        if (!removedContracts.containsKey(id))
            return false;

        removedContracts.remove(id);
        return true;
    }

    public boolean deleteNewContractFromCartByPhoneNumber(String phoneNumber) {

        if (!newContracts.containsKey(phoneNumber))
            return false;

        newContracts.remove(phoneNumber);
        return true;
    }

    public List<ContractDto> getRemovedContracts() {

        return new ArrayList<ContractDto>(removedContracts.values());
    }

    public List<ContractDto> getChangedContracts() {

        return new ArrayList<ContractDto>(changedContracts.values());
    }

    public List<ContractDto> getNewContracts() {

        return new ArrayList<ContractDto>(newContracts.values());
    }
}
