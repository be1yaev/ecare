package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Option;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* Created by Andrew on 04.09.2014.
*/
@Repository
public interface OptionDao {

    /**
     * Adding new option to DB
     * @param option not persisted new option
     */
    public Long addNewOption(Option option);

    /**
     * Find option by ID
     * @param optionId option ID
     * @return founded Option or null, if Option not found
     */
    public Option getOptionById(Long optionId);

    /**
     * Find option by title
     * @param optionTitle string with option title
     * @return founded Option or null, if Option not found
     */
    public Option getOptionByTitle(String optionTitle);

    /**
     * Find all options from DB
     * @return List<Option> with all options or null, if list is empty
     */
    public List<Option> getAllOptions();

    /**
     * Remove option from DB
     * @param option option for removing
     * @return true if option has been removed, false otherwise
     */
    public boolean removeOption(Option option);

//    /**
//     * Convert List<DTO> to List<Entity>
//     * @param options list of DTO
//     * @return list of Entity's
//     */
//    public List<Option> getOptionsListFromDtoList(List<OptionDto> options);

    /**
     * Update fields of existent Option object
     * @param option option for update
     */
    boolean update(Option option);

//    /**
//     * Convert List<Entity> to List<DTO>
//     * @param options list of Entity's
//     * @return list of DTO
//     */
//    public List<OptionDto> getOptionsDtoListFromList(List<Option> options);
}
