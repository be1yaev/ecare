package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Contract;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Repository
public class ContractDaoImpl implements ContractDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = Logger.getLogger(ContractDaoImpl.class);

    public ContractDaoImpl() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Long addNewContract(Contract contract) {
        // TODO: validation
        if (getContractByPhoneNumber(contract.getPhoneNumber()) == null) {
            getCurrentSession().save(contract);
            logger.info("Contract has been added to db: " + contract);
            return contract.getId();
        }

        logger.info("Contract has not added to db: " + contract);
        return null;
    }

    @Override
    public Contract getContractById(Long contractId) {
        List<Contract> list;

        list = getCurrentSession()
                .createQuery("from Contract as contract where contract.id =:id")
                .setString("id", contractId.toString())
                .list();

        if(!list.isEmpty()) {
            logger.info("Result of find contract by ID{" + contractId + "}: " + list.get(0));
            return list.get(0);
        }

        logger.info("Contract with ID{" + contractId + "} has not found");
        return null;
    }

    @Override
    public Contract getContractByPhoneNumber(String phoneNumber) {
        List<Contract> result;
        result = getCurrentSession()
                .createQuery("from Contract as contract where contract.phoneNumber =:phone")
                .setString("phone", phoneNumber)
                .list();

        if (result != null && result.size() != 0) {
            logger.info("Contract has been found by phone{'"
                    + phoneNumber + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("Contract with phone{'" + phoneNumber + "'} not found");
            return null;
        }
    }

    @Override
    public List<Contract> getAllContracts() {
        List<Contract> resultList;
        resultList = getCurrentSession()
                .createQuery("from Contract as contract")
                .list();

        if (resultList == null)
            resultList = Collections.emptyList();

        return resultList;
    }

    @Override
    // TODO: Add exception handler for non existent contract
    public boolean removeContract(Contract contract) {

        getCurrentSession().delete(contract);

        logger.info("Contract has been removed: " + contract);
        return true;
    }

    @Override
    public void updateContract(@Valid Contract contract) {

        getCurrentSession().update(contract);

        logger.info("Contract has been updated: " + contract);
    }

//    @Override
//    public List<Contract> getContractsLikePhoneNumber(String phoneNumber) {
//
//        List<Contract> result = null;
////        result = em.createQuery("SELECT contract " + "FROM Contract contract "
////                + "WHERE contract.phoneNumber LIKE :phone")
////                .setParameter("phone", phoneNumber)
////                .getResultList();
//
//        if (result != null && result.size() != 0) {
//            logger.info("Contracts has been found like phone{'"
//                    + phoneNumber + "'}: " + result.get(0));
//            return result;
//        } else {
//            logger.info("Contracts with phone{'" + phoneNumber + "'} not found");
//            return Collections.EMPTY_LIST;
//        }
//    }
}
