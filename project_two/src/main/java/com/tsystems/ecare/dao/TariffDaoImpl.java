package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Tariff;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class TariffDaoImpl implements TariffDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = Logger.getLogger(TariffDaoImpl.class.getName());

    public TariffDaoImpl() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Long addNewTariff(Tariff tariff) {
        // TODO: validation
        if (getTariffByTitle(tariff.getTitle()) == null) {
            getCurrentSession().save(tariff);
            logger.info("Tariff has been added to db: " + tariff);
            return tariff.getId();
        }

        logger.info("Tariff has not added to db: " + tariff);
        return null;
    }

    @Override
    public Tariff getTariffById(Long tariffId) {
        List<Tariff> list;

        list = getCurrentSession()
                .createQuery("from Tariff as tariff where tariff.id =:id")
                .setString("id", tariffId.toString())
                .list();

        if(!list.isEmpty()) {
            logger.info("Result of find tariff by ID{" + tariffId + "}: " + list.get(0));
            return list.get(0);
        }

        logger.info("Tariff with ID{" + tariffId + "} has not found");
        return null;
    }

    @Override
    public Tariff getTariffByTitle(String tariffTitle) {

        List<Tariff> result = null;
        result = getCurrentSession()
                .createQuery("from Tariff as tariff where tariff.title =:title")
                .setString("title", tariffTitle)
                .list();

        if (result != null && result.size() != 0) {
            logger.info("Tariff has been found by title{'" + tariffTitle + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("Tariff with title{'" + tariffTitle + "'} not found");
            return null;
        }
    }

    @Override
    public List<Tariff> getAllTariffs() {
        List<Tariff> resultList;
        resultList = getCurrentSession()
                .createQuery("from Tariff as tariff")
                .list();

        if (resultList == null)
            resultList = Collections.emptyList();

        return resultList;
    }

    @Override
    public boolean removeTariff(Tariff tariff) {

        getCurrentSession().delete(tariff);

        logger.info("Tariff has been removed: " + tariff);

        return true;
    }

    @Override
    public void updateTariff(Tariff tariff) {
        getCurrentSession().update(tariff);
    }

    @Override
    public boolean update(Tariff tariff) {
        sessionFactory.getCurrentSession().update(tariff);
        return true;
    }

    @Override
    public boolean removeOption(Tariff tariff) {

        getCurrentSession().delete(tariff);

        logger.info("Tariff has been removed: " + tariff);
        return true;
    }
}
