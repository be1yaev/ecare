package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Repository
public interface UserDao {

    /**
     * Adding new user to DB
     * @param user not persisted new user
     */
    public boolean addNewUser(User user);

    /**
     * Find user by ID
     * @param userId user ID
     * @return founded User or null, if User not found
     */
    public User getUserById(Long userId);

    /**
     * Find user by E-mail
     * @param email string with e-mail
     * @return founded User or null, if User not found
     */
    public User getUserByEmail(String email);

    /**
     * Find all users from DB
     * @return List<User> with all users or null, if list is empty
     */
    public List<User> getAllUsers();

    /**
     * Remove user from DB
     * @param user user for removing
     * @return true if user has been removed, false otherwise
     */
    public boolean removeUser(User user);

    boolean update(User user);
}
