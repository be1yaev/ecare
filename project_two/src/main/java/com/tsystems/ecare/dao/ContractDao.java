package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Contract;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Repository
public interface ContractDao {

    /**
     * Adding new contract to DB
     * @param contract not persisted new contract
     * return new contract id
     */
    public Long addNewContract(Contract contract);

    /**
     * Find contract by ID
     * @param contractId contract ID
     * @return founded Contract or null, if Contract not found
     */
    public Contract getContractById(Long contractId);

    /**
     * Find contract by phone number
     * @param phoneNumber string with phone number
     * @return founded Contract or null, if Contract not found
     */
    public Contract getContractByPhoneNumber(String phoneNumber);

    /**
     * Find all contracts from DB
     * @return List<Contract> with all contracts or null, if list is empty
     */
    public List<Contract> getAllContracts();

    /**
     * Remove contract from DB
     * @param contract contract for removing
     * @return true if contract has been removed, false otherwise
     */
    public boolean removeContract(Contract contract);

    /**
     * Update fields of existent Contract object
     * @param contract contract for update
     */
    public void updateContract(Contract contract);

//    /**
//     * Find contracts by part of phone number, using SQL LIKE syntax
//     * @param phoneNumber part of phone number for search
//     */
    //public List<Contract> getContractsLikePhoneNumber(String phoneNumber);
}
