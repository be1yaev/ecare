package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Option;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class OptionDaoImpl implements OptionDao{

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = Logger.getLogger(OptionDaoImpl.class.getName());

    public OptionDaoImpl() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Long addNewOption(Option option) {
        // TODO: validation
        if (getOptionByTitle(option.getTitle()) == null) {
            getCurrentSession().save(option);
            logger.info("Option has been added to db: " + option);
            return option.getId();
        }

        logger.info("Option has not added to db: " + option);
        return null;
    }

    @Override
    public Option getOptionById(Long optionId) {
        List<Option> optionsList;

        optionsList = getCurrentSession()
                .createQuery("from Option as option where option.id =:id")
                .setString("id", optionId.toString())
                .list();

        if(!optionsList.isEmpty()) {
            logger.info("Result of find option by ID{" + optionId + "}: " + optionsList.get(0));
            return optionsList.get(0);
        }

        logger.info("Option with ID{" + optionId + "} has not found");
        return null;
    }

    @Override
    public Option getOptionByTitle(String optionTitle) {
        List<Option> optionsList;

        optionsList = getCurrentSession()
                .createQuery("from Option as option where option.title =:title")
                .setString("title", optionTitle)
                .list();

        if (optionsList != null && optionsList.size() != 0) {
            logger.info("Option has been found by title{'" + optionTitle + "'}: " + optionsList.get(0));
            return optionsList.get(0);
        } else {
            logger.info("Option with title{'" + optionTitle + "'} not found");
            return null;
        }
    }

    @Override
    public List<Option> getAllOptions() {
        List<Option> resultList;
        resultList = getCurrentSession()
                .createQuery("from Option as option")
                .list();

        if (resultList == null)
            resultList = Collections.emptyList();

        return resultList;
    }

    @Override
    public boolean removeOption(Option option) {

        getCurrentSession().delete(option);

        logger.info("Option has been removed: " + option);
        return true;
    }


    @Override
    public boolean update(Option option) {
        sessionFactory.getCurrentSession().update(option);
        return true;
    }
}
