package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.User;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = Logger.getLogger(UserDaoImpl.class.getName());

    public UserDaoImpl() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public boolean addNewUser(User user) {
        // TODO: validation
        if (getUserByEmail(user.getEmail()) == null) {
            getCurrentSession().save(user);
            logger.info("User has been added to db: " + user);
            return true;
        }

        logger.info("User has not added to db: " + user);
        return false;
    }

    @Override
    public User getUserById(Long userId) {

        List<User> usersList;

        usersList = getCurrentSession()
                .createQuery("from User as user where user.id =:id")
                .setString("id", userId.toString())
                .list();

        if(!usersList.isEmpty()) {
            logger.info("Result of find user by ID{" + userId + "}: " + usersList.get(0));
            return usersList.get(0);
        }

        logger.info("User with ID{" + userId + "} has not found");
        return null;
    }

    @Override
    public User getUserByEmail(String email) {

        List<User> result = null;
        result = getCurrentSession().createQuery(
                        "SELECT user FROM User user WHERE user.email=:email"
                ).setParameter("email", email)
                .list();

        if (result != null && result.size() != 0) {
            logger.info("User has been found by email{'" + email + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("User with email{'" + email + "'} not found");
            return null;
        }
    }

    @Override
    public List<User> getAllUsers() {
        List<User> resultList;
        resultList = getCurrentSession()
                .createQuery("from User as user")
                .list();

        if (resultList == null)
            resultList = Collections.emptyList();

        return resultList;
    }

    @Override
    public boolean removeUser(User user) {
        getCurrentSession().delete(user);
        logger.info("User has been removed: " + user);
        return true;
    }

    @Override
    public boolean update(User user) {
        sessionFactory.getCurrentSession().update(user);
        return true;
    }
}
