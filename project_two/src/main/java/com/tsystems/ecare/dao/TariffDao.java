package com.tsystems.ecare.dao;

import com.tsystems.ecare.domain.Tariff;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Andrew on 04.09.2014.
 */
@Repository
public interface TariffDao {

    /**
     * Adding new tariff to DB
     * @param tariff not persisted new tariff
     */
    public Long addNewTariff(Tariff tariff);

    /**
     * Find tariff by ID
     * @param tariffId tariff ID
     * @return founded Tariff or null, if Tariff not found
     */
    public Tariff getTariffById(Long tariffId);

    /**
     * Find tariff by title
     * @param tariffTitle string with tariff title
     * @return founded Tariff or null, if Tariff not found
     */
    public Tariff getTariffByTitle(String tariffTitle);

    /**
     * Find all tariffs from DB
     * @return List<Tariff> with all tariffs or null, if list is empty
     */
    public List<Tariff> getAllTariffs();

    /**
     * Remove tariff from DB
     * @param tariff tariff for removing
     * @return true if tariff has been removed, false otherwise
     */
    public boolean removeTariff(Tariff tariff);

    /**
     * Update fields of existent Tariff object
     * @param tariff tariff for update
     */
    public void updateTariff(Tariff tariff);

    /**
     * Update fields of existent Tariff object
     * @param tariff tariff for update
     */
    public boolean update(Tariff tariff);

    /**
     * Remove tariff from DB
     * @param tariff tariff for removing
     * @return true if tariff has been removed, false otherwise
     */
    public boolean removeOption(Tariff tariff);
}
