package com.tsystems.ecare.controller;

import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.service.OptionService;
import com.tsystems.ecare.service.TariffService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Andrew on 27.09.2014.
 */
@Controller
@RequestMapping("/tariff")
public class TariffController {
    private static final Logger logger = Logger.getLogger(TariffController.class);

    @Autowired
    private TariffService tariffService;

    @Autowired
    private OptionService optionService;


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }

    @RequestMapping(value="")
    public String getMainPage(Model model) {

        logger.info("Request for getMainPage()");

        return getListOfTariffs(model);
    }

    @RequestMapping(value="/list")
    private String getListOfTariffs(Model model) {

        logger.info("Request for getListOfTariffs()");

        List<TariffDto> tariffsList;
        tariffsList = tariffService.getAllTariffs();

        model.addAttribute("tariffsList", tariffsList);

        return "tariff/list";
    }

    @RequestMapping(value="/create")
    private String getCreateTariffPage(Model model) {

        logger.info("Request for getCreateTariffPage()");

        model.addAttribute("tariff", new TariffDto());

        return "tariff/create";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addNewTariff(@Valid @ModelAttribute("tariff") TariffDto tariff,
                             BindingResult result,
                             Model model) {

        logger.info("Request for addNewTariff()");

        if (!result.hasErrors()) {
            Long createdTariffId;
            createdTariffId = tariffService.add(tariff);

            if (createdTariffId != null) {
                model.addAttribute("successMessage",
                        "Tariff has been successfully created with id {" + createdTariffId + "}");

                return getEditPossibleOptionsPage(createdTariffId, model);
            }
        }

        model.addAttribute("errorMessage", "Tariff hasn't been created, check correctness of fields");
        model.addAttribute("tariff", tariff);
        return "tariff/create";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateTariff(@Valid @ModelAttribute("tariff") TariffDto tariff,
                             BindingResult result,
                             Model model) {

        logger.info("Request for updateTariff()");

        if (result.hasErrors()) {
            return "tariff/edit/info";
        }

        if (tariffService.updateInformation(tariff)) {
            model.addAttribute("successMessage", "Tariff with id{" + tariff.getId() + "} has been successfully updated");
            return getEditTariffInfoPage(tariff.getId(), model);
        }

        model.addAttribute("errorMessage", "Tariff hasn't been updated");
        return "tariff/edit/info";
    }

    @RequestMapping(value = "/info/{id}")
    public String getTariffInfoPage(@PathVariable("id") Long tariffId,
                                    Model model) {

        logger.info("Request for getTariffInfoPage()");

        TariffDto tariffDto;
        tariffDto = tariffService.getTariffById(tariffId, true);

        if (tariffDto != null) {
            model.addAttribute("currentTariff", tariffDto);
            return "tariff/info";
        }

        model.addAttribute("errorMessage", "Tariff with id{" + tariffId + "} not exist");
        return getListOfTariffs(model);
    }

    @RequestMapping(value = "/edit/{id}/info")
    public String getEditTariffInfoPage(@PathVariable("id") Long tariffId, Model model) {

        logger.info("Request for getEditTariffInfoPage()");

        TariffDto tariffDto;
        tariffDto = tariffService.getTariffById(tariffId, true);

        if (tariffDto == null) {
            model.addAttribute("errorMessage", "Tariff with id{" + tariffId + "} not exist");
            return getListOfTariffs(model);
        }

        model.addAttribute("tariff", tariffDto);

        return "tariff/edit/info";
    }

    @RequestMapping(value = "/edit/{id}/options")
    public String getEditPossibleOptionsPage(@PathVariable("id") Long tariffId,
                                                             Model model) {

        logger.info("Request for getEditPossibleOptionsPage()");

        TariffDto tariffDto;
        tariffDto = tariffService.getTariffById(tariffId, true);

        if (tariffDto == null) {
            model.addAttribute("errorMessage", "Tariff with id{" + tariffId + "} not exist");
            return getListOfTariffs(model);
        }

        model.addAttribute("allOptionsList", optionService.getAllOptions());
        model.addAttribute("currentTariff", tariffDto);

        return "tariff/edit/options";
    }

    @RequestMapping(value = "/edit/{currentTariffId}/possible_options/add/{optionId}")
    public String addPossibleOption(
            @PathVariable("currentTariffId") Long currentTariffId,
            @PathVariable("optionId") Long optionId,
            Model model) {

        logger.info("Request for addPossibleOption()");

        optionService.addPossibleTariff(optionId, currentTariffId);

        model.addAttribute("successMessage", "Option with id{" + optionId
                + "} has been successfully added to possible options");

        return getEditPossibleOptionsPage(currentTariffId, model);
    }

    @RequestMapping(value = "/edit/{currentTariffId}/possible_options/remove/{optionId}")
    public String removePossibleOption(
            @PathVariable("currentTariffId") Long currentTariffId,
            @PathVariable("optionId") Long optionId,
            Model model) {

        logger.info("Request for removePossibleOption()");

        optionService.removePossibleTariff(optionId, currentTariffId);

        model.addAttribute("successMessage", "Option with id{" + optionId
                + "} has been successfully removed from possible options");

        return getEditPossibleOptionsPage(currentTariffId, model);
    }

    @RequestMapping("/remove/{id}")
    public String removeTariff(@PathVariable("id") Long tariffId,
                               Model model) {

        logger.info("Request for removeTariff()");

        try {
            if (tariffService.remove(tariffId)) {
                model.addAttribute("successMessage", "Tariff with id{" + tariffId + "} has been successfully removed from DB");
            } else {
                model.addAttribute("errorMessage", "Tariff with id{" + tariffId + "} can not be removed");
            }
        } catch (IllegalStateException e) {
            model.addAttribute("errorMessage", e.getMessage());
        }

        return getListOfTariffs(model);
    }

}
