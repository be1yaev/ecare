package com.tsystems.ecare.controller;

import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.service.OptionService;
import com.tsystems.ecare.service.TariffService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by Andrew on 27.09.2014.
 */
@Controller
@RequestMapping("/option")
public class OptionController {
    private static final Logger logger = Logger.getLogger(OptionController.class);

    @Autowired
    private OptionService optionService;

    @Autowired
    private TariffService tariffService;


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }

    @RequestMapping(value="")
    public String getMainPage(Model model) {

        logger.info("Request for getMainPage()");

        return getListOfOptions(model);
    }

    @RequestMapping(value="/list")
    private String getListOfOptions(Model model) {

        logger.info("Request for getListOfOptions()");

        List<OptionDto> optionsList;
        optionsList = optionService.getAllOptions();

        model.addAttribute("optionsList", optionsList);

        return "option/list";
    }

    @RequestMapping(value="/create")
    private String getCreateOptionPage(Model model) {

        logger.info("Request for getCreateOptionPage()");

        model.addAttribute("option", new OptionDto());

        return "option/create";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addNewOption(@Valid @ModelAttribute("option") OptionDto option,
                             BindingResult result,
                             Model model) {

        logger.info("Request for addNewOption()");

        if (!result.hasErrors()) {
            Long createdOptionId;
            createdOptionId = optionService.add(option);

            if (createdOptionId != null) {
                model.addAttribute("successMessage",
                        "Option has been successfully created with id {" + createdOptionId + "}");

                return getEditDependentAndIncompatibleOptionsPage(createdOptionId, model);
            }
        }

        model.addAttribute("errorMessage", "Option hasn't been created, check correctness of fields");
        model.addAttribute("option", option);
        return "option/create";
    }

    @RequestMapping(value = "/edit/{id}/info")
    public String getEditOptionInfoPage(@PathVariable("id") Long optionId, Model model) {

        logger.info("Request for getEditOptionInfoPage()");

        OptionDto optionDto;
        optionDto = optionService.getOptionById(optionId, true);

        if (optionDto == null) {
            model.addAttribute("errorMessage", "Option with id{" + optionId + "} not exist");
            return getListOfOptions(model);
        }

        model.addAttribute("currentOption", optionDto);

        return "option/edit/info";
    }

    @RequestMapping(value = "/edit/{id}/options")
    public String getEditDependentAndIncompatibleOptionsPage(@PathVariable("id") Long optionId,
                                                             Model model) {

        logger.info("Request for getEditDependentAndIncompatibleOptionsPage()");

        OptionDto optionDto;
        optionDto = optionService.getOptionById(optionId, true);

        if (optionDto == null) {
            model.addAttribute("errorMessage", "Option with id{" + optionId + "} not exist");
            return getListOfOptions(model);
        }

        model.addAttribute("allOptionsList", optionService.getAllPossibleOptions(optionId));
        model.addAttribute("currentOption", optionDto);

        return "option/edit/options";
    }

    @RequestMapping(value = "/edit/{id}/tariffs")
    public String getEditPossibleTariffsPage(@PathVariable("id") Long optionId,
                                                             Model model) {

        logger.info("Request for getEditPossibleTariffsPage()");

        OptionDto optionDto;
        optionDto = optionService.getOptionById(optionId, true);

        if (optionDto == null) {
            model.addAttribute("errorMessage", "Option with id{" + optionId + "} not exist");
            return getListOfOptions(model);
        }

        List<TariffDto> possibleTariffs;
        possibleTariffs = tariffService.getAllTariffs();
        possibleTariffs.removeAll(optionDto.getPossibleTariffs());

        model.addAttribute("allTariffsList", possibleTariffs);
        model.addAttribute("currentOption", optionDto);

        return "option/edit/tariffs";
    }


    @RequestMapping(value = "/edit/{currentOptionId}/dependent_option/add/{dependentOptionId}")
    public String addDependentOption(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("dependentOptionId") Long dependentOptionId,
            Model model) {

        logger.info("Request for addDependentOption()");

        try {
            optionService.addDependentOption(currentOptionId, dependentOptionId);
            model.addAttribute("successMessage", "Option with id{" + dependentOptionId
                    + "} has been successfully added to dependent options");
        } catch (IncompatibleOptionsException e) {
            model.addAttribute("errorMessage", "Option with id{" + dependentOptionId + "} hasn't been" +
                    " added to dependent options. Because " + e.getMessage());
        }

        return getEditDependentAndIncompatibleOptionsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/edit/{currentOptionId}/dependent_option/remove/{dependentOptionId}")
    public String removeDependentOption(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("dependentOptionId") Long dependentOptionId,
            Model model) {

        logger.info("Request for removeDependentOption()");

        optionService.removeDependentOption(currentOptionId, dependentOptionId);

        model.addAttribute("successMessage", "Option with id{" + dependentOptionId
                + "} has been successfully removed from dependent options");

        return getEditDependentAndIncompatibleOptionsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/edit/{currentOptionId}/incompatible_option/add/{incompatibleOptionId}")
    public String addIncompatibleOption(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("incompatibleOptionId") Long incompatibleOptionId,
            Model model) {

        logger.info("Request for addIncompatibleOption()");

        try {
            optionService.addIncompatibleOption(currentOptionId, incompatibleOptionId);
            model.addAttribute("successMessage", "Option with id{" + incompatibleOptionId
                    + "} has been successfully added to incompatible options");
        } catch (IncompatibleOptionsException e) {
            model.addAttribute("errorMessage", "Option with id{" + incompatibleOptionId
                    + "} hasn't been added to incompatible options. " + "Because " + e.getMessage());
        }

        return getEditDependentAndIncompatibleOptionsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/edit/{currentOptionId}/incompatible_option/remove/{incompatibleOptionId}")
    public String removeIncompatibleOption(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("incompatibleOptionId") Long incompatibleOptionId,
            Model model) {

        logger.info("Request for removeIncompatibleOption()");

        optionService.removeIncompatibleOption(currentOptionId, incompatibleOptionId);

        model.addAttribute("successMessage", "Option with id{" + incompatibleOptionId
                + "} has been successfully removed from incompatible options");

        return getEditDependentAndIncompatibleOptionsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/edit/{currentOptionId}/possible_tariffs/add/{tariffId}")
    public String addPossibleTariff(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("tariffId") Long tariffId,
            Model model) {

        logger.info("Request for addPossibleTariff()");

        optionService.addPossibleTariff(currentOptionId, tariffId);

        model.addAttribute("successMessage", "Tariff with id{" + tariffId
                + "} has been successfully added to possible tariffs");

        return getEditPossibleTariffsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/edit/{currentOptionId}/possible_tariffs/remove/{tariffId}")
    public String removePossibleTariff(
            @PathVariable("currentOptionId") Long currentOptionId,
            @PathVariable("tariffId") Long tariffId,
            Model model) {

        logger.info("Request for removePossibleTariff()");

        optionService.removePossibleTariff(currentOptionId, tariffId);

        model.addAttribute("successMessage", "Tariff with id{" + tariffId
                + "} has been successfully removed from possible tariffs");

        return getEditPossibleTariffsPage(currentOptionId, model);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateOption(@Valid @ModelAttribute("option") OptionDto option,
                             BindingResult result,
                             Model model) {

        logger.info("Request for updateOption()");

        if (result.hasErrors()) {
            return "option/edit/info";
        }

        if (optionService.updateInformation(option)) {
            model.addAttribute("successMessage", "Option with id{" + option.getId() + "} has been successfully updated");
            return getEditOptionInfoPage(option.getId(), model);
        }

        model.addAttribute("errorMessage", "Option hasn't been updated");
        return "option/edit/info";
    }

    @RequestMapping(value = "/info/{id}")
    public String getOptionInfoPage(@PathVariable("id") Long optionId,
                             Model model) {

        logger.info("Request for getOptionInfoPage()");

        OptionDto optionDto;
        optionDto = optionService.getOptionById(optionId, true);

        if (optionDto != null) {
            model.addAttribute("currentOption", optionDto);
            return "option/info";
        }

        model.addAttribute("errorMessage", "Option with id{" + optionId + "} not exist");
        return getListOfOptions(model);
    }

    @RequestMapping("/remove/{id}")
    public String removeOption(@PathVariable("id") Long optionId,
                             Model model) {

        logger.info("Request for removeUser()");

        if (optionService.remove(optionId)) {
            model.addAttribute("successMessage", "Option with id{" + optionId + "} has been successfully removed from DB");
        } else {
            model.addAttribute("errorMessage", "Option with id{" + optionId + "} can not be removed");
        }

        return getListOfOptions(model);
    }
}
