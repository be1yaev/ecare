package com.tsystems.ecare.controller;

import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.dto.UserDto;
import com.tsystems.ecare.service.*;
import com.tsystems.ecare.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
* Created by Andrew on 27.09.2014.
*/
@Controller
@RequestMapping("/contract")
public class ContractController {
    private static final Logger logger = Logger.getLogger(ContractController.class);

    @Autowired
    private ContractService contractService;

    @Autowired
    private UserService userService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private OptionService optionService;

    @Autowired
    private SecurityService securityService;


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }

//    @ExceptionHandler(Exception.class)
//    public ModelAndView handleError(HttpServletRequest request, Exception exception) {
//
//        logger.error("Request: " + request.getRequestURL() + " raised " + exception);
//
////        StringBuffer stackTrace = new StringBuffer(50);
////        for (StackTraceElement element : exception.getStackTrace()) {
////            stackTrace.append(element.toString() + "\n");
////        }
//
//        // TODO: add hide input
//        ModelAndView mav = new ModelAndView();
//        mav.addObject("errorReason", exception.getMessage());
//        //mav.addObject("exceptionStackTrace", stackTrace);
//        mav.addObject("errorCode", "400");
//
//        mav.setViewName("error");
//
//        return mav;
//    }

    @RequestMapping(value="")
    public String getMainPage(Model model,
                              HttpSession session) {

        logger.info("Request for getMainPage()");

        return getListOfContracts(model);
    }

    @RequestMapping(value="/list")
    public String getListOfContracts(Model model) {

        logger.info("Request for getListOfContracts()");

        if (securityService.hasRole("ROLE_CLIENT"))
            return getListOfClientContracts(securityService.getUserId(), model);

        List<ContractDto> contractsList;
        contractsList = contractService.getAllContracts();

        model.addAttribute("contractsList", contractsList);

        return "contract/list";
    }

    @RequestMapping(value="/client_contracts_list/{userId}")
    public String getListOfClientContracts(@PathVariable(value = "userId") Long userId,
                                            Model model) {

        logger.info("Request for getListOfClientContracts()");

        List<ContractDto> contractsList;
        contractsList = contractService.getUserContracts(userId);

        model.addAttribute("contractsList", contractsList);

        return "contract/list";
    }

    @RequestMapping(value="/info/{contractId}")
    public String getInfoPage(@PathVariable(value = "contractId") Long contractId,
                              Model model,
                              HttpSession session) {

        logger.info("Request for getInfoPage()");

        ContractDto contract;
        contract = contractService.getContractById(contractId, true);

        if (contract == null) {
            model.addAttribute("errorMessage", "Contract with id{" + contractId + "} has not exist");
            return getListOfContracts(model);
        }

        model.addAttribute("contract", contract);
        return "contract/info";
    }


    @RequestMapping(value="/create")
    public String createNewContract(Model model,
                                         HttpSession session) {

        logger.info("Request for getCreateContractPage()");

        ContractDto contract;
        contract = new ContractDto();

        session.setAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT, contract);

        return getContractSelectUserPage(model, session);
    }


    @RequestMapping(value="/edit/{contractId}")
    public String editExistContract(@PathVariable(value = "contractId") Long contractId,
                                    Model model,
                                    HttpSession session) {

        logger.info("Request for getCreateContractPage()");

        ContractDto contract;
        contract = contractService.getContractById(contractId, true);

        if (contract.getBlockedByManager() && securityService.hasRole("ROLE_CLIENT")) {
            model.addAttribute("errorMessage", "Manager can only editing blocked contract");
            return getListOfContracts(model);
        }

        if (contract == null) {
            model.addAttribute("errorMessage", "Contract with id{" + contractId + "} has not exist");
            return getListOfContracts(model);
        }

        session.setAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT, contract);

        return getContractSelectUserPage(model, session);
    }

    @RequestMapping(value="/edit/select_user")
    public String getContractSelectUserPage(Model model,
                                          HttpSession session) {

        logger.info("Request for getContractSelectUserPage()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        if (securityService.hasRole("ROLE_CLIENT")) {
            return selectUserForEditableContract(null, model, session);
        }

        model.addAttribute("currentUser", contract.getUser());
        model.addAttribute("usersList", userService.getAllUsers(Arrays.asList(User.Role.CLIENT)));

        return "contract/edit/select_user";
    }

    @RequestMapping(value = "/edit/select_user/{userId}")
    public String selectUserForEditableContract(@PathVariable(value = "userId") Long userId,
                                           Model model,
                                           HttpSession session) {

        logger.info("Request for selectUserForEditableContract()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        UserDto user;

        if (securityService.hasRole("ROLE_CLIENT")) {
            user = userService.getUserById(securityService.getUserId(), false);
        } else {
            user = userService.getUserById(userId, false);
        }

        if (user == null) {
            model.addAttribute("errorMessage", "Selected user not exist");
            return getContractSelectUserPage(model, session);
        }

        contract.setUser(user);
        model.addAttribute("successMessage", "User with email{"
                + user.getEmail() + "} has been selected as new contract client");
        session.setAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT, contract);

        return getEditContractInfoPage(model, session);
    }

    @RequestMapping(value="/edit/info")
    public String getEditContractInfoPage(Model model,
                                          HttpSession session) {

        logger.info("Request for getEditContractInfoPage()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        model.addAttribute("contract", contract);

        return "contract/edit/info";
    }

    @RequestMapping(value = "/edit/info/save", method = RequestMethod.POST)
    public String saveEditableContractInfo(@Valid @ModelAttribute("contract") ContractDto contract,
                                        BindingResult result,
                                        Model model,
                                        HttpSession session) {

        logger.info("Request for saveEditableContractInfo()");

        ContractDto contractFromSession;
        contractFromSession = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contractFromSession == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        if (result.hasErrors()) {
            return "contract/edit/info";
        }

        contractFromSession.setPhoneNumber(contract.getPhoneNumber());
        contractFromSession.setBlockedByManager(contract.getBlockedByManager());
        contractFromSession.setBlockedByClient(contract.getBlockedByClient());

        session.setAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT, contractFromSession);
        model.addAttribute("successMessage", "Contract info has been saved");

        return getSelectContractTariffPage(model, session);
    }

    @RequestMapping(value="/edit/select_tariff")
    public String getSelectContractTariffPage(Model model,
                                          HttpSession session) {

        logger.info("Request for getSelectContractTariffPage()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        model.addAttribute("currentTariff", contract.getTariff());
        model.addAttribute("tariffsList", tariffService.getAllTariffs());

        return "contract/edit/select_tariff";
    }

    @RequestMapping(value = "/edit/select_tariff/{tariffId}")
    public String selectContractTariff(@PathVariable(value = "tariffId") Long tariffId,
                                           Model model,
                                           HttpSession session) {

        logger.info("Request for selectContractTariff()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        TariffDto tariff;
        tariff = tariffService.getTariffById(tariffId, true);

        if (tariff == null) {
            model.addAttribute("errorMessage", "Selected tariff not exist");
            return getSelectContractTariffPage(model, session);
        }

        contract.setTariff(tariff);
        contract.setConnectedOptions(new ArrayList<OptionDto>());
        session.setAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT, contract);

        return getSelectContractOptionsPage(model, session);
    }

    @RequestMapping(value="/edit/select_options")
    public String getSelectContractOptionsPage(Model model,
                                                  HttpSession session) {

        logger.info("Request for getSelectContractOptionsPage()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        if (contract.getTariff() == null) {
            model.addAttribute("errorMessage", "Select tariff before");
            return getSelectContractTariffPage(model, session);
        }

        model.addAttribute("connectedOptions", contract.getConnectedOptions());
        model.addAttribute("optionsList", contract.getTariff().getPossibleOptions());

        return "contract/edit/select_options";
    }

    @RequestMapping(value = "/edit/add_option/{optionId}")
    public String addOptionToNewContract(@PathVariable(value = "optionId") Long optionId,
                                             Model model,
                                             HttpSession session) {

        logger.info("Request for addOptionToNewContract()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        if (contract.getConnectedOptions() == null)
            contract.setConnectedOptions(new ArrayList<OptionDto>());

        OptionDto option;
        option = optionService.getOptionById(optionId, false);

        if (!contract.getConnectedOptions().contains(option))
            contract.getConnectedOptions().add(option);

        return getSelectContractOptionsPage(model, session);
    }

    @RequestMapping(value = "/edit/remove_option/{optionId}")
    public String removeOptionFromContract(@PathVariable(value = "optionId") Long optionId,
                                         Model model,
                                         HttpSession session) {

        logger.info("Request for removeOptionFromContract()");

        ContractDto contract;
        contract = (ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);

        if (contract == null) {
            model.addAttribute("errorMessage", "Select editable contract before, or create new");
            return getListOfContracts(model);
        }

        if (contract.getConnectedOptions() == null)
            contract.setConnectedOptions(new ArrayList<OptionDto>());

        OptionDto option;
        option = optionService.getOptionById(optionId, false);

        if (contract.getConnectedOptions().contains(option))
            contract.getConnectedOptions().remove(option);

        return getSelectContractOptionsPage(model, session);
    }

    @RequestMapping(value="/edit/cancel")
    public String cancelEditableContract(Model model,
                                         HttpSession session) {

        logger.info("Request for cancelEditableContract()");

        // TODO: Cancel

        session.removeAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT);
        model.addAttribute("successMessage", "Editable contract has been cleared");

        return getListOfContracts(model);
    }
}
