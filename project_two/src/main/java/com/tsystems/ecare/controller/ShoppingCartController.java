package com.tsystems.ecare.controller;

import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.exception.IncompatibleOptionsException;
import com.tsystems.ecare.service.ShoppingCartService;
import com.tsystems.ecare.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Andrew on 29.09.2014.
 */
@Controller
@RequestMapping("/cart")
public class ShoppingCartController {
    private static final Logger logger = Logger.getLogger(ShoppingCartController.class);

    @Autowired
    ShoppingCartService shoppingCartService;

    @Autowired
    ContractController contractController;


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }

    @RequestMapping(value="")
    public String getMainPage(Model model) {

        logger.info("Request for getMainPage()");

        model.addAttribute("newContractsList", shoppingCartService.getNewContracts());
        model.addAttribute("changedContractsList", shoppingCartService.getChangedContracts());
        model.addAttribute("removedContractsList", shoppingCartService.getRemovedContracts());

        return "cart/contracts_list";
    }

    @RequestMapping(value="/add_contract")
    public String addContractToCart(Model model,
                                    HttpSession session) {

        logger.info("Request for addContractToCart()");

        if (shoppingCartService.addContract((ContractDto) session.getAttribute(Constants.SESSION_ATTR_EDITABLE_CONTRACT))) {
            model.addAttribute(
                    "successMessage",
                    "Contract has been saved into your shopping cart."
            );
        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract hasn't been saved into your shopping cart."
            );
        }

        return contractController.getEditContractInfoPage(model, session);
    }

    @RequestMapping(value="/remove_contract/{contractId}")
    public String addRemovedContractToCart(@PathVariable(value = "contractId") Long contractId,
                                           Model model,
                                           HttpSession session) {

        logger.info("Request for addRemovedContractToCart()");

        if (shoppingCartService.addRemovedContract(contractId)) {
            model.addAttribute(
                    "successMessage",
                    "Contract has been saved into your shopping cart as removed"
            );
        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract hasn't been saved into your shopping cart. Can not removed"
            );
        }

        return contractController.getListOfContracts(model);
    }

    @RequestMapping(value="/remove/changed/{contractId}")
    public String removeChangedContractFromCart(@PathVariable(value = "contractId") Long contractId,
                                             Model model) {

        logger.info("Request for removeChangedContractFromCart()");

        if (shoppingCartService.removeChangedContract(contractId)) {
            model.addAttribute(
                    "successMessage",
                    "Contract has been removed from your shopping cart"
            );
        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract with id{" + contractId + "} hasn't been found in your shopping cart"
            );
        }

        return getMainPage(model);
    }

    @RequestMapping(value="/remove/removed/{contractId}")
    public String removeRemovedContractFromCart(@PathVariable(value = "contractId") Long contractId,
                                                Model model) {

        logger.info("Request for removeChangedContractFromCart()");

        if (shoppingCartService.removeRemovedContract(contractId)) {
            model.addAttribute(
                    "successMessage",
                    "Contract has been removed from your shopping cart"
            );
        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract with id{" + contractId + "} hasn't been found in your shopping cart"
            );
        }

        return getMainPage(model);
    }

    @RequestMapping(value="/remove/new/{phoneNumber}")
    public String removeNewContractFromCart(@PathVariable(value = "phoneNumber") String phoneNumber,
                                             Model model) {

        logger.info("Request for removeNewContractFromCart()");

        if (shoppingCartService.removeNewContract(phoneNumber)) {
            model.addAttribute(
                    "successMessage",
                    "Contract has been removed from your shopping cart"
            );
        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract with phone{" + phoneNumber + "} hasn't been found in your shopping cart"
            );
        }

        return getMainPage(model);
    }

    @RequestMapping(value="/edit/new/{phoneNumber}")
    public String editNewContractFromCart(@PathVariable(value = "phoneNumber") String phoneNumber,
                                            Model model,
                                            HttpSession session) {

        logger.info("Request for editNewContractFromCart()");

        ContractDto contract;
        contract = shoppingCartService.getNewContract(phoneNumber);

        if (contract != null) {
            model.addAttribute(
                    "successMessage",
                    "Contract was taken back from shopping cart"
            );

            return contractController.getEditContractInfoPage(model, session);
        }

        model.addAttribute(
                "errorMessage",
                "Contract with phone{" + phoneNumber + "} hasn't been found in your shopping cart"
        );

        return getMainPage(model);
    }

    @RequestMapping(value="/edit/changed/{contractId}")
    public String editChangedContractFromCart(@PathVariable(value = "contractId") Long contractId,
                                          Model model,
                                          HttpSession session) {

        logger.info("Request for editChangedContractFromCart()");

        ContractDto contract;
        contract = shoppingCartService.getChangedContract(contractId);

        if (contract != null) {
            model.addAttribute(
                    "successMessage",
                    "Contract was taken back from shopping cart"
            );

            return contractController.getEditContractInfoPage(model, session);
        }

        model.addAttribute(
                "errorMessage",
                "Contract with id{" + contractId + "} hasn't been found in your shopping cart"
        );

        return getMainPage(model);
    }

//    @RequestMapping(value="/save/new/{phoneNumber}")
//    public String saveNewContractFromCart(@PathVariable(value = "phoneNumber") String phoneNumber,
//                                          Model model,
//                                          HttpSession session) {
//
//        logger.info("Request for saveNewContractFromCart()");
//
//        ContractDto contract;
//        contract = shoppingCartService.getNewContract(phoneNumber);
//
//        if (contract != null) {
//            model.addAttribute(
//                    "successMessage",
//                    "Contract was taken back from shopping cart"
//            );
//
//            return contractController.getEditContractInfoPage(model, session);
//        }
//
//        model.addAttribute(
//                "errorMessage",
//                "Contract with phone{" + phoneNumber + "} hasn't been found in your shopping cart"
//        );
//
//        return getMainPage(model);
//    }

    @RequestMapping(value="/save/removed/{contractId}")
    public String saveRemovedContractFromCart(@PathVariable(value = "contractId") Long contractId,
                                              Model model) {

        logger.info("Request for saveRemovedContractFromCart()");

        if (shoppingCartService.saveRemovedContract(contractId)) {
            model.addAttribute(
                    "successMessage",
                    "Contract with id{" + contractId + "} has been successfully removed"
            );

        } else {
            model.addAttribute(
                    "errorMessage",
                    "Contract with id{" + contractId + "} hasn't been removed"
            );
        }

        return getMainPage(model);
    }

    @RequestMapping(value="/save/changed/{contractId}")
    public String saveChangedContractFromCart(@PathVariable(value = "contractId") Long contractId,
                                              Model model) {

        logger.info("Request for editChangedContractFromCart()");

        try {
            if (shoppingCartService.saveChangedContract(contractId)) {
                model.addAttribute(
                        "successMessage",
                        "Contract with id{" + contractId + "} has been successfully saved."
                );

                return getMainPage(model);
            }
        } catch (AccessDeniedException | IncompatibleOptionsException | NullPointerException e) {
            model.addAttribute(
                    "errorMessage",
                    "Contract with id{" + contractId + "} hasn't been saved. Error: " + e.getMessage()
            );
        }

        return getMainPage(model);
    }

    @RequestMapping(value="/save/new/{phoneNumber}")
    public String saveNewContractFromCart(@PathVariable(value = "phoneNumber") String phoneNumber,
                                              Model model) {

        logger.info("Request for editChangedContractFromCart()");

        try {
            Long newId;
            newId = shoppingCartService.saveNewContract(phoneNumber);

            model.addAttribute(
                    "successMessage",
                    "Contract with id{" + newId + "} has been successfully created"
            );

            return getMainPage(model);
        } catch (AccessDeniedException | IncompatibleOptionsException | NullPointerException e) {
            model.addAttribute(
                    "errorMessage",
                    "Contract with phone{" + phoneNumber + "} hasn't been saved. Error: " + e.getMessage()
            );
        }

        return getMainPage(model);
    }
}
