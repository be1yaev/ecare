package com.tsystems.ecare.controller;

import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.UserDto;
import com.tsystems.ecare.exception.AccessDeniedException;
import com.tsystems.ecare.service.SecurityService;
import com.tsystems.ecare.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrew on 12.09.2014.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static final Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }

    @RequestMapping(value="")
    public String getMainPage(Model model, HttpSession session) {

        logger.info("Request for getMainPage()");

        if (securityService.hasRole("ROLE_MANAGER") || securityService.hasRole("ROLE_ADMIN")) {
            return getListOfUsers(model);
        }

        return getUserInfo(model, session);
    }

    @RequestMapping("/login")
    public String getLoginForm(
            Model model,
            @RequestParam(required = false) String fail,
            String logout,
            String denied) {

        logger.info("Request for login()");

        if (fail != null) {
            model.addAttribute("errorMessage", "Invalid username or password");
        } else if (logout != null) {
            model.addAttribute("successMessage", "Logged Out successfully");
        } else if (denied != null) {
            model.addAttribute("errorMessage", "Access denied for current user");
        }

        return "user/login";
    }

//    @RequestMapping("/info")
//    public String getUserPage(Model model) {
//
//        logger.info("Request for getUserPage()");
//
//        model.addAttribute("username", "Test");
//        return "user/info";
//    }

    @RequestMapping("/registration")
    public String getUserRegistrationPage(Model model) {

        logger.info("Request for getUserRegistrationPage()");

        model.addAttribute("user", new UserDto());
        return "user/registration";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addNewUser(@Valid @ModelAttribute("user") UserDto user,
                             BindingResult result,
                             Model model) {

        logger.info("Request for addNewUser()");

        if (!result.hasErrors()) {
            if (userService.add(user)) {
                model.addAttribute("successMessage", "User has been successfully registered");
                //redirectAttributes.addFlashAttribute("successMessage", "User {" + user.toString() + "} has been successfully created");
                // Redirect to previous page
                //return getUserRegistrationPage(model);
                return getUserRegistrationPage(model);
            }
        }

        model.addAttribute("errorMessage", "User hasn't been created, check correctness of fields");
        model.addAttribute("user", user);
        return "user/registration";
    }

    @RequestMapping("/access_denied")
    public String getAccessDeniedPage(Model model) {

        logger.info("Request for getAccessDeniedPage()");
        model.addAttribute("errorReason", "Access denied for current user.");
        return "error";
    }

    @RequestMapping(value="/list")
    public String getListOfUsers(Model model) {

        logger.info("Request for getListOfUsers()");

        List<UserDto> usersList;

        if (securityService.hasRole("ROLE_ADMIN")) {
            usersList = userService.getAllUsers(Arrays.asList(User.Role.CLIENT, User.Role.MANAGER));
        } else {
            usersList = userService.getAllUsers(Arrays.asList(User.Role.CLIENT));
        }

        model.addAttribute("usersList", usersList);

        return "user/list";
    }

    @RequestMapping(value="/info")
    public String getUserInfo(Model model, HttpSession session) {

        logger.info("Request for getUserInfo()");

        Long userId;
        userId = (Long) session.getAttribute("currentUserId");

        return getUserInfoById(userId, model);
    }

    @RequestMapping(value="/info/{id}")
    public String getUserInfoById(@PathVariable("id") Long userId,
                               Model model) {

        logger.info("Request for getUserInfoById()");

        UserDto userInformation = userService.getUserById(userId, true);
        model.addAttribute("user", userInformation);

        return "user/info";
    }


    @RequestMapping(value="/edit")
    public String getEditUserPage(Model model, HttpSession session) {

        logger.info("Request for getEditUserPage()");

        Long userId;
        userId = (Long) session.getAttribute("currentUserId");

        return getEditUserPageById(userId, model);
    }

    @RequestMapping(value="/edit/{id}")
    public String getEditUserPageById(@PathVariable("id") Long userId,
                                   Model model) {

        logger.info("Request for getEditUserPageById()");

        UserDto userInformation = userService.getUserById(userId, false);
        model.addAttribute("user", userInformation);

        return "user/edit";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateUser(@Valid @ModelAttribute("user") UserDto user,
                             BindingResult result,
                             @RequestParam(value="passwordConfirmation", required=false) String passwordConfirmation,
                             Model model,
                             HttpSession session) throws AccessDeniedException {

        logger.info("Request for updateUser()");

        if (result.hasErrors()) {
            return "user/edit";
        }

        try {
            if (userService.update(user, passwordConfirmation)) {
                model.addAttribute("successMessage", "Information of user with id{" + user.getId()
                        + "} has been successfully updated");
                return getMainPage(model, session);
            }
        } catch (AccessDeniedException e) {
            // TODO: Add error403 page and message for this exc
            model.addAttribute("errorReason", e.getMessage());
            return "error";
        }

        model.addAttribute("errorMessage", "User has not been updated");
        return "user/edit";
    }

    @RequestMapping("/remove/{id}")
    public String removeUser(@PathVariable("id") Long userId,
                             Model model) throws AccessDeniedException {

        logger.info("Request for removeUser()");

        if (userService.remove(userId)) {
            model.addAttribute("successMessage", "User with id{" + userId + "} has been successfully removed from DB");
        } else {
            model.addAttribute("errorMessage", "User with id{" + userId + "} does not exist");
        }

        return getListOfUsers(model);
    }
}
