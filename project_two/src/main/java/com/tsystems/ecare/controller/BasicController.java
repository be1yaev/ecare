package com.tsystems.ecare.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by Andrew on 20.07.2014.
 */
@Controller
@RequestMapping("")
public class BasicController {
    private static final Logger logger = Logger.getLogger(BasicController.class);

    @RequestMapping(value = {"/", "/index**" })
    public String welcome(ModelMap model, Principal principal) {

        logger.info("Request for welcome()");

        return "index";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception exception) {

        logger.error("Request: " + request.getRequestURL() + " raised " + exception);

//        StringBuffer stackTrace = new StringBuffer(50);
//        for (StackTraceElement element : exception.getStackTrace()) {
//            stackTrace.append(element.toString() + "\n");
//        }

        // TODO: add hide input
        ModelAndView mav = new ModelAndView();
        mav.addObject("errorReason", exception.getMessage());
        //mav.addObject("exceptionStackTrace", stackTrace);
        mav.addObject("errorCode", "400");

        mav.setViewName("error");

        return mav;
    }
}
