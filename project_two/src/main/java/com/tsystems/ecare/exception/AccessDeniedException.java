package com.tsystems.ecare.exception;

/**
 * Created by Andrew on 23.09.2014.
 */
public class AccessDeniedException extends Exception {
    public AccessDeniedException() {
        super();
    }

    public AccessDeniedException(String message) {
        super(message);
    }
}
