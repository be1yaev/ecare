package com.tsystems.ecare.exception;

/**
 * Created by Andrew on 27.09.2014.
 */
public class IncompatibleOptionsException extends Exception {
    private Long targetOptionId;
    private Long incompatibleOptionId;

    public IncompatibleOptionsException(String message) {
        super(message);
    }

    public IncompatibleOptionsException(Long targetOptionId, Long incompatibleOptionId) {
        super("option id{" + targetOptionId + "}" +
                " incompatible with id{" + incompatibleOptionId + "}");

        this.targetOptionId = targetOptionId;
        this.incompatibleOptionId = incompatibleOptionId;
    }

    public Long getTargetOptionId() {
        return targetOptionId;
    }

    public Long getIncompatibleOptionId() {
        return incompatibleOptionId;
    }
}
