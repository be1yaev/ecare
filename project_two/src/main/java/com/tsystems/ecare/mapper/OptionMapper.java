package com.tsystems.ecare.mapper;

import com.tsystems.ecare.domain.Option;
import com.tsystems.ecare.dto.OptionDto;

import java.util.List;

/**
 * Created by Andrew on 27.09.2014.
 */
public class OptionMapper {
    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromDtoToEntity(OptionDto dto, Option entity) {
        entity.setId(dto.getId());
        entity.setTitle(dto.getTitle());
        entity.setPrice(dto.getPrice());
        entity.setDescription(dto.getDescription());
        entity.setConnectionCost(dto.getConnectionCost());
    }

    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromEntityToDto(OptionDto dto, Option entity) {
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setPrice(entity.getPrice());
        dto.setDescription(entity.getDescription());
        dto.setConnectionCost(entity.getConnectionCost());
    }

    /**
     * Convert List<Entity> to List<DTO>
     * @param dtos list of DTO's
     * @param entityes list of Entity's
     */
    public static void fromListEntityesToDtos(List<OptionDto> dtos, List<Option> entityes) {
        for (Option entity : entityes) {
            OptionDto dto;
            dto = new OptionDto();

            fromEntityToDto(dto, entity);

            dtos.add(dto);
        }
    }

//    public static List<Option> fromListDtoToEntity(List<OptionDto> addedIncompatibleOptions) {
//        List<Option> result;
//        result = new ArrayList<Option>();
//
//        for (OptionDto dto : addedIncompatibleOptions) {
//            Long id = dto.getId();
//            if (id != null) {
//
//            } else {
//                result.add(Option.getFromDTO(dto));
//            }
//        }
//
//        return result;
//    }
}
