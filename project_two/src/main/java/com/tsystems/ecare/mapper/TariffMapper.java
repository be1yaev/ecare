package com.tsystems.ecare.mapper;

import com.tsystems.ecare.domain.Tariff;
import com.tsystems.ecare.dto.TariffDto;

import java.util.List;

/**
 * Created by Andrew on 27.09.2014.
 */
public class TariffMapper {
    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromDtoToEntity(TariffDto dto, Tariff entity) {
        entity.setId(dto.getId());
        entity.setTitle(dto.getTitle());
        entity.setDescription(dto.getDescription());
    }

    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromEntityToDto(TariffDto dto, Tariff entity) {
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
    }

    /**
     * Convert List<Entity> to List<DTO>
     * @param dtos list of DTO's
     * @param entityes list of Entity's
     */
    public static void fromListEntityesToDtos(List<TariffDto> dtos, List<Tariff> entityes) {
        for (Tariff entity : entityes) {
            TariffDto dto;
            dto = new TariffDto();

            fromEntityToDto(dto, entity);

            dtos.add(dto);
        }
    }

//    public static List<Option> fromListDtoToEntity(List<OptionDto> addedIncompatibleOptions) {
//        List<Option> result;
//        result = new ArrayList<Option>();
//
//        for (OptionDto dto : addedIncompatibleOptions) {
//            Long id = dto.getId();
//            if (id != null) {
//
//            } else {
//                result.add(Option.getFromDTO(dto));
//            }
//        }
//
//        return result;
//    }
}
