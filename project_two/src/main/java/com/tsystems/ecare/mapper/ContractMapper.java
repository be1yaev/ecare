package com.tsystems.ecare.mapper;

import com.tsystems.ecare.domain.Contract;
import com.tsystems.ecare.dto.ContractDto;
import com.tsystems.ecare.dto.OptionDto;
import com.tsystems.ecare.dto.TariffDto;
import com.tsystems.ecare.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 27.09.2014.
 */
public class ContractMapper {
    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromDtoToEntity(ContractDto dto, Contract entity) {
        entity.setId(dto.getId());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setBlockedByManager(dto.getBlockedByManager());
        entity.setBlockedByClient(dto.getBlockedByClient());
    }

    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromEntityToDto(ContractDto dto, Contract entity) {
        dto.setId(entity.getId());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setBlockedByManager(entity.getBlockedByManager());
        dto.setBlockedByClient(entity.getBlockedByClient());

        dto.setTariff(new TariffDto());
        TariffMapper.fromEntityToDto(
                dto.getTariff(),
                entity.getTariff()
        );

        dto.setUser(new UserDto());
        UserMapper.fromEntityToDto(
                dto.getUser(),
                entity.getClient()
        );
    }

    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromEntityToDtoWithLists(ContractDto dto, Contract entity) {
        fromEntityToDto(dto, entity);

        dto.setConnectedOptions(new ArrayList<OptionDto>());
        OptionMapper.fromListEntityesToDtos(
                dto.getConnectedOptions(),
                entity.getConnectedOptions()
        );
    }

    /**
     * Convert List<Entity> to List<DTO> with lists
     * @param dtos list of DTO's
     * @param entityes list of Entity's
     */
    public static void fromListEntityesToDtosWithLists(List<ContractDto> dtos, List<Contract> entityes) {
        for (Contract entity : entityes) {
            ContractDto dto;
            dto = new ContractDto();

            fromEntityToDtoWithLists(dto, entity);

            dtos.add(dto);
        }
    }

    /**
     * Convert List<Entity> to List<DTO>
     * @param dtos list of DTO's
     * @param entityes list of Entity's
     */
    public static void fromListEntityesToDtos(List<ContractDto> dtos, List<Contract> entityes) {
        for (Contract entity : entityes) {
            ContractDto dto;
            dto = new ContractDto();

            fromEntityToDto(dto, entity);

            dtos.add(dto);
        }
    }
}
