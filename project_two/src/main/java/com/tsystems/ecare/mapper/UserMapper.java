package com.tsystems.ecare.mapper;

import com.tsystems.ecare.domain.User;
import com.tsystems.ecare.dto.UserDto;

/**
 * Created by Andrew on 17.09.2014.
 */
public class UserMapper {
    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromDtoToEntity(UserDto dto, User entity) {
        entity.setId(dto.getId());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setFirstname(dto.getFirstname());
        entity.setSurname(dto.getSurname());
        entity.setPatronymic(dto.getPatronymic());
        entity.setPassportNumber(dto.getPassportNumber());
        entity.setPassportDescription(dto.getPassportDescription());
        entity.setCountry(dto.getCountry());
        entity.setCity(dto.getCity());
        entity.setAddress(dto.getAddress());
        entity.setRole(getEntityRole(dto.getRole()));
    }

    /**
     *
     * @param dto
     * @param entity
     */
    public static void fromEntityToDto(UserDto dto, User entity) {
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setFirstname(entity.getFirstname());
        dto.setSurname(entity.getSurname());
        dto.setPatronymic(entity.getPatronymic());
        dto.setPassportNumber(entity.getPassportNumber());
        dto.setPassportDescription(entity.getPassportDescription());
        dto.setCountry(entity.getCountry());
        dto.setCity(entity.getCity());
        dto.setAddress(entity.getAddress());
        dto.setRole(getDtoRole(entity.getRole()));
    }

    /**
     *
     * @param roleFromDto
     * @return
     */
    public static User.Role getEntityRole(UserDto.Role roleFromDto) {
        if (roleFromDto == null)
            return null;

        return User.Role.valueOf(roleFromDto.toString());
    }

    /**
     *
     * @param roleFromEntity
     * @return
     */
    public static UserDto.Role getDtoRole(User.Role roleFromEntity) {
        if (roleFromEntity == null)
            return null;

        return UserDto.Role.valueOf(roleFromEntity.toString());
    }
}
