//package com.tsystems.ecare.server;
//
//import com.tsystems.ecare.server.dao.UserDAO;
//import com.tsystems.ecare.server.domain.User;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
//
///**
// * Created by Andrew on 17.08.2014.
// */
//public class UserDAOTest {
//    static private EntityManagerFactory emf;
//    private static EntityManager em;
//
//    private static UserDAO userDAO;
//
//    private static User user;
//
//    @BeforeClass
//    public static void initAndAddNewUser() {
//        emf = Persistence.createEntityManagerFactory("eCare");
//        em = emf.createEntityManager();
//        userDAO = new UserDAO(em);
//
//        userDAO.beginTransaction();
//        user = new User(
//                "surname",
//                "Andrew",
//                "patronymic",
//                "passportNumber",
//                "passportDescription",
//                "country",
//                "city",
//                "address",
//                "andreybelyaev@test.com",
//                "password",
//                User.Role.CLIENT,
//                null
//        );
//        userDAO.addNewUser(
//                new User(
//                        "surname",
//                        "Andrew",
//                        "patronymic",
//                        "passport",
//                        "passportDescription",
//                        "country",
//                        "city",
//                        "address",
//                        "andreybelyaev@test.com",
//                        "password",
//                        User.Role.CLIENT,
//                        null
//                )
//        );
//        userDAO.commitTransaction();
//
//        user = em.find(User.class, user.getId());
//        //Assert.assertEquals(em.find(User.class, user.getId()), user);
//        em.find(User.class, user.getId()).setSurname("Belyaev");
//    }
//
//    @Test
//    public void testGetUserById() {
//        Assert.assertEquals(userDAO.getUserById(user.getId()), user);
//    }
//
//    @Test
//    public void testGetUserByEmail() {
//        Assert.assertEquals(userDAO.getUserByEmail(user.getEmail()), user);
//    }
//
//    @Test
//    public void testGetUserByNonExistentEmail() {
//        Assert.assertEquals(null, userDAO.getUserByEmail("sadfghjdsdadefrgthd"));
//    }
//
//    @Test
//    public void testGetNonExistentUser() {
//        Assert.assertEquals(userDAO.getUserById(9999999L), null);
//    }
//
//    @AfterClass
//    public static void deletingUserAndCloseEntityFactory() {
//        user = em.find(User.class, user.getId());
//
//        userDAO.removeUser(user);
//
//        em.close();
//        emf.close();
//    }
//}
