package com.tsystems.ecare.server.service;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.server.dao.ContractDAO;
import com.tsystems.ecare.server.domain.Contract;
import com.tsystems.ecare.server.domain.Tariff;
import com.tsystems.ecare.server.domain.User;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class ContractService {
    private static final Logger logger = Logger.getLogger(ContractService.class);
    private static ContractService serviceInstance;
    private static ContractDAO contractDAO = null;

    public static void setEntityManager(EntityManager em) {
        contractDAO = new ContractDAO(em);
    }

    public synchronized static ContractService getInstance() {
        if (serviceInstance == null) {
            if (contractDAO == null)
                throw new IllegalStateException("EntityManager has not set.");

            serviceInstance = new ContractService();
        }
        return serviceInstance;
    }

    private ContractService() {

    }

    public Command getContractsList(GetContractsListCommand command) {
        List<Contract> contracts;

        if (command.getLikePhoneNumber() != null)
            contracts = contractDAO.getContractsLikePhoneNumber(command.getLikePhoneNumber());
        else
            contracts = contractDAO.getAllContracts();

        List<ContractDTO> result = new ArrayList<ContractDTO>();
        for (Contract c : contracts) {
            result.add(c.toDTO());
        }

        return new ContractsListCommand(result);
    }

    public FullContractDataCommand getFullContractData(GetFullContractDataRequestCommand command) {
        Contract contract;
        contract = contractDAO.getContractById(command.getContractId());

        Tariff tariff;
        User user;
        List<OptionDTO> options;

        tariff = contract.getTariff();
        user = contract.getClient();
        options = OptionService.getInstance().getOptionsDtoListFromList(contract.getConnectedOptions());

        logger.info(contract.toDTO());
        logger.info(options);
        logger.info(tariff != null ? tariff.toDTO() : null);
        logger.info(user != null ? user.toDTO() : null);

        FullContractDataCommand result;
        result = new FullContractDataCommand(
                contract.toDTO(),
                options,
                tariff != null ? tariff.toDTO() : null,
                user != null ? user.toDTO() : null
        );
        return result;
    }

    public CreateContractConfirmationCommand createNewContract(CreateContractCommand command) {
        CreateContractConfirmationCommand resultCommand;

        contractDAO.beginTransaction();
        resultCommand = createNewContractWithoutTransaction(command);

        if (resultCommand.isCreateConfirm())
            contractDAO.commitTransaction();
        else
            contractDAO.rollbackTransaction();

        return resultCommand;
    }

    public CreateContractConfirmationCommand createNewContractWithoutTransaction(CreateContractCommand command) {
        if (contractDAO.getContractByPhoneNumber(command.getContractDTO().getPhoneNumber()) == null) {

            Contract contract;
            contract = Contract.getFromDTO(command.getContractDTO());

            boolean resultAdding;
            resultAdding = contractDAO.addNewContract(contract);

            contract.addTariff(
                    TariffService.getInstance().getTariffById(
                            command.getCurrentTariff().getId()
                    )
            );
            contract.addClient(
                    UserService.getInstance().getUserById(
                            command.getCurrentUser().getId()
                    )
            );
            contract.addConnectedOptions(
                    OptionService.getInstance().getOptionsListFromDtoList(
                            command.getAddedOptions()
                    )
            );

            if (OptionService.getInstance().checkForCompatibility(contract.getConnectedOptions())) {
                if (resultAdding) {
                    return new CreateContractConfirmationCommand(true, contract.toDTO());
                }
            } else {
                CreateContractConfirmationCommand resultCommand;
                resultCommand = new CreateContractConfirmationCommand(false, null);
                resultCommand.setMessage("Error checking for compatibility options");
                return resultCommand;
            }
        }

        CreateContractConfirmationCommand resultCommand;
        resultCommand = new CreateContractConfirmationCommand(false, null);
        resultCommand.setMessage("Error when adding");
        return resultCommand;
    }

    public UpdateResultCommand updateContract(UpdateContractCommand command) {
        contractDAO.beginTransaction();

        UpdateResultCommand resultCommand;
        resultCommand = updateContractWithoutTransaction(command);

        if (resultCommand.getUpdatedSuccessfully())
            contractDAO.commitTransaction();
        else
            contractDAO.rollbackTransaction();

        return resultCommand;
    }

    public UpdateResultCommand updateContractWithoutTransaction(UpdateContractCommand command) {
        Contract contract;
        contract = contractDAO.getContractById(command.getUpdatedContractDTO().getId());

        contract.update(command.getUpdatedContractDTO());
        contract.addTariff(
                TariffService.getInstance().getTariffById(
                        command.getCurrentTariff().getId()
                )
        );

        contract.addConnectedOptions(
                OptionService.getInstance().getOptionsListFromDtoList(
                        command.getAddedOptions())
        );
        contract.removeConnectedOptions(
                OptionService.getInstance().getOptionsListFromDtoList(
                        command.getRemovedOptions())
        );

        if (OptionService.getInstance().checkForCompatibility(contract.getConnectedOptions())) {
            contractDAO.updateContract(contract);
            return new UpdateResultCommand(true);
        } else {
            UpdateResultCommand resultCommand;
            resultCommand = new UpdateResultCommand(false);
            resultCommand.setMessage("Error checking for compatibility options");
            return resultCommand;
        }
    }

    public DeleteResultCommand deleteContract(DeleteContractCommand command) {
        contractDAO.beginTransaction();

        Contract contract;
        contract = contractDAO.getContractById(command.getContractId());
        contractDAO.removeContract(contract);

        contractDAO.commitTransaction();

        if (contractDAO.getContractById(command.getContractId()) == null)
            return new DeleteResultCommand(true);
        else
            return new DeleteResultCommand(false);
    }

    public UpdateResultCommand updateAndCreateContractsFromCart(UpdateAndCreateContractsFromCartCommand command) {
        StringBuilder errors = new StringBuilder();

        contractDAO.beginTransaction();

        for (CreateContractCommand c : command.getNewContracts()) {
            try {
                CreateContractConfirmationCommand result = createNewContractWithoutTransaction(c);

                if (!result.isCreateConfirm()) {
                    errors.append("Error in new contract with phone number {"
                            + c.getContractDTO().getPhoneNumber() + "}: " + result.getMessage() + "\n");
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                errors.append("Error in new contract with phone number {"
                        + c.getContractDTO().getPhoneNumber() + "}: " + e.getMessage() + "\n");
            }
        }

        for (UpdateContractCommand c : command.getUpdatedContracts()) {
            try {
                UpdateResultCommand result = updateContractWithoutTransaction(c);

                if (!result.getUpdatedSuccessfully()) {
                    errors.append("Error in new contract with phone number {"
                            + c.getUpdatedContractDTO().getPhoneNumber() + "}: " + result.getMessage() + "\n");
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                errors.append("Error in updated contract with phone number {"
                        + c.getUpdatedContractDTO().getPhoneNumber() + "}: " + e.getMessage() + "\n");
            }
        }

        if (errors.length() == 0) {
            contractDAO.commitTransaction();
            return new UpdateResultCommand(true);
        } else {
            contractDAO.rollbackTransaction();
            UpdateResultCommand resultCommand;
            resultCommand = new UpdateResultCommand(false);
            resultCommand.setMessage(errors.toString());
            return resultCommand;
        }
    }

    public GetUserResultCommand getUserByContractId(GetUserByContractIdCommand command) {
        Contract contract;
        contract = contractDAO.getContractById(command.getContractId());

        if (contract != null) {
            return new GetUserResultCommand(contract.getClient().toDTO());
        }
        return new GetUserResultCommand(null);
    }
}
