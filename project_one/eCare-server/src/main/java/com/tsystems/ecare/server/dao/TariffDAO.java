package com.tsystems.ecare.server.dao;

import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.server.domain.Tariff;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class TariffDAO extends AbstractDAO {
    private static final Logger logger = Logger.getLogger(TariffDAO.class.getName());

    public TariffDAO(EntityManager em) {
        super(em);
    }

    /**
     * Adding new tariff to DB
     * @param tariff not persisted new tariff
     */
    public boolean addNewTariff(Tariff tariff) {
        if (validator.validate(tariff).size() == 0) {
            if (getTariffByTitle(tariff.getTitle()) == null) {
                em.persist(tariff);
                logger.info("Tariff has been added to db: " + tariff);
                return true;
            }
        }

        logger.info("Tariff has not added to db: " + tariff);
        return false;
    }

    /**
     * Find tariff by ID
     * @param tariffId tariff ID
     * @return founded Tariff or null, if Tariff not found
     */
    public Tariff getTariffById(Long tariffId) {

        Tariff tariff;
        tariff = em.find(Tariff.class, tariffId);

        logger.info("Result of find tariff by ID{" + tariffId + "}: " + tariff);
        return tariff;
    }

    /**
     * Find tariff by title
     * @param tariffTitle string with tariff title
     * @return founded Tariff or null, if Tariff not found
     */
    public Tariff getTariffByTitle(String tariffTitle) {

        List<Tariff> result;
        result = em.createQuery("SELECT tariff FROM Tariff tariff WHERE tariff.title=:title")
                .setParameter("title", tariffTitle)
                .getResultList();

        if (result != null && result.size() != 0) {
            logger.info("Tariff has been found by title{'" + tariffTitle + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("Tariff with title{'" + tariffTitle + "'} not found");
            return null;
        }
    }

    /**
     * Find all tariffs from DB
     * @return List<Tariff> with all tariffs or null, if list is empty
     */
    public List<Tariff> getAllTariffs() {
        return em.createQuery("SELECT tariff FROM Tariff tariff")
                .getResultList();
    }

    /**
     * Remove tariff from DB
     * @param tariff tariff for removing
     * @return true if tariff has been removed, false otherwise
     */
    // TODO: Add exception handler for non existent tariff
    public boolean removeTariff(Tariff tariff) {
        em.remove(tariff);

        logger.info("Tariff has been removed: " + tariff);

        return true;
    }

    public List<Tariff> getTariffsListFromDtoList(List<TariffDTO> tariffs) {
        List<Tariff> result;
        result = new ArrayList<Tariff>();

        for (TariffDTO dto : tariffs) {
            Long id = dto.getId();
            if (id != null) {
                result.add(getTariffById(id));
            } else {
                result.add(Tariff.getFromDTO(dto));
            }
        }

        return result;
    }

    public void updateTariff(Tariff tariff) {
        em.merge(tariff);
    }
}
