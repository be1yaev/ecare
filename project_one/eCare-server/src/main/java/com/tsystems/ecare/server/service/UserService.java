package com.tsystems.ecare.server.service;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.UserDTO;
import com.tsystems.ecare.server.dao.UserDAO;
import com.tsystems.ecare.server.domain.Contract;
import com.tsystems.ecare.server.domain.User;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);
    private static UserService serviceInstance;
    private static UserDAO userDAO = null;

    public static void setEntityManager(EntityManager em) {
        userDAO = new UserDAO(em);
    }

    public synchronized static UserService getInstance() {
        if (serviceInstance == null) {
            if (userDAO == null)
                throw new IllegalStateException("EntityManager has not set.");

            serviceInstance = new UserService();
        }
        return serviceInstance;
    }

    private UserService() {
    }

    /**
     *
     * @param verificationCommand
     * @return
     */
    public LoginConfirmationCommand verificationUser(UserVerificationCommand verificationCommand) {
        User user;
        user = userDAO.getUserByEmail(verificationCommand.getEmail());

        if (user != null) {
            if (user.getPassword().equals(verificationCommand.getPassword())) {
                return new LoginConfirmationCommand(true, user.toDTO());
            }
        }

        return new LoginConfirmationCommand(false, null);
    }

    /**
     *
     * @param registrationCommand
     * @return
     */
    public UserRegistrationConfirmationCommand registrationUser(UserRegistrationCommand registrationCommand) {
        registrationCommand.getUserInfo().setRole(UserDTO.Role.CLIENT);

        userDAO.beginTransaction();
        User user;
        user = User.getFromDTO(registrationCommand.getUserInfo());
        user.setPassword(registrationCommand.getPassword());

        if (userDAO.addNewUser(user)) {
            userDAO.commitTransaction();
            return new UserRegistrationConfirmationCommand(true, user.getId());
        } else {
            userDAO.rollbackTransaction();
            return new UserRegistrationConfirmationCommand(false, null);
        }
    }

    public ContractsListCommand getUserContractsList(GetUserContractsListCommand command) {
        User user;
        user = userDAO.getUserById(command.getUserId());

        List<ContractDTO> result = new ArrayList<ContractDTO>();
        for (Contract contract : user.getContracts()) {
            result.add(contract.toDTO());
        }

        return new ContractsListCommand(result);
    }

    public UsersListCommand getUsersList(GetUsersListCommand command) {
        List<User> users;
        users = userDAO.getAllUsers();

        List<UserDTO> result = new ArrayList<UserDTO>();
        for (User user : users) {
            result.add(user.toDTO());
        }

        return new UsersListCommand(result);
    }

    public User getUserById(Long id) {
        return userDAO.getUserById(id);
    }

    public Command getUserById(GetUserByIdCommand command) {
        return new GetUserResultCommand(userDAO.getUserById(command.getUserId()).toDTO());
    }
}
