package com.tsystems.ecare.server.dao;

import com.tsystems.ecare.server.domain.Contract;
import com.tsystems.ecare.server.domain.User;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.*;

public class UserDAO extends AbstractDAO {
    private static final Logger logger = Logger.getLogger(UserDAO.class.getName());

    public UserDAO(EntityManager em) {
        super(em);
    }

//    /**
//     * Adding new contract to user
//     * @param user persisted user for adding contract
//     * @param contract new contract object
//     */
//    public void addContractToUser(User user, Contract contract) {
//
//        em.getTransaction().begin();
//        em.persist(contract);
//        user.addContract(contract);
//        em.getTransaction().commit();
//
//        logger.info("Contract has been added to user: " + contract + ", " + user);
//    }

    /**
     * Adding new user to DB
     * @param user not persisted new user
     */
    public boolean addNewUser(User user) {
        if (validator.validate(user).size() == 0) {
            if (getUserByEmail(user.getEmail()) == null) {
                em.persist(user);
                logger.info("User has been added to db: " + user);
                return true;
            }
        }

        logger.info("User has not added to db: " + user);
        return false;
    }

    /**
     * Find user by ID
     * @param userId user ID
     * @return founded User or null, if User not found
     */
    public User getUserById(Long userId) {

        User user;
        user = em.find(User.class, userId);

        logger.info("Result of find user by ID{" + userId + "}: " + user);
        return user;
    }

    /**
     * Find user by E-mail
     * @param email string with e-mail
     * @return founded User or null, if User not found
     */
    public User getUserByEmail(String email) {

        List<User> result;
        result = em.createQuery("SELECT user FROM User user WHERE user.email=:email")
                .setParameter("email", email)
                .getResultList();

        if (result != null && result.size() != 0) {
            logger.info("User has been found by email{'" + email + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("User with email{'" + email + "'} not found");
            return null;
        }
    }

    /**
     * Find all users from DB
     * @return List<User> with all users or null, if list is empty
     */
    public List<User> getAllUsers() {
        return em.createQuery("SELECT user " + "FROM User user")
                .getResultList();
    }

    /**
     * Remove user from DB
     * @param user user for removing
     * @return true if user has been removed, false otherwise
     */
    // TODO: Add exception handler for non existent user
    public boolean removeUser(User user) {
        em.remove(user);
        logger.info("User has been removed: " + user);
        return true;
    }

//    /**
//     * Find all users from DB, who have telephone number like arg.
//     * @param telephoneNumber telephone number for find
//     * @return List<User> with users or null, if list is empty
//     */
//    public List<User> getUsersWithTelephoneNumber(String telephoneNumber) {
//        List<Contract> contracts;
//        contracts = em.createQuery("SELECT contract " +
//                "FROM Contract contract " +
//                "WHERE contract.telephoneNumber " +
//                "LIKE :number")
//                .setParameter("number", telephoneNumber)
//                .getResultList();
//
//        Set<User> users = new TreeSet<User>();
//        for (Contract c : contracts) {
//            users.add(c.getClient());
//        }
//
//        return new ArrayList<User>(users);
//    }
}
