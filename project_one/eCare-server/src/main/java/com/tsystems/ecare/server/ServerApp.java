package com.tsystems.ecare.server;

import com.tsystems.ecare.server.connector.ConnectionListener;
import com.tsystems.ecare.server.service.ContractService;
import com.tsystems.ecare.server.service.OptionService;
import com.tsystems.ecare.server.service.TariffService;
import com.tsystems.ecare.server.service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrew on 20.08.2014.
 */
public class ServerApp {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("eCare");;
        EntityManager em = emf.createEntityManager();

        UserService.setEntityManager(em);
        ContractService.setEntityManager(em);
        OptionService.setEntityManager(em);
        TariffService.setEntityManager(em);

        ConnectionListener listener = new ConnectionListener();
        ExecutorService listenerExecutor = Executors.newSingleThreadExecutor();

        listenerExecutor.execute(listener);
    }
}
