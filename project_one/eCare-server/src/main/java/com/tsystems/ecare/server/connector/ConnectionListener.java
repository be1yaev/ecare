package com.tsystems.ecare.server.connector;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Server connection listener for receive and handle client connections
 */
public class ConnectionListener implements Runnable {
    private static final Logger logger = Logger.getLogger(ConnectionListener.class);

    private EntityManager em;
    private EntityManagerFactory emf;

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private final int SERVER_PORT = 9999;

    private ExecutorService threadPoolExecutor;
    private final int THREAD_POOL_CAPACITY = 5;

    public ConnectionListener() {
        emf = Persistence.createEntityManagerFactory("eCare");
        em = emf.createEntityManager();

        threadPoolExecutor = Executors.newFixedThreadPool(THREAD_POOL_CAPACITY);
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(SERVER_PORT);

            while (!Thread.currentThread().isInterrupted()) {
                logger.info("Waiting for connections...");

                // Waiting for connection
                clientSocket = serverSocket.accept();
                logger.info("Client has been connected to server: " + clientSocket);

                // Handle
                Runnable task = new ClientConnectionHandler(clientSocket, em);
                threadPoolExecutor.execute(task);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        logger.info("Thread has been interrupted.");
        threadPoolExecutor.shutdown();
    }
}
