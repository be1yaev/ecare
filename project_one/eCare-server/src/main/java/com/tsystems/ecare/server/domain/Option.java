package com.tsystems.ecare.server.domain;

import com.tsystems.ecare.common.dto.OptionDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="table_option")
public class Option {

    @Id
    @Column(name="option_id")
    @GeneratedValue
    private Long id;

    @NotNull(message = "Field \"Option title\" can not be empty")
    @Size(min = 1, max = 50, message = "Length of \"Tariff title\" must be between {2} and {1} characters")
    @Column(name = "title", unique = true)
    private String title;

    @NotNull(message = "Field \"Option description\" can not be empty")
    @Size(min = 1, max = 250, message = "Length of \"Tariff description\" must be between {2} and {1} characters")
    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private Float price;

    @Column(name = "connection_cost")
    private Float connectionCost;

    @ManyToMany(mappedBy = "possibleOptions")
    private List<Tariff> possibleTariffs;


    @ManyToMany
    @JoinTable(name="relation_incompatible_options",
            joinColumns=@JoinColumn(name="option_id"),
            inverseJoinColumns=@JoinColumn(name="incompatible_option_id"))
    private List<Option> incompatibleOptions;

    @ManyToMany(mappedBy = "connectedOptions")
    private List<Contract> connectedContracts;
//    @ManyToMany(mappedBy = "incompatibleOptions")
//    private List<Option> inverseIncompatibleOptions;

    @ManyToMany
    @JoinTable(name="relation_dependent_options",
            joinColumns=@JoinColumn(name="option_id"),
            inverseJoinColumns=@JoinColumn(name="dependent_option_id"))
    private List<Option> dependentOptions;

    public void addIncompatibleOption(Option incompatibleOption) {
        if (incompatibleOptions == null)
            incompatibleOptions = new ArrayList<Option>();

        if (!incompatibleOptions.contains(incompatibleOption))
            incompatibleOptions.add(incompatibleOption);

        if (incompatibleOption.getIncompatibleOptions() == null)
            incompatibleOption.setIncompatibleOptions(new ArrayList<Option>());

        if (!incompatibleOption.getIncompatibleOptions().contains(this))
            incompatibleOption.getIncompatibleOptions().add(this);
    }

    public void addIncompatibleOptions(List<Option> options) {
        for (Option o : options) {
            addIncompatibleOption(o);
        }
    }

    public void addDependentOption(Option dependentOption) {
        if (dependentOptions == null)
            dependentOptions = new ArrayList<Option>();

        if (!dependentOptions.contains(dependentOption))
            dependentOptions.add(dependentOption);

//        if (dependentOption.getDependentOptions() == null)
//            dependentOption.setDependentOptions(new ArrayList<Option>());
//
//        if (!dependentOption.getDependentOptions().contains(this))
//            dependentOption.getDependentOptions().add(this);
    }

    public void addDependentOptions(List<Option> options) {
        for (Option o : options) {
            addDependentOption(o);
        }
    }

    public void addPossibleTariff(Tariff possibleTariff) {
        if (possibleTariffs == null)
            possibleTariffs = new ArrayList<Tariff>();

        if (!possibleTariffs.contains(possibleTariff))
            possibleTariffs.add(possibleTariff);

        List<Option> possibleOptionsInTariff = possibleTariff.getPossibleOptions();
        if (possibleOptionsInTariff == null)
            possibleOptionsInTariff = new ArrayList<Option>();

        if (!possibleOptionsInTariff.contains(this))
            possibleOptionsInTariff.add(this);

        possibleTariff.setPossibleOptions(possibleOptionsInTariff);
    }

    public void addPossibleTariffs(List<Tariff> tariffs) {
        for (Tariff t : tariffs)
            addPossibleTariff(t);
    }

    public void removeIncompatibleOption(Option option) {
        if (incompatibleOptions == null)
            incompatibleOptions = new ArrayList<Option>();

        if (incompatibleOptions.contains(option))
            incompatibleOptions.remove(option);


        if (option.getIncompatibleOptions() == null)
            option.setIncompatibleOptions(new ArrayList<Option>());

        if (option.getIncompatibleOptions().contains(this))
            option.getIncompatibleOptions().remove(this);
    }

    public void removeIncompatibleOptions(List<Option> options) {
        for (Option o : options) {
            removeIncompatibleOption(o);
        }
    }

    public void removeDependentOption(Option option) {
        if (dependentOptions == null)
            dependentOptions = new ArrayList<Option>();

        if (dependentOptions.contains(option))
            dependentOptions.remove(option);

//        if (option.getDependentOptions() == null)
//            option.setDependentOptions(new ArrayList<Option>());
//
//        if (option.getDependentOptions().contains(this))
//            option.getDependentOptions().remove(this);
    }

    public void removeDependentOptions(List<Option> options) {
        for (Option o : options) {
            removeDependentOption(o);
        }
    }

    public void removePossibleTariff(Tariff tariff) {

        if (possibleTariffs == null)
            possibleTariffs = new ArrayList<Tariff>();

        if (possibleTariffs.contains(tariff))
            possibleTariffs.remove(tariff);

        List<Option> possibleOptionsInTariff = tariff.getPossibleOptions();
        if (possibleOptionsInTariff == null)
            possibleOptionsInTariff = new ArrayList<Option>();

        if (possibleOptionsInTariff.contains(this))
            possibleOptionsInTariff.remove(this);

        tariff.setPossibleOptions(possibleOptionsInTariff);
    }

    public void removePossibleTariffs(List<Tariff> tariffs) {
        for (Tariff t : tariffs)
            removePossibleTariff(t);
    }

    public Option() {}

    public Option(String title,
                  String description,
                  Float price,
                  Float connectionCost,
                  List<Tariff> possibleTariffs,
                  List<Option> incompatibleOptions,
                  List<Option> dependentOptions) {

        this.title = title;
        this.description = description;
        this.price = price;
        this.connectionCost = connectionCost;
        this.possibleTariffs = possibleTariffs;
        this.incompatibleOptions = incompatibleOptions;
        this.dependentOptions = dependentOptions;
    }

    public OptionDTO toDTO() {
        return new OptionDTO(
                this.id,
                this.title,
                this.description,
                this.price,
                this.connectionCost
        );
    }


    public static Option getFromDTO(OptionDTO optionInfo) {
        return new Option(
                optionInfo.getTitle(),
                optionInfo.getDescription(),
                optionInfo.getPrice(),
                optionInfo.getConnectionCost(),
                null,
                null,
                null
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getConnectionCost() {
        return connectionCost;
    }

    public void setConnectionCost(Float connectionCost) {
        this.connectionCost = connectionCost;
    }

    public List<Tariff> getPossibleTariffs() {
        return possibleTariffs;
    }

    public void setPossibleTariffs(List<Tariff> possibleTariffs) {
        this.possibleTariffs = possibleTariffs;
    }

    public List<Option> getIncompatibleOptions() {
        return incompatibleOptions;
    }

    public void setIncompatibleOptions(List<Option> incompatibleOptions) {
        this.incompatibleOptions = incompatibleOptions;
    }

    public List<Option> getDependentOptions() {
        return dependentOptions;
    }

    public void setDependentOptions(List<Option> dependentOptions) {
        this.dependentOptions = dependentOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Option option = (Option) o;

        if (connectedContracts != null ? !connectedContracts.equals(option.connectedContracts) : option.connectedContracts != null)
            return false;
        if (connectionCost != null ? !connectionCost.equals(option.connectionCost) : option.connectionCost != null)
            return false;
        if (dependentOptions != null ? !dependentOptions.equals(option.dependentOptions) : option.dependentOptions != null)
            return false;
        if (description != null ? !description.equals(option.description) : option.description != null) return false;
        if (id != null ? !id.equals(option.id) : option.id != null) return false;
        if (incompatibleOptions != null ? !incompatibleOptions.equals(option.incompatibleOptions) : option.incompatibleOptions != null)
            return false;
        if (possibleTariffs != null ? !possibleTariffs.equals(option.possibleTariffs) : option.possibleTariffs != null)
            return false;
        if (price != null ? !price.equals(option.price) : option.price != null) return false;
        if (title != null ? !title.equals(option.title) : option.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (connectionCost != null ? connectionCost.hashCode() : 0);
        result = 31 * result + (possibleTariffs != null ? possibleTariffs.hashCode() : 0);
        result = 31 * result + (incompatibleOptions != null ? incompatibleOptions.hashCode() : 0);
        result = 31 * result + (connectedContracts != null ? connectedContracts.hashCode() : 0);
        result = 31 * result + (dependentOptions != null ? dependentOptions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Option{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", connectionCost=" + connectionCost +
                ", possibleTariffs.size=" + (possibleTariffs != null ? possibleTariffs.size() : 0) +
                ", incompatibleOptions.size=" + (incompatibleOptions != null ? incompatibleOptions.size() : 0) +
                ", dependentOptions.size=" + (dependentOptions != null ? dependentOptions.size() : 0) +
                ", connectedContracts.size=" + (connectedContracts != null ? connectedContracts.size() : 0) +
                '}';
    }

    public static List<Option> fromListDTO(List<OptionDTO> addedIncompatibleOptions) {
        List<Option> result;
        result = new ArrayList<Option>();

        for (OptionDTO dto : addedIncompatibleOptions) {
            Long id = dto.getId();
            if (id != null) {

            } else {
                result.add(Option.getFromDTO(dto));
            }
        }

        return result;
    }

    public void update(OptionDTO dto) {
        title = dto.getTitle();
        description = dto.getDescription();
        price = dto.getPrice();
        connectionCost = dto.getConnectionCost();
    }

    public List<Contract> getConnectedContracts() {
        return connectedContracts;
    }

    public void setConnectedContracts(List<Contract> connectedContracts) {
        this.connectedContracts = connectedContracts;
    }
}
