package com.tsystems.ecare.server.dao;

import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.server.domain.Option;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class OptionDAO extends AbstractDAO {
    private Logger logger = Logger.getLogger(OptionDAO.class.getName());

    public OptionDAO(EntityManager em) {
        super(em);
    }

    /**
     * Adding new option to DB
     * @param option not persisted new option
     */
    public boolean addNewOption(Option option) {
        if (validator.validate(option).size() == 0) {
            if (getOptionByTitle(option.getTitle()) == null) {
                em.persist(option);
                logger.info("Option has been added to db: " + option);
                return true;
            }
        }

        logger.info("Option has not added to db: " + option);
        return false;
    }

    /**
     * Find option by ID
     * @param optionId option ID
     * @return founded Option or null, if Option not found
     */
    public Option getOptionById(Long optionId) {

        Option option;
        option = em.find(Option.class, optionId);

        logger.info("Result of find option by ID{" + optionId + "}: " + option);
        return option;
    }

    /**
     * Find option by title
     * @param optionTitle string with option title
     * @return founded Option or null, if Option not found
     */
    public Option getOptionByTitle(String optionTitle) {

        List<Option> result;
        result = em.createQuery("SELECT option FROM Option option WHERE option.title=:title")
                .setParameter("title", optionTitle)
                .getResultList();

        if (result != null && result.size() != 0) {
            logger.info("Option has been found by title{'" + optionTitle + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("Option with title{'" + optionTitle + "'} not found");
            return null;
        }
    }

    /**
     * Find all options from DB
     * @return List<Option> with all options or null, if list is empty
     */
    public List<Option> getAllOptions() {
        return em.createQuery("SELECT option FROM Option option")
                .getResultList();
    }

    /**
     * Remove option from DB
     * @param option option for removing
     * @return true if option has been removed, false otherwise
     */
    // TODO: Add exception handler for non existent option
    public boolean removeOption(Option option) {

        em.remove(option);

        logger.info("Option has been removed: " + option);
        return true;
    }

    public List<Option> getOptionsListFromDtoList(List<OptionDTO> options) {
        List<Option> result;
        result = new ArrayList<Option>();

        for (OptionDTO dto : options) {
            Long id = dto.getId();
            if (id != null) {
                result.add(getOptionById(id));
            } else {
                result.add(Option.getFromDTO(dto));
            }
        }

        return result;
    }

    public void updateOption(Option option) {
        em.merge(option);
    }

    public List<OptionDTO> getOptionsDtoListFromList(List<Option> options) {
        List<OptionDTO> result;
        result = new ArrayList<OptionDTO>();

        if (result != null) {
            for (Option o : options) {
                result.add(o.toDTO());
            }
        }

        return result;
    }
}
