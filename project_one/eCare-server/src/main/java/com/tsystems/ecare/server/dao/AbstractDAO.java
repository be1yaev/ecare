package com.tsystems.ecare.server.dao;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by Andrew on 23.08.2014.
 */
public abstract class AbstractDAO {
    protected EntityManager em;
    protected ValidatorFactory factory;
    protected Validator validator;

    private static final Logger logger = Logger.getLogger(AbstractDAO.class);


    protected AbstractDAO(EntityManager em) {
        this.em = em;
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void beginTransaction() {
        em.getTransaction().begin();
        logger.info("Begin transaction");
    }

    public void commitTransaction() {
        em.getTransaction().commit();
        em.clear();
        logger.info("Commit transaction");
    }

    public void rollbackTransaction() {
        em.getTransaction().rollback();
        em.clear();
        logger.info("Rollback transaction");
    }

    public void flushChanges() {
        em.flush();
        logger.info("Flush EntityManager");
    }
}

