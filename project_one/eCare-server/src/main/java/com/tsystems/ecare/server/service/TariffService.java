package com.tsystems.ecare.server.service;

import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.server.dao.TariffDAO;
import com.tsystems.ecare.server.domain.Option;
import com.tsystems.ecare.server.domain.Tariff;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class TariffService {
    private static final Logger logger = Logger.getLogger(TariffService.class);
    private static TariffService serviceInstance;
    private static TariffDAO tariffDAO = null;

    public static void setEntityManager(EntityManager em) {
        tariffDAO = new TariffDAO(em);
    }

    public synchronized static TariffService getInstance() {
        if (serviceInstance == null) {
            if (tariffDAO == null)
                throw new IllegalStateException("EntityManager has not set.");

            serviceInstance = new TariffService();
        }
        return serviceInstance;
    }

    private TariffService() {

    }

    /**
     *
     * @param command
     * @return
     */
    public TariffsListCommand getTariffsList(GetTariffsListCommand command) {
        List<Tariff> tariffs;
        tariffs = tariffDAO.getAllTariffs();

        List<TariffDTO> result = new ArrayList<TariffDTO>();
        for (Tariff t : tariffs) {
            result.add(t.toDTO());
        }

        return new TariffsListCommand(result);
    }

    /**
     *
     * @param options
     * @return
     */
    public List<Tariff> getTariffsListFromDtoList(List<TariffDTO> options) {
        return tariffDAO.getTariffsListFromDtoList(options);
    }

    /**
     *
     * @param command
     * @return
     */
    public DeleteResultCommand deleteTariff(DeleteTariffCommand command) {
        tariffDAO.beginTransaction();

        Tariff tariff;
        tariff = tariffDAO.getTariffById(command.getTariffId());

        try {
            tariffDAO.removeTariff(tariff);
            tariffDAO.commitTransaction();
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            return new DeleteResultCommand(false);
        }

        return new DeleteResultCommand(true);
    }

    /**
     *
     * @param command
     * @return
     */
    public CreateTariffConfirmationCommand createTariff(CreateTariffCommand command) {
        Tariff tariff;
        tariff = Tariff.getFromDTO(command.getNewTariff());

        boolean resultAdding;
        tariffDAO.beginTransaction();
        resultAdding = tariffDAO.addNewTariff(tariff);
        tariffDAO.commitTransaction();

        CreateTariffConfirmationCommand resultCommand;
        if (resultAdding) {
            resultCommand = new CreateTariffConfirmationCommand(resultAdding, tariff.toDTO());
        } else {
            resultCommand = new CreateTariffConfirmationCommand(resultAdding, null);
            resultCommand.setMessage("Error when adding, title not unique");
        }

        return resultCommand;
    }

    /**
     *
     * @param command
     * @return
     */
    public OptionsListCommand getPossibleOptionsList(GetPossibleOptionsListCommand command) {Option option;
        List<Option> possibleOptions;
        Tariff tariff;

        tariff = tariffDAO.getTariffById(command.getHeadTariff().getId());
        possibleOptions = tariff.getPossibleOptions();

        List<OptionDTO> result = new ArrayList<OptionDTO>();
        for (Option o : possibleOptions) {
            result.add(o.toDTO());
        }

        return new OptionsListCommand(result);
    }

    /**
     *
     * @param command
     * @return
     */
    public UpdateResultCommand updateTariff(UpdateTariffCommand command) {
        tariffDAO.beginTransaction();

        Tariff tariff;
        tariff = tariffDAO.getTariffById(command.getTariff().getId());

        tariff.update(command.getTariff());
        tariff.addPossibleOptions(OptionService.getInstance().getOptionsListFromDtoList(
                        command.getAddedPossibleOptions())
        );
        tariff.removePossibleOptions(OptionService.getInstance().getOptionsListFromDtoList(
                        command.getRemovedPossibleOptions())
        );

        tariffDAO.updateTariff(tariff);
        tariffDAO.commitTransaction();

        return new UpdateResultCommand(true);
    }

    public Tariff getTariffById(Long id) {
        return tariffDAO.getTariffById(id);
    }

    /**
     *
     * @param command
     * @return
     */
    public GetTariffResultCommand getTariffById(GetTariffByIdCommand command) {
        return new GetTariffResultCommand(
                tariffDAO.getTariffById(
                        command.getTariffId()
                ).toDTO()
        );
    }
}
