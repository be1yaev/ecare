package com.tsystems.ecare.server.service;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.server.dao.OptionDAO;
import com.tsystems.ecare.server.domain.Option;
import com.tsystems.ecare.server.domain.Tariff;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class OptionService {
    private static final Logger logger = Logger.getLogger(OptionService.class);
    private static OptionService serviceInstance;
    private static OptionDAO optionDAO = null;

    public static void setEntityManager(EntityManager em) {
        optionDAO = new OptionDAO(em);
    }

    public synchronized static OptionService getInstance() {
        if (serviceInstance == null) {
            if (optionDAO == null)
                throw new IllegalStateException("EntityManager has not set.");

            serviceInstance = new OptionService();
        }
        return serviceInstance;
    }

    private OptionService() {}

    /**
     *
     * @param command
     * @return
     */
    public OptionsListCommand getOptionsList(GetOptionsListCommand command) {
        List<Option> options;
        options = optionDAO.getAllOptions();

        List<OptionDTO> result = new ArrayList<OptionDTO>();
        for (Option o : options) {
            result.add(o.toDTO());
        }

        return new OptionsListCommand(result);
    }

    /**
     *
     * @param command
     * @return
     */
    public CreateOptionConfirmationCommand createOption(CreateOptionCommand command) {
        Option option;
        option = Option.getFromDTO(command.getNewOption());

        boolean result;
        optionDAO.beginTransaction();
        result = optionDAO.addNewOption(option);

        if (result) {
            optionDAO.commitTransaction();
            return new CreateOptionConfirmationCommand(true, option.toDTO());
        } else {
            optionDAO.rollbackTransaction();
            return new CreateOptionConfirmationCommand(false, null);
        }
    }

    /**
     *
     * @param command
     * @return
     */
    public Command getIncompatibleOptionsList(GetIncompatibleOptionsListCommand command) {
        Option option;
        List<Option> incompatibleOptions;

        option = optionDAO.getOptionById(command.getHeadOption().getId());
        incompatibleOptions = option.getIncompatibleOptions();

        List<OptionDTO> result = new ArrayList<OptionDTO>();
            for (Option o : incompatibleOptions) {
                result.add(o.toDTO());
            }

        return new OptionsListCommand(result);
    }

    /**
     *
     * @param command
     * @return
     */
    public Command getDependentOptionsList(GetDependentOptionsListCommand command) {
        Option option;
        List<Option> dependentOptions;

        option = optionDAO.getOptionById(command.getHeadOption().getId());
        dependentOptions = option.getDependentOptions();

        List<OptionDTO> result = new ArrayList<OptionDTO>();
            for (Option o : dependentOptions) {
                result.add(o.toDTO());
            }

        return new OptionsListCommand(result);
    }

    /**
     *
     * @param command
     * @return
     */
    public GetOptionResultCommand getOptionById(GetOptionByIdCommand command) {
        return new GetOptionResultCommand(
                optionDAO.getOptionById(
                        command.getOptionId()
                ).toDTO()
        );
    }

    /**
     *
     * @param command
     * @return
     */
    public TariffsListCommand getPossibleTariffsList(GetPossibleTariffsListCommand command) {
        Option option;
        List<Tariff> possibleTariffs;

        option = optionDAO.getOptionById(command.getHeadOption().getId());
        possibleTariffs = option.getPossibleTariffs();

        List<TariffDTO> result = new ArrayList<TariffDTO>();
            for (Tariff o : possibleTariffs) {
                result.add(o.toDTO());
            }

        return new TariffsListCommand(result);
    }

    /**
     *
     * @param command
     * @return
     */
    public UpdateResultCommand updateOption(UpdateOptionCommand command) {
        optionDAO.beginTransaction();

        Option option;
        option = optionDAO.getOptionById(command.getOption().getId());

        option.update(command.getOption());

        option.addIncompatibleOptions(optionDAO.getOptionsListFromDtoList(command.getAddedIncompatibleOptions()));
        option.addDependentOptions(optionDAO.getOptionsListFromDtoList(command.getAddedDependentOptions()));
        option.addPossibleTariffs(TariffService.getInstance().getTariffsListFromDtoList(
                        command.getAddedPossibleTariffs())
        );

        option.removeIncompatibleOptions(optionDAO.getOptionsListFromDtoList(command.getRemovedIncompatibleOptions()));
        option.removeDependentOptions(optionDAO.getOptionsListFromDtoList(command.getRemovedDependentOptions()));
        option.removePossibleTariffs(TariffService.getInstance().getTariffsListFromDtoList(
                        command.getRemovedPossibleTariffs())
        );

        optionDAO.updateOption(option);
        optionDAO.commitTransaction();

        return new UpdateResultCommand(true);
    }

    /**
     *
     * @param command
     * @return
     */
    public DeleteResultCommand deleteOption(DeleteOptionCommand command) {
        optionDAO.beginTransaction();

        Option option;
        option = optionDAO.getOptionById(command.getOptionId());
        optionDAO.removeOption(option);

        optionDAO.commitTransaction();

        return new DeleteResultCommand(true);
    }

    /**
     *
     * @param tariffs
     * @return
     */
    public List<Option> getOptionsListFromDtoList(List<OptionDTO> tariffs) {
        return optionDAO.getOptionsListFromDtoList(tariffs);
    }

    public List<OptionDTO> getOptionsDtoListFromList(List<Option> options) {
        return optionDAO.getOptionsDtoListFromList(options);
    }

    /**
     * Check options for compability
     * @param optionList list for check
     * @return true, if options are compatibility, false otherwise
     */
    public boolean checkForCompatibility(List<Option> optionList) {
        for (Option option : optionList) {
            List<Option> incompatibleOptionsList;
            incompatibleOptionsList = option.getIncompatibleOptions();

            for (Option incompatibleOption : incompatibleOptionsList)
                if (optionList.contains(incompatibleOption))
                    return false;

            List<Option> dependentOptionsList;
            dependentOptionsList = option.getDependentOptions();

            for (Option dependentOption : dependentOptionsList)
                if (!optionList.contains(dependentOption))
                    return false;
        }
        return true;
    }
}
