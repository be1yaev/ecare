package com.tsystems.ecare.server.dao;

import com.tsystems.ecare.server.domain.Contract;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContractDAO extends AbstractDAO {
    private Logger logger;

    public ContractDAO(EntityManager em) {
        super(em);
        logger = Logger.getLogger(ContractDAO.class.getName());
    }

    /**
     * Adding new contract to DB
     * @param contract not persisted new contract
     */
    public boolean addNewContract(Contract contract) {
        if (validator.validate(contract).size() == 0) {
            em.persist(contract);
            logger.info("Contract has been added to db: " + contract);
            return true;
        }

        logger.info("Contract has not added to db: " + contract);
        return false;
    }

    /**
     * Find contract by ID
     * @param contractId contract ID
     * @return founded Contract or null, if Contract not found
     */
    public Contract getContractById(Long contractId) {

        Contract contract;
        contract = em.find(Contract.class, contractId);

        logger.info("Result of find contract by ID{" + contractId + "}: " + contract);
        return contract;
    }

    /**
     * Find contract by phone number
     * @param phoneNumber string with phone number
     * @return founded Contract or null, if Contract not found
     */
    public Contract getContractByPhoneNumber(String phoneNumber) {
        List<Contract> result;
        result = em.createQuery("SELECT contract " + "FROM Contract contract "
                + "WHERE contract.phoneNumber=:phone")
                .setParameter("phone", phoneNumber)
                .getResultList();

        if (result != null && result.size() != 0) {
            logger.info("Contract has been found by phone{'"
                    + phoneNumber + "'}: " + result.get(0));
            return result.get(0);
        } else {
            logger.info("Contract with phone{'" + phoneNumber + "'} not found");
            return null;
        }
    }

    /**
     * Find all contracts from DB
     * @return List<Contract> with all contracts or null, if list is empty
     */
    public List<Contract> getAllContracts() {
        return em.createQuery("SELECT contract " + "FROM Contract contract")
                .getResultList();
    }

    /**
     * Remove contract from DB
     * @param contract contract for removing
     * @return true if contract has been removed, false otherwise
     */
    // TODO: Add exception handler for non existent contract
    public boolean removeContract(Contract contract) {
        em.remove(contract);

        logger.info("Contract has been removed: " + contract);
        return true;
    }

    public void updateContract(Contract contract) {
        em.merge(contract);
        logger.info("Contract has been updated: " + contract);
    }

    public List<Contract> getContractsLikePhoneNumber(String phoneNumber) {

        List<Contract> result;
        result = em.createQuery("SELECT contract " + "FROM Contract contract "
                + "WHERE contract.phoneNumber LIKE :phone")
                .setParameter("phone", phoneNumber)
                .getResultList();

        if (result != null && result.size() != 0) {
            logger.info("Contracts has been found like phone{'"
                    + phoneNumber + "'}: " + result.get(0));
            return result;
        } else {
            logger.info("Contracts with phone{'" + phoneNumber + "'} not found");
            return Collections.EMPTY_LIST;
        }
    }
}
