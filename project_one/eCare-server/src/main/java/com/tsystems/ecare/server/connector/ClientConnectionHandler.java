package com.tsystems.ecare.server.connector;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.server.service.ContractService;
import com.tsystems.ecare.server.service.OptionService;
import com.tsystems.ecare.server.service.TariffService;
import com.tsystems.ecare.server.service.UserService;
import org.apache.log4j.Logger;
import org.hibernate.exception.JDBCConnectionException;

import javax.persistence.EntityManager;
import java.io.*;
import java.net.Socket;

public class ClientConnectionHandler implements Runnable {
    private static final Logger logger = Logger.getLogger(ClientConnectionHandler.class);

    private Socket clientSocket;
    private EntityManager em;

    public ClientConnectionHandler(Socket clientSocket, EntityManager em) {
        this.clientSocket = clientSocket;
        this.em = em;
    }

    @Override
    public void run() {
        logger.info("Handle connection from client: " + clientSocket);

        try {
            ObjectInputStream inputStream;
            ObjectOutputStream outputStream;
            Command request;
            Command response = null;

            // Receive command from client
            inputStream = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            request = (Command)inputStream.readObject();

            try {
                logger.info("Request command: " + request);
                response = executeCommand(request);
                logger.info("Response command: " + response);
            } catch (Exception e) {
                logger.error("Error {" + e.getMessage() + "} handling command: " + request);
            }

            // Send command to client
            outputStream = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
            outputStream.writeObject(response);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("Error receiving command from client: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (JDBCConnectionException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    private Command executeCommand(Command command) {
        Command response = null;

        switch (command.getCommandType()) {
            case USER_VERIFICATION_REQUEST:
                response = UserService.getInstance().verificationUser((UserVerificationCommand) command);
                break;
            case REGISTRATION_REQUEST:
                response = UserService.getInstance().registrationUser((UserRegistrationCommand) command);
                break;
            case GET_USER_CONTRACTS_LIST_REQUEST:
                response = UserService.getInstance().getUserContractsList((GetUserContractsListCommand) command);
                break;
            case GET_USERS_LIST_REQUEST:
                response = UserService.getInstance().getUsersList((GetUsersListCommand) command);
                break;
            case GET_CONTRACTS_LIST_REQUEST:
                response = ContractService.getInstance().getContractsList((GetContractsListCommand) command);
                break;
            case GET_OPTIONS_LIST_REQUEST:
                response = OptionService.getInstance().getOptionsList((GetOptionsListCommand) command);
                break;
            case GET_TARIFFS_LIST_REQUEST:
                response = TariffService.getInstance().getTariffsList((GetTariffsListCommand) command);
                break;
            case CREATE_NEW_OPTION_REQUEST:
                response = OptionService.getInstance().createOption((CreateOptionCommand) command);
                break;
            case GET_INCOMPATIBLE_OPTIONS_LIST_REQUEST:
                response = OptionService.getInstance().getIncompatibleOptionsList((GetIncompatibleOptionsListCommand) command);
                break;
            case GET_DEPENDENT_OPTIONS_LIST_REQUEST:
                response = OptionService.getInstance().getDependentOptionsList((GetDependentOptionsListCommand) command);
                break;
            case GET_OPTION_BY_ID_REQUEST:
                response = OptionService.getInstance().getOptionById((GetOptionByIdCommand) command);
                break;
            case GET_POSSIBLE_TARIFFS_LIST_REQUEST:
                response = OptionService.getInstance().getPossibleTariffsList((GetPossibleTariffsListCommand) command);
                break;
            case UPDATE_OPTION_REQUEST:
                response = OptionService.getInstance().updateOption((UpdateOptionCommand) command);
                break;
            case DELETE_OPTION_REQUEST:
                response = OptionService.getInstance().deleteOption((DeleteOptionCommand) command);
                break;
            case DELETE_TARIFF_REQUEST:
                response = TariffService.getInstance().deleteTariff((DeleteTariffCommand) command);
                break;
            case CREATE_NEW_TARIFF_REQUEST:
                response = TariffService.getInstance().createTariff((CreateTariffCommand) command);
                break;
            case GET_POSSIBLE_OPTIONS_LIST_REQUEST:
                response = TariffService.getInstance().getPossibleOptionsList((GetPossibleOptionsListCommand) command);
                break;
            case UPDATE_TARIFF_REQUEST:
                response = TariffService.getInstance().updateTariff((UpdateTariffCommand) command);
                break;
            case GET_TARIFF_BY_ID_REQUEST:
                response = TariffService.getInstance().getTariffById((GetTariffByIdCommand) command);
                break;
            case GET_FULL_CONTRACT_DATA_REQUEST:
                response = ContractService.getInstance().getFullContractData((GetFullContractDataRequestCommand) command);
                break;
            case CREATE_NEW_CONTRACT_REQUEST:
                response = ContractService.getInstance().createNewContract((CreateContractCommand) command);
                break;
            case UPDATE_CONTRACT_REQUEST:
                response = ContractService.getInstance().updateContract((UpdateContractCommand) command);
                break;
            case DELETE_CONTRACT_REQUEST:
                response = ContractService.getInstance().deleteContract((DeleteContractCommand) command);
                break;
            case UPDATE_AND_CREATE_CONTRACTS_FROM_CART_REQUEST:
                response = ContractService.getInstance().updateAndCreateContractsFromCart((UpdateAndCreateContractsFromCartCommand) command);
                break;
            case GET_USER_BY_ID_REQUEST:
                response = UserService.getInstance().getUserById((GetUserByIdCommand) command);
                break;
            case GET_USER_BY_CONTRACT_ID_REQUEST:
                response = ContractService.getInstance().getUserByContractId((GetUserByContractIdCommand) command);
                break;
        }

        return response;
    }
}
