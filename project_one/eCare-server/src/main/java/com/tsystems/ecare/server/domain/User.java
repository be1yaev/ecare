package com.tsystems.ecare.server.domain;

import com.tsystems.ecare.common.dto.UserDTO;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="table_user")
public class User {

    public enum Role {
        MANAGER, ADMIN, CLIENT;
    }

    @Id
    @Column(name="user_id")
    //@Min(value = 1, message = "Incorrect ID, please input numeric value")
    @GeneratedValue
    private Long id;

    @NotNull(message = "Field \"User name\" can not be empty")
    @Size(min = 3, max = 25, message = "Length of \"User name\" must be between {2} and {1} characters")
    @Column(name = "surname"/*, unique = true*/)
    private String surname;

    @NotNull(message = "Field \"User firstname\" can not be empty")
    @Size(min = 3, max = 25, message = "Length of \"User firstname\" must be between {2} and {1} characters")
    @Column(name = "name"/*, unique = true*/)
    private String firstname;

    //@NotNull(message = "Field \"User patronymic\" can not be empty")
    @Size(min = 3, max = 25, message = "Length of \"User patronymic\" must be between {2} and {1} characters")
    @Column(name = "patronymic"/*, unique = true*/)
    private String patronymic;

    //@NotNull(message = "Field \"Passport number\" can not be empty")
    @Size(min = 6, max = 20, message = "Length of \"Passport number\" must be between {2} and {1} characters")
    @Column(name = "passport_number"/*, unique = true*/)
    private String passportNumber;

    //@NotNull(message = "Field \"Passport description\" can not be empty")
    @Size(min = 0, max = 50, message = "Length of \"Passport description\" must be between {2} and {1} characters")
    @Column(name = "passport_description")
    private String passportDescription;

    //@NotNull(message = "Field \"Country\" can not be empty")
    @Size(min = 0, max = 25, message = "Length of \"Country name\" must be between {2} and {1} characters")
    @Column(name = "country")
    private String country;

    //@NotNull(message = "Field \"City\" can not be empty")
    @Size(min = 0, max = 25, message = "Length of \"City name\" must be between {2} and {1} characters")
    @Column(name = "city")
    private String city;

    //@NotNull(message = "Field \"Address\" can not be empty")
    @Size(min = 0, max = 100, message = "Length of \"Address\" must be between {2} and {1} characters")
    @Column(name = "address")
    private String address;

    //@NotNull(message = "Field \"E-mail\" can not be empty")
    @Size(min = 0, max = 50, message = "Length of \"E-mail\" must be between {2} and {1} characters")
    @Column(name = "email")
    @Email
    private String email;

    //@NotNull(message = "Field \"Password\" can not be empty")
    @Size(min = 0, max = 50, message = "Length of \"Password\" must be between {2} and {1} characters")
    @Column(name = "password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @OneToMany(mappedBy = "client")
//    @JoinTable(name="relation_user_and_contracts",
//            joinColumns=@JoinColumn(name="user_id"),
//            inverseJoinColumns=@JoinColumn(name="contract_id"))
    private List<Contract> contracts;

    /** Setting default value of role */
    @PrePersist
    void prePersist() {
        if (role == null)
            role = Role.CLIENT;
    }

    /**
     * Adding new contract and connecting user with contract
     * @param contract new contract object
     */
    public void addContract(Contract contract) {
        contract.setClient(this);

        if (contracts == null)
            contracts = new ArrayList<Contract>();

        contracts.add(contract);
    }

    public User() {}

    public User(String surname,
                String firstname,
                String patronymic,
                String passportNumber,
                String passportDescription,
                String country,
                String city,
                String address,
                String email,
                String password,
                Role role,
                List<Contract> contracts) {

        this.surname = surname;
        this.firstname = firstname;
        this.patronymic = patronymic;
        this.passportNumber = passportNumber;
        this.passportDescription = passportDescription;
        this.country = country;
        this.city = city;
        this.address = address;
        this.email = email;
        this.password = password;
        this.role = role;
        this.contracts = contracts;
    }

    public UserDTO toDTO() {
        return new UserDTO(
                this.id,
                this.surname,
                this.firstname,
                this.patronymic,
                this.passportNumber,
                this.passportDescription,
                this.country,
                this.city,
                this.address,
                this.email,
                UserDTO.Role.valueOf(this.role.name())
        );
    }

    public static User getFromDTO(UserDTO userInfo) {
        return new User(
                userInfo.getSurname(),
                userInfo.getFirstname(),
                userInfo.getPatronymic(),
                userInfo.getPassportNumber(),
                userInfo.getPassportDescription(),
                userInfo.getCountry(),
                userInfo.getCity(),
                userInfo.getAddress(),
                userInfo.getEmail(),
                null,
                Role.valueOf(userInfo.getRole().name()),
                null
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportDescription() {
        return passportDescription;
    }

    public void setPassportDescription(String passportDescription) {
        this.passportDescription = passportDescription;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (address != null ? !address.equals(user.address) : user.address != null) return false;
        if (city != null ? !city.equals(user.city) : user.city != null) return false;
        if (contracts != null ? !contracts.equals(user.contracts) : user.contracts != null) return false;
        if (country != null ? !country.equals(user.country) : user.country != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (firstname != null ? !firstname.equals(user.firstname) : user.firstname != null) return false;
        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (passportDescription != null ? !passportDescription.equals(user.passportDescription) : user.passportDescription != null)
            return false;
        if (passportNumber != null ? !passportNumber.equals(user.passportNumber) : user.passportNumber != null)
            return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (patronymic != null ? !patronymic.equals(user.patronymic) : user.patronymic != null) return false;
        if (role != user.role) return false;
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
        result = 31 * result + (passportDescription != null ? passportDescription.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (contracts != null ? contracts.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", passportDescription='" + passportDescription + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", contracts.size=" + (contracts != null ? contracts.size() : 0) +
                '}';
    }
}
