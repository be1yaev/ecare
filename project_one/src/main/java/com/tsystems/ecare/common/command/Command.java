package com.tsystems.ecare.common.command;

import java.io.Serializable;

/** Client-server command common interface */
public interface Command extends Serializable {
    CommandType getCommandType();
//    Command execute();
}
