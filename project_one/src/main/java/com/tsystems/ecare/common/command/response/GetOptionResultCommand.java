package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetOptionResultCommand extends AbstractResponseCommand {
    private OptionDTO option;

    // TODO: For the next version
    // private String token;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_OPTION_RESPONSE;
    }

    public GetOptionResultCommand() {
    }

    public GetOptionResultCommand(OptionDTO option) {
        this.option = option;
    }

    public OptionDTO getOption() {
        return option;
    }

    public void setOption(OptionDTO option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "GetOptionResultCommand{" +
                "option=" + option +
                '}';
    }
}
