package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.ContractDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class CreateContractConfirmationCommand extends AbstractResponseCommand {
    private boolean createConfirm;
    private ContractDTO createdContract;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_CONTRACT_RESPONSE;
    }

    public CreateContractConfirmationCommand() {
    }

    public CreateContractConfirmationCommand(boolean createConfirm, ContractDTO createdContract) {
        this.createConfirm = createConfirm;
        this.createdContract = createdContract;
    }

    public boolean isCreateConfirm() {
        return createConfirm;
    }

    public void setCreateConfirm(boolean createConfirm) {
        this.createConfirm = createConfirm;
    }

    public ContractDTO getCreatedContract() {
        return createdContract;
    }

    public void setCreatedContract(ContractDTO createdContract) {
        this.createdContract = createdContract;
    }

    @Override
    public String toString() {
        return "CreateContractConfirmationCommand{" +
                "createConfirm=" + createConfirm +
                ", createdContract=" + createdContract +
                '}';
    }
}
