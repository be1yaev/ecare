package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.TariffDTO;

/**
 * Created by Andrew on 23.08.2014.
 */
public class CreateTariffCommand implements Command {
    private TariffDTO newTariff;
//    private List<OptionDTO> incompatibleOptions;
//    private List<OptionDTO> dependentOptions;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_TARIFF_REQUEST;
    }

    public CreateTariffCommand() {
    }

    public CreateTariffCommand(TariffDTO newTariff) {
        this.newTariff = newTariff;
    }

    public TariffDTO getNewTariff() {
        return newTariff;
    }

    public void setNewTariff(TariffDTO newTariff) {
        this.newTariff = newTariff;
    }

    @Override
    public String toString() {
        return "CreateTariffCommand{" +
                "newTariff=" + newTariff +
                '}';
    }
}
