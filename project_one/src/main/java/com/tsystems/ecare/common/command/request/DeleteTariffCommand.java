package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 23.08.2014.
 */
public class DeleteTariffCommand implements Command {
    private Long tariffId;

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_TARIFF_REQUEST;
    }

    public DeleteTariffCommand(Long optionId) {
        this.tariffId = optionId;
    }

    public DeleteTariffCommand() {
    }

    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }

    @Override
    public String toString() {
        return "DeleteTariffCommand{" +
                "tariffId=" + tariffId +
                '}';
    }
}
