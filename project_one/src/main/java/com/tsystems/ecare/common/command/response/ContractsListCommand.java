package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.ContractDTO;

import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class ContractsListCommand extends AbstractResponseCommand {
    private List<ContractDTO> contracts;

    @Override
    public CommandType getCommandType() {
        return CommandType.CONTRACTS_LIST_RESPONSE;
    }

    public ContractsListCommand() {
        super();
    }

    public ContractsListCommand(List<ContractDTO> contracts) {
        super();
        this.contracts = contracts;
    }

    public List<ContractDTO> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractDTO> contracts) {
        this.contracts = contracts;
    }

    @Override
    public String toString() {
        return "ContractListCommand{" +
                "contracts=" + contracts +
                '}';
    }
}
