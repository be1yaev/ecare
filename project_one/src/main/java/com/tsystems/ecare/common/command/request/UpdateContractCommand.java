package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;

import java.util.List;

/**
 * Created by Andrew on 23.08.2014.
 */
public class UpdateContractCommand implements Command {
    ContractDTO updatedContractDTO;
    UserDTO currentUser;
    TariffDTO currentTariff;
    List<OptionDTO> addedOptions;
    List<OptionDTO> removedOptions;

    @Override
    public CommandType getCommandType() {
        return CommandType.UPDATE_CONTRACT_REQUEST;
    }

    public UpdateContractCommand() {
    }

    public UpdateContractCommand(ContractDTO updatedContractDTO,
                                 UserDTO currentUser,
                                 TariffDTO currentTariff,
                                 List<OptionDTO> addedOptions,
                                 List<OptionDTO> removedOptions) {

        this.updatedContractDTO = updatedContractDTO;
        this.currentUser = currentUser;
        this.currentTariff = currentTariff;
        this.addedOptions = addedOptions;
        this.removedOptions = removedOptions;
    }

    public ContractDTO getUpdatedContractDTO() {
        return updatedContractDTO;
    }

    public void setUpdatedContractDTO(ContractDTO updatedContractDTO) {
        this.updatedContractDTO = updatedContractDTO;
    }

    public UserDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserDTO currentUser) {
        this.currentUser = currentUser;
    }

    public TariffDTO getCurrentTariff() {
        return currentTariff;
    }

    public void setCurrentTariff(TariffDTO currentTariff) {
        this.currentTariff = currentTariff;
    }

    public List<OptionDTO> getAddedOptions() {
        return addedOptions;
    }

    public void setAddedOptions(List<OptionDTO> addedOptions) {
        this.addedOptions = addedOptions;
    }

    public List<OptionDTO> getRemovedOptions() {
        return removedOptions;
    }

    public void setRemovedOptions(List<OptionDTO> removedOptions) {
        this.removedOptions = removedOptions;
    }

    @Override
    public String toString() {
        return "UpdateContractCommand{" +
                "updatedContractDTO=" + updatedContractDTO +
                ", currentUser=" + currentUser +
                ", currentTariff=" + currentTariff +
                ", addedOptions=" + addedOptions +
                ", removedOptions=" + removedOptions +
                '}';
    }
}
