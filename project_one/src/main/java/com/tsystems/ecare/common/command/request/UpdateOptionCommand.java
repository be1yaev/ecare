package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;

import java.util.List;

/**
 * Created by Andrew on 23.08.2014.
 */
public class UpdateOptionCommand implements Command {
    private OptionDTO option;

    private List<OptionDTO> addedIncompatibleOptions;
    private List<OptionDTO> addedDependentOptions;
    private List<TariffDTO> addedPossibleTariffs;

    private List<OptionDTO> removedIncompatibleOptions;
    private List<OptionDTO> removedDependentOptions;
    private List<TariffDTO> removedPossibleTariffs;

    @Override
    public CommandType getCommandType() {
        return CommandType.UPDATE_OPTION_REQUEST;
    }

    public UpdateOptionCommand() {
    }

    public UpdateOptionCommand(OptionDTO option,
                               List<OptionDTO> addedIncompatibleOptions,
                               List<OptionDTO> addedDependentOptions,
                               List<TariffDTO> addedPossibleTariffs,
                               List<OptionDTO> removedIncompatibleOptions,
                               List<OptionDTO> removedDependentOptions,
                               List<TariffDTO> removedPossibleTariffs) {
        this.option = option;
        this.addedIncompatibleOptions = addedIncompatibleOptions;
        this.addedDependentOptions = addedDependentOptions;
        this.addedPossibleTariffs = addedPossibleTariffs;
        this.removedIncompatibleOptions = removedIncompatibleOptions;
        this.removedDependentOptions = removedDependentOptions;
        this.removedPossibleTariffs = removedPossibleTariffs;
    }

    public OptionDTO getOption() {
        return option;
    }

    public void setOption(OptionDTO option) {
        this.option = option;
    }

    public List<OptionDTO> getAddedIncompatibleOptions() {
        return addedIncompatibleOptions;
    }

    public void setAddedIncompatibleOptions(List<OptionDTO> addedIncompatibleOptions) {
        this.addedIncompatibleOptions = addedIncompatibleOptions;
    }

    public List<OptionDTO> getAddedDependentOptions() {
        return addedDependentOptions;
    }

    public void setAddedDependentOptions(List<OptionDTO> addedDependentOptions) {
        this.addedDependentOptions = addedDependentOptions;
    }

    public List<TariffDTO> getAddedPossibleTariffs() {
        return addedPossibleTariffs;
    }

    public void setAddedPossibleTariffs(List<TariffDTO> addedPossibleTariffs) {
        this.addedPossibleTariffs = addedPossibleTariffs;
    }

    public List<OptionDTO> getRemovedIncompatibleOptions() {
        return removedIncompatibleOptions;
    }

    public void setRemovedIncompatibleOptions(List<OptionDTO> removedIncompatibleOptions) {
        this.removedIncompatibleOptions = removedIncompatibleOptions;
    }

    public List<OptionDTO> getRemovedDependentOptions() {
        return removedDependentOptions;
    }

    public void setRemovedDependentOptions(List<OptionDTO> removedDependentOptions) {
        this.removedDependentOptions = removedDependentOptions;
    }

    public List<TariffDTO> getRemovedPossibleTariffs() {
        return removedPossibleTariffs;
    }

    public void setRemovedPossibleTariffs(List<TariffDTO> removedPossibleTariffs) {
        this.removedPossibleTariffs = removedPossibleTariffs;
    }

    @Override
    public String toString() {
        return "UpdateOptionCommand{" +
                "option=" + option +
                ", addedIncompatibleOptions=" + addedIncompatibleOptions +
                ", addedDependentOptions=" + addedDependentOptions +
                ", addedPossibleTariffs=" + addedPossibleTariffs +
                ", removedIncompatibleOptions=" + removedIncompatibleOptions +
                ", removedDependentOptions=" + removedDependentOptions +
                ", removedPossibleTariffs=" + removedPossibleTariffs +
                '}';
    }
}
