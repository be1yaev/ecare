package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;

import java.util.List;

/**
 * Created by Andrew on 23.08.2014.
 */
public class UpdateTariffCommand implements Command {
    private TariffDTO tariff;

    private List<OptionDTO> addedPossibleOptions;
    private List<OptionDTO> removedPossibleOptions;

    @Override
    public CommandType getCommandType() {
        return CommandType.UPDATE_TARIFF_REQUEST;
    }

    public UpdateTariffCommand() {
    }

    public UpdateTariffCommand(TariffDTO tariff, List<OptionDTO> addedPossibleOptions, List<OptionDTO> removedPossibleOptions) {
        this.tariff = tariff;
        this.addedPossibleOptions = addedPossibleOptions;
        this.removedPossibleOptions = removedPossibleOptions;
    }

    public TariffDTO getTariff() {
        return tariff;
    }

    public void setTariff(TariffDTO tariff) {
        this.tariff = tariff;
    }

    public List<OptionDTO> getAddedPossibleOptions() {
        return addedPossibleOptions;
    }

    public void setAddedPossibleOptions(List<OptionDTO> addedPossibleOptions) {
        this.addedPossibleOptions = addedPossibleOptions;
    }

    public List<OptionDTO> getRemovedPossibleOptions() {
        return removedPossibleOptions;
    }

    public void setRemovedPossibleOptions(List<OptionDTO> removedPossibleOptions) {
        this.removedPossibleOptions = removedPossibleOptions;
    }

    @Override
    public String toString() {
        return "UpdateTariffCommand{" +
                "tariff=" + tariff +
                ", addedPossibleOptions=" + addedPossibleOptions +
                ", removedPossibleOptions=" + removedPossibleOptions +
                '}';
    }
}
