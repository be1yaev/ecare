package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetUserByContractIdCommand implements Command {
    private Long contractId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_USER_BY_CONTRACT_ID_REQUEST;
    }

    public GetUserByContractIdCommand() {
    }

    public GetUserByContractIdCommand(Long userId) {
        this.contractId = userId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long userId) {
        this.contractId = userId;
    }

    @Override
    public String toString() {
        return "GetUserByIdCommand{" +
                "contractId=" + contractId +
                '}';
    }
}
