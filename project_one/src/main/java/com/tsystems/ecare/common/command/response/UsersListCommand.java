package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.UserDTO;

import java.util.List;

/**
 * Created by Andrew on 23.08.2014.
 */
public class UsersListCommand extends AbstractResponseCommand {
    private List<UserDTO> users;

    @Override
    public CommandType getCommandType() {
        return CommandType.USERS_LIST_RESPONSE;
    }

    public UsersListCommand() {
        super();
    }

    public UsersListCommand(List<UserDTO> users) {
        super();
        this.users = users;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "UsersListCommand{" +
                "users=" + users +
                '}';
    }
}
