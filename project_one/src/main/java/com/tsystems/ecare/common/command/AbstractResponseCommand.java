package com.tsystems.ecare.common.command;

/**
 * Created by Andrew on 23.08.2014.
 */
public abstract class AbstractResponseCommand implements Command {
    private String message;
    private boolean error;

    protected AbstractResponseCommand() {
        message = "";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
