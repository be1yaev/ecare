package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.TariffDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class CreateTariffConfirmationCommand extends AbstractResponseCommand {
    private boolean createConfirm;
    private TariffDTO createdTariff;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_TARIFF_RESPONSE;
    }

    public CreateTariffConfirmationCommand() {
    }

    public CreateTariffConfirmationCommand(boolean createConfirm, TariffDTO createdTariff) {
        this.createConfirm = createConfirm;
        this.createdTariff = createdTariff;
    }

    public boolean isCreateConfirm() {
        return createConfirm;
    }

    public void setCreateConfirm(boolean createConfirm) {
        this.createConfirm = createConfirm;
    }

    public TariffDTO getCreatedTariff() {
        return createdTariff;
    }

    public void setCreatedTariff(TariffDTO createdTariff) {
        this.createdTariff = createdTariff;
    }

    @Override
    public String toString() {
        return "CreateTariffConfirmationCommand{" +
                "createConfirm=" + createConfirm +
                ", createdTariff=" + createdTariff +
                '}';
    }
}
