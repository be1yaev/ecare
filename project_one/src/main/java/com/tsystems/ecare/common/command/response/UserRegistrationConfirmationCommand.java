package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UserRegistrationConfirmationCommand extends AbstractResponseCommand {
    private boolean registrationConfirm;
    private Long newId;

    @Override
    public CommandType getCommandType() {
        return CommandType.REGISTRATION_RESPONSE;
    }

    public UserRegistrationConfirmationCommand() {
        super();
    }

    public UserRegistrationConfirmationCommand(boolean registrationConfirm, Long newId) {
        super();
        this.registrationConfirm = registrationConfirm;
        this.newId = newId;
    }

    public boolean isRegistrationConfirm() {
        return registrationConfirm;
    }

    public void setRegistrationConfirm(boolean registrationConfirm) {
        this.registrationConfirm = registrationConfirm;
    }

    public Long getNewId() {
        return newId;
    }

    public void setNewId(Long newId) {
        this.newId = newId;
    }

    @Override
    public String toString() {
        return "UserRegistrationConfirmationCommand{" +
                "registrationConfirm=" + registrationConfirm +
                ", newId=" + newId +
                '}';
    }
}
