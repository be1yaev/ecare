package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;

import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class FullContractDataCommand extends AbstractResponseCommand {
    private ContractDTO contract;
    private List<OptionDTO> options;
    private TariffDTO tariff;
    private UserDTO user;

    @Override
    public CommandType getCommandType() {
        return CommandType.FULL_CONTRACT_DATA_RESPONSE;
    }

    public FullContractDataCommand() {
    }

    public FullContractDataCommand(ContractDTO contracts, List<OptionDTO> options, TariffDTO tariff, UserDTO user) {
        this.contract = contracts;
        this.options = options;
        this.tariff = tariff;
        this.user = user;
    }

    public ContractDTO getContract() {
        return contract;
    }

    public void setContract(ContractDTO contract) {
        this.contract = contract;
    }

    public List<OptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<OptionDTO> options) {
        this.options = options;
    }

    public TariffDTO getTariff() {
        return tariff;
    }

    public void setTariff(TariffDTO tariff) {
        this.tariff = tariff;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "FulldContractDataCommand{" +
                "contract=" + contract +
                ", options.size=" + ((options != null) ? options.size() : "0" ) +
                ", tariff=" + tariff +
                ", user=" + user +
                '}';
    }
}

