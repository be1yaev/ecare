package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 23.08.2014.
 */
public class DeleteContractCommand implements Command {
    private Long contractId;

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_CONTRACT_REQUEST;
    }

    public DeleteContractCommand(Long optionId) {
        this.contractId = optionId;
    }

    public DeleteContractCommand() {
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    @Override
    public String toString() {
        return "DeleteContractCommand{" +
                "contractId=" + contractId +
                '}';
    }
}
