package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetTariffByIdCommand implements Command {
    private Long tariffId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_TARIFF_BY_ID_REQUEST;
    }

    public GetTariffByIdCommand() {
    }

    public GetTariffByIdCommand(Long tariffId) {
        this.tariffId = tariffId;
    }

    public Long getTariffId() {
        return tariffId;
    }

    public void setTariffId(Long tariffId) {
        this.tariffId = tariffId;
    }

    @Override
    public String toString() {
        return "GetTariffByIdCommand{" +
                "tariffId=" + tariffId +
                '}';
    }
}
