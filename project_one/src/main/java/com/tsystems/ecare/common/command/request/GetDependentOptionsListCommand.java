package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetDependentOptionsListCommand implements Command {
    private OptionDTO headOption;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_DEPENDENT_OPTIONS_LIST_REQUEST;
    }

    public GetDependentOptionsListCommand() {
    }

    public GetDependentOptionsListCommand(OptionDTO headOption) {
        this.headOption = headOption;
    }

    public OptionDTO getHeadOption() {
        return headOption;
    }

    public void setHeadOption(OptionDTO headOption) {
        this.headOption = headOption;
    }

    @Override
    public String toString() {
        return "GetDependentOptionsListCommand{" +
                "headOption=" + headOption +
                '}';
    }
}
