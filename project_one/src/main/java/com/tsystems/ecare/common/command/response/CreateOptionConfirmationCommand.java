package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class CreateOptionConfirmationCommand extends AbstractResponseCommand {
    private boolean createConfirm;
    private OptionDTO newOption;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_OPTION_RESPONSE;
    }

    public CreateOptionConfirmationCommand() {
    }

    public CreateOptionConfirmationCommand(boolean createConfirm, OptionDTO newOption) {
        this.createConfirm = createConfirm;
        this.newOption = newOption;
    }

    public boolean isCreateConfirm() {
        return createConfirm;
    }

    public void setCreateConfirm(boolean createConfirm) {
        this.createConfirm = createConfirm;
    }

    public OptionDTO getNewOption() {
        return newOption;
    }

    public void setNewOption(OptionDTO newOption) {
        this.newOption = newOption;
    }

    @Override
    public String toString() {
        return "CreateOptionConfirmationCommand{" +
                "createConfirm=" + createConfirm +
                ", newOption=" + newOption +
                '}';
    }
}
