package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;

import java.util.List;

/**
 * Created by Andrew on 23.08.2014.
 */
public class CreateContractCommand implements Command {
    ContractDTO contractDTO;
    UserDTO currentUser;
    TariffDTO currentTariff;
    List<OptionDTO> addedOptions;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_CONTRACT_REQUEST;
    }

    public CreateContractCommand() {
    }

    public CreateContractCommand(ContractDTO contractDTO, UserDTO currentUser, TariffDTO currentTariff, List<OptionDTO> addedOptions) {
        this.contractDTO = contractDTO;
        this.currentUser = currentUser;
        this.currentTariff = currentTariff;
        this.addedOptions = addedOptions;
    }

    public ContractDTO getContractDTO() {
        return contractDTO;
    }

    public void setContractDTO(ContractDTO contractDTO) {
        this.contractDTO = contractDTO;
    }

    public UserDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserDTO currentUser) {
        this.currentUser = currentUser;
    }

    public TariffDTO getCurrentTariff() {
        return currentTariff;
    }

    public void setCurrentTariff(TariffDTO currentTariff) {
        this.currentTariff = currentTariff;
    }

    public List<OptionDTO> getAddedOptions() {
        return addedOptions;
    }

    public void setAddedOptions(List<OptionDTO> addedOptions) {
        this.addedOptions = addedOptions;
    }

    @Override
    public String toString() {
        return "CreateContractCommand{" +
                "contractDTO=" + contractDTO +
                ", currentUser=" + currentUser +
                ", currentTariff=" + currentTariff +
                ", addedOptions=" + addedOptions +
                '}';
    }
}
