package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 21.08.2014.
 */
public class UserVerificationCommand implements Command {
    String email;
    String password;

    @Override
    public CommandType getCommandType() {
        return CommandType.USER_VERIFICATION_REQUEST;
    }

    public UserVerificationCommand() {
    }

    public UserVerificationCommand(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserVerificationCommand{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
