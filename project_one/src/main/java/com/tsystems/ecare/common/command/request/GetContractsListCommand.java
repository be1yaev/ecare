package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetContractsListCommand implements Command {
    private String likePhoneNumber;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_CONTRACTS_LIST_REQUEST;
    }

    public GetContractsListCommand() {
    }

    public GetContractsListCommand(String likePhoneNumber) {
        this.likePhoneNumber = likePhoneNumber;

    }

    public String getLikePhoneNumber() {
        return likePhoneNumber;
    }

    public void setLikePhoneNumber(String likePhoneNumber) {
        this.likePhoneNumber = likePhoneNumber;
    }

    @Override
    public String toString() {
        return "GetContractsListCommand{" +
                "likePhoneNumber='" + likePhoneNumber + '\'' +
                '}';
    }
}
