package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

import java.util.List;

/**
 * Created by Andrew on 25.08.2014.
 */
public class UpdateAndCreateContractsFromCartCommand implements Command {
    private List<CreateContractCommand> newContracts;
    private List<UpdateContractCommand> updatedContracts;

    @Override
    public CommandType getCommandType() {
        return CommandType.UPDATE_AND_CREATE_CONTRACTS_FROM_CART_REQUEST;
    }

    public UpdateAndCreateContractsFromCartCommand() {
    }

    public UpdateAndCreateContractsFromCartCommand(List<CreateContractCommand> newContracts, List<UpdateContractCommand> updatedContracts) {
        this.newContracts = newContracts;
        this.updatedContracts = updatedContracts;
    }

    public List<CreateContractCommand> getNewContracts() {
        return newContracts;
    }

    public void setNewContracts(List<CreateContractCommand> newContracts) {
        this.newContracts = newContracts;
    }

    public List<UpdateContractCommand> getUpdatedContracts() {
        return updatedContracts;
    }

    public void setUpdatedContracts(List<UpdateContractCommand> updatedContracts) {
        this.updatedContracts = updatedContracts;
    }

    @Override
    public String toString() {
        return "UpdateAndCreateContractsFromCartCommand{" +
                "newContracts=" + newContracts +
                ", updatedContracts=" + updatedContracts +
                '}';
    }
}
