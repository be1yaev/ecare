package com.tsystems.ecare.common.dto;

import java.io.Serializable;

public class TariffDTO implements Serializable, Comparable {

    private Long id;

    private String title;
    private String description;

    public TariffDTO(Long id) {
        this.id = id;
    }

    public TariffDTO(Long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariffDTO tariffDTO = (TariffDTO) o;

        if (description != null ? !description.equals(tariffDTO.description) : tariffDTO.description != null)
            return false;
        if (id != null ? !id.equals(tariffDTO.id) : tariffDTO.id != null) return false;
        if (title != null ? !title.equals(tariffDTO.title) : tariffDTO.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((TariffDTO)o).getId());
    }

    @Override
    public String toString() {
        return "TariffDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
