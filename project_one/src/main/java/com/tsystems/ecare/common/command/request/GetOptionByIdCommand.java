package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetOptionByIdCommand implements Command {
    private Long optionId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_OPTION_BY_ID_REQUEST;
    }

    public GetOptionByIdCommand() {
    }

    public GetOptionByIdCommand(Long optionId) {
        this.optionId = optionId;
    }

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    @Override
    public String toString() {
        return "GetOptionByIdCommand{" +
                "optionId=" + optionId +
                '}';
    }
}
