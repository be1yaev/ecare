package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetOptionsListCommand implements Command {
    @Override
    public CommandType getCommandType() {
        return CommandType.GET_OPTIONS_LIST_REQUEST;
    }

    public GetOptionsListCommand() {
    }

    @Override
    public String toString() {
        return "GetOptionsListCommand{}";
    }
}
