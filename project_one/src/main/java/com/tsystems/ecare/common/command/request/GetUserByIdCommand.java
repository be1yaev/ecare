package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetUserByIdCommand implements Command {
    private Long userId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_USER_BY_ID_REQUEST;
    }

    public GetUserByIdCommand() {
    }

    public GetUserByIdCommand(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GetUserByIdCommand{" +
                "userId=" + userId +
                '}';
    }
}
