package com.tsystems.ecare.common.dto;

import java.io.Serializable;

public class OptionDTO implements Serializable, Comparable {

    private Long id;

    private String title;
    private String description;

    private Float price;
    private Float connectionCost;

    public OptionDTO() {
    }

    public OptionDTO(Long id, String title, String description, Float price, Float connectionCost) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.connectionCost = connectionCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getConnectionCost() {
        return connectionCost;
    }

    public void setConnectionCost(Float connectionCost) {
        this.connectionCost = connectionCost;
    }

    @Override
    public String toString() {
        return "OptionDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", connectionCost=" + connectionCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptionDTO optionDTO = (OptionDTO) o;

        if (connectionCost != null ? !connectionCost.equals(optionDTO.connectionCost) : optionDTO.connectionCost != null)
            return false;
        if (description != null ? !description.equals(optionDTO.description) : optionDTO.description != null)
            return false;
        if (id != null ? !id.equals(optionDTO.id) : optionDTO.id != null) return false;
        if (price != null ? !price.equals(optionDTO.price) : optionDTO.price != null) return false;
        if (title != null ? !title.equals(optionDTO.title) : optionDTO.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (connectionCost != null ? connectionCost.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((OptionDTO)o).getId());
    }
}
