package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetPossibleTariffsListCommand implements Command {
    private OptionDTO headOption;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_POSSIBLE_TARIFFS_LIST_REQUEST;
    }

    public GetPossibleTariffsListCommand() {
    }

    public GetPossibleTariffsListCommand(OptionDTO headOption) {
        this.headOption = headOption;
    }

    public OptionDTO getHeadOption() {
        return headOption;
    }

    public void setHeadOption(OptionDTO headOption) {
        this.headOption = headOption;
    }

    @Override
    public String toString() {
        return "GetPossibleTariffsListCommand{" +
                "headOption=" + headOption +
                '}';
    }
}
