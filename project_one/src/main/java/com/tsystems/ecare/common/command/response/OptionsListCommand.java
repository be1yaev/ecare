package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class OptionsListCommand extends AbstractResponseCommand {
    private List<OptionDTO> options;

    @Override
    public CommandType getCommandType() {
        return CommandType.OPTIONS_LIST_RESPONSE;
    }

    public OptionsListCommand() {
        super();
    }

    public OptionsListCommand(List<OptionDTO> options) {
        super();
        this.options = options;
    }

    public List<OptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<OptionDTO> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "OptionsListCommand{" +
                "options=" + options +
                '}';
    }
}
