package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 24.08.2014.
 */
public class GetFullContractDataRequestCommand implements Command {
    Long contractId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_FULL_CONTRACT_DATA_REQUEST;
    }

    public GetFullContractDataRequestCommand() {
    }

    public GetFullContractDataRequestCommand(Long contractId) {
        this.contractId = contractId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    @Override
    public String toString() {
        return "GetFullContractDataRequest{" +
                "contractId=" + contractId +
                '}';
    }
}
