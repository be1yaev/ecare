package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.TariffDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetTariffResultCommand extends AbstractResponseCommand {
    private TariffDTO tariff;

    // TODO: For the next version
    // private String token;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_TARIFF_RESPONSE;
    }

    public GetTariffResultCommand() {
    }

    public GetTariffResultCommand(TariffDTO tariff) {
        this.tariff = tariff;
    }

    public TariffDTO getTariff() {
        return tariff;
    }

    public void setTariff(TariffDTO tariff) {
        this.tariff = tariff;
    }

    @Override
    public String toString() {
        return "GetTariffResultCommand{" +
                "tariff=" + tariff +
                '}';
    }
}
