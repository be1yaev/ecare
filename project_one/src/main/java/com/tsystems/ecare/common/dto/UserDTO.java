package com.tsystems.ecare.common.dto;

import java.io.Serializable;

public class UserDTO implements Serializable, Comparable {
    public enum Role {
        MANAGER, ADMIN, CLIENT;
    }

    private Long id;

    private String surname;
    private String firstname;
    private String patronymic;

    private String passportNumber;
    private String passportDescription;

    private String country;
    private String city;
    private String address;

    private String email;
    private Role role;

    public UserDTO() {
    }

    public UserDTO(Long id,
                   String surname,
                   String firstname,
                   String patronymic,
                   String passportNumber,
                   String passportDescription,
                   String country,
                   String city,
                   String address,
                   String email,
                   Role role) {

        this.id = id;
        this.surname = surname;
        this.firstname = firstname;
        this.patronymic = patronymic;
        this.passportNumber = passportNumber;
        this.passportDescription = passportDescription;
        this.country = country;
        this.city = city;
        this.address = address;
        this.email = email;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportDescription() {
        return passportDescription;
    }

    public void setPassportDescription(String passportDescription) {
        this.passportDescription = passportDescription;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", passportDescription='" + passportDescription + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDTO userDTO = (UserDTO) o;

        if (address != null ? !address.equals(userDTO.address) : userDTO.address != null) return false;
        if (city != null ? !city.equals(userDTO.city) : userDTO.city != null) return false;
        if (country != null ? !country.equals(userDTO.country) : userDTO.country != null) return false;
        if (email != null ? !email.equals(userDTO.email) : userDTO.email != null) return false;
        if (firstname != null ? !firstname.equals(userDTO.firstname) : userDTO.firstname != null) return false;
        if (id != null ? !id.equals(userDTO.id) : userDTO.id != null) return false;
        if (passportDescription != null ? !passportDescription.equals(userDTO.passportDescription) : userDTO.passportDescription != null)
            return false;
        if (passportNumber != null ? !passportNumber.equals(userDTO.passportNumber) : userDTO.passportNumber != null)
            return false;
        if (patronymic != null ? !patronymic.equals(userDTO.patronymic) : userDTO.patronymic != null) return false;
        if (role != userDTO.role) return false;
        if (surname != null ? !surname.equals(userDTO.surname) : userDTO.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
        result = 31 * result + (passportDescription != null ? passportDescription.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return id.compareTo(((UserDTO)o).getId());
    }
}
