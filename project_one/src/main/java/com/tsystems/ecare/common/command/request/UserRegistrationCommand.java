package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.UserDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UserRegistrationCommand implements Command {
    private UserDTO userInfo;
    private String password;

    @Override
    public CommandType getCommandType() {
        return CommandType.REGISTRATION_REQUEST;
    }

    public UserRegistrationCommand() {
    }

    public UserRegistrationCommand(UserDTO userInfo, String password) {
        this.userInfo = userInfo;
        this.password = password;
    }

    public UserDTO getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserDTO userInfo) {
        this.userInfo = userInfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserRegistrationCommand{" +
                "userInfo=" + userInfo +
                ", password='" + password + '\'' +
                '}';
    }
}
