package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.UserDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class LoginConfirmationCommand extends AbstractResponseCommand {
    private boolean loginConfirm;
    private UserDTO user;

    // TODO: For the next version
    // private String token;

    @Override
    public CommandType getCommandType() {
        return CommandType.LOGIN_RESPONSE;
    }

    public LoginConfirmationCommand() {
        super();
    }

    public LoginConfirmationCommand(boolean loginConfirm, UserDTO user) {
        super();
        this.loginConfirm = loginConfirm;
        this.user = user;
    }

    public boolean isLoginConfirm() {
        return loginConfirm;
    }

    public void setLoginConfirm(boolean loginConfirm) {
        this.loginConfirm = loginConfirm;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "LoginConfirmationCommand{" +
                "loginConfirm=" + loginConfirm +
                ", user=" + user +
                '}';
    }
}
