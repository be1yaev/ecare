package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.TariffDTO;

import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class TariffsListCommand extends AbstractResponseCommand {
    private List<TariffDTO> tariffs;

    @Override
    public CommandType getCommandType() {
        return CommandType.TARIFFS_LIST_RESPONSE;
    }

    public TariffsListCommand() {
        super();
    }

    public TariffsListCommand(List<TariffDTO> tariffs) {
        super();
        this.tariffs = tariffs;
    }

    public List<TariffDTO> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<TariffDTO> tariffs) {
        this.tariffs = tariffs;
    }

    @Override
    public String toString() {
        return "TariffsListCommand{" +
                "tariffs=" + tariffs +
                '}';
    }
}
