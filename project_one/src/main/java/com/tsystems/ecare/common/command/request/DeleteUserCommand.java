package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 23.08.2014.
 */
public class DeleteUserCommand implements Command {
    private Long userId;

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_USER_REQUEST;
    }

    public DeleteUserCommand(Long optionId) {
        this.userId = optionId;
    }

    public DeleteUserCommand() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "DeleteContractCommand{" +
                "userId=" + userId +
                '}';
    }
}
