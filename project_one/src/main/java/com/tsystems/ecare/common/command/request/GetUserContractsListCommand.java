package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetUserContractsListCommand implements Command {
    private Long userId;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_USER_CONTRACTS_LIST_REQUEST;
    }

    public GetUserContractsListCommand() {
    }

    public GetUserContractsListCommand(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "GetUserContractsListCommand{" +
                "userId=" + userId +
                '}';
    }
}
