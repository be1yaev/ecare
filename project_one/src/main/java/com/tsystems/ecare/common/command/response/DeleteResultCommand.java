package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class DeleteResultCommand extends AbstractResponseCommand {
    private Boolean deletedSuccessfully;

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_RESPONSE;
    }

    public DeleteResultCommand() {
    }

    public DeleteResultCommand(Boolean deletedSuccessfully) {
        this.deletedSuccessfully = deletedSuccessfully;
    }

    public Boolean getDeletedSuccessfully() {
        return deletedSuccessfully;
    }

    public void setDeletedSuccessfully(Boolean deletedSuccessfully) {
        this.deletedSuccessfully = deletedSuccessfully;
    }

    @Override
    public String toString() {
        return "DeleteResultCommand{" +
                "deletedSuccessfully=" + deletedSuccessfully +
                '}';
    }
}
