package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.OptionDTO;

/**
 * Created by Andrew on 23.08.2014.
 */
public class CreateOptionCommand implements Command {
    private OptionDTO newOption;
//    private List<OptionDTO> incompatibleOptions;
//    private List<OptionDTO> dependentOptions;

    @Override
    public CommandType getCommandType() {
        return CommandType.CREATE_NEW_OPTION_REQUEST;
    }

    public CreateOptionCommand() {
    }

    public CreateOptionCommand(OptionDTO newOption) {
        this.newOption = newOption;
    }

    public OptionDTO getNewOption() {
        return newOption;
    }

    public void setNewOption(OptionDTO newOption) {
        this.newOption = newOption;
    }

    @Override
    public String toString() {
        return "CreateOptionCommand{" +
                "newOption=" + newOption +
                '}';
    }
}
