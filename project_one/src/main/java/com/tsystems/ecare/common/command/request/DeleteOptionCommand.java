package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 23.08.2014.
 */
public class DeleteOptionCommand implements Command {
    private Long optionId;

    @Override
    public CommandType getCommandType() {
        return CommandType.DELETE_OPTION_REQUEST;
    }

    public DeleteOptionCommand(Long optionId) {
        this.optionId = optionId;
    }

    public DeleteOptionCommand() {
    }

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    @Override
    public String toString() {
        return "DeleteOptionCommand{" +
                "optionId=" + optionId +
                '}';
    }
}
