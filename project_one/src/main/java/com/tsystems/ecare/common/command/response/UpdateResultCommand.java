package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UpdateResultCommand extends AbstractResponseCommand {
    private Boolean updatedSuccessfully;

    @Override
    public CommandType getCommandType() {
        return CommandType.UPDATE_RESPONSE;
    }

    public UpdateResultCommand() {
    }

    public UpdateResultCommand(Boolean updatedSuccessfully) {
        this.updatedSuccessfully = updatedSuccessfully;
    }

    public Boolean getUpdatedSuccessfully() {
        return updatedSuccessfully;
    }

    public void setUpdatedSuccessfully(Boolean updatedSuccessfully) {
        this.updatedSuccessfully = updatedSuccessfully;
    }

    @Override
    public String toString() {
        return "UpdateResultCommand{" +
                "updatedSuccessfully=" + updatedSuccessfully +
                '}';
    }
}
