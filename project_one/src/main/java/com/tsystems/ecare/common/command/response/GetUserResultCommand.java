package com.tsystems.ecare.common.command.response;

import com.tsystems.ecare.common.command.AbstractResponseCommand;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.UserDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetUserResultCommand extends AbstractResponseCommand {
    private UserDTO user;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_TARIFF_RESPONSE;
    }

    public GetUserResultCommand() {
    }

    public GetUserResultCommand(UserDTO user) {
        this.user = user;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GetUserResultCommand{" +
                "user=" + user +
                '}';
    }
}
