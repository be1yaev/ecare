package com.tsystems.ecare.common.command.request;

import com.tsystems.ecare.common.command.Command;
import com.tsystems.ecare.common.command.CommandType;
import com.tsystems.ecare.common.dto.TariffDTO;

/**
 * Created by Andrew on 22.08.2014.
 */
public class GetPossibleOptionsListCommand implements Command {
    private TariffDTO headTariff;

    @Override
    public CommandType getCommandType() {
        return CommandType.GET_POSSIBLE_OPTIONS_LIST_REQUEST;
    }

    public GetPossibleOptionsListCommand() {
    }

    public GetPossibleOptionsListCommand(TariffDTO headTariff) {
        this.headTariff = headTariff;
    }

    public TariffDTO getHeadTariff() {
        return headTariff;
    }

    public void setHeadTariff(TariffDTO headTariff) {
        this.headTariff = headTariff;
    }

    @Override
    public String toString() {
        return "GetPossibleOptionsListCommand{" +
                "headTariff=" + headTariff +
                '}';
    }
}
