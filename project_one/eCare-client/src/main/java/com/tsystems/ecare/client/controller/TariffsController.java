package com.tsystems.ecare.client.controller;

import com.tsystems.ecare.client.logic.TariffsModel;
import com.tsystems.ecare.client.view.manager.CreateNewTariffForm;
import com.tsystems.ecare.client.view.manager.EditTariffForm;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class TariffsController {
    private static TariffsController controllerInstance = null;

    public synchronized static TariffsController getInstance() {
        if (controllerInstance == null) {
            controllerInstance = new TariffsController();
        }
        return controllerInstance;
    }

    private static final Logger logger = Logger.getLogger(TariffsController.class.getName());

    private TariffsController() {

    }

    /**
     *
     * @return
     */
    public TableModel getAllTariffsTableModel() {
        return TariffsModel.getInstance().getAllTariffsTableModel();
    }

    /**
     *
     * @param tariffs
     * @return
     */
    public TableModel getShortTariffsTableModelFromList(ArrayList<TariffDTO> tariffs) {
        return TariffsModel.getInstance().getTableModelFromTariffsList(tariffs, true);
    }

    /**
     *
     * @return
     */
    public List<TariffDTO> getAllTariffs() {
        return TariffsModel.getInstance().getAllTariffs();
    }

    /**
     *
     * @param tariffsList
     * @return
     */
    public TableModel getFullTariffsTableModelFromList(List<TariffDTO> tariffsList) {
        return TariffsModel.getInstance().getTableModelFromTariffsList(tariffsList, false);
    }

    /**
     *
     * @param tariffId
     * @return
     */
    public boolean deleteTariff(Long tariffId) {
        return TariffsModel.getInstance().deleteTariff(tariffId);
    }

    /**
     *
     * @param parent
     */
    public void showCreateNewTariffWindow(JComponent parent) {
        new CreateNewTariffForm(parent).setVisible(true);
    }

    /**
     *
     * @param newTariff
     * @return
     */
    public TariffDTO createNewTariff(TariffDTO newTariff) {
        return TariffsModel.getInstance().createNewTariff(newTariff);
    }

    /**
     *
     * @param tariffId
     * @param parent
     */
    public void showEditingTariffWindow(Long tariffId, JComponent parent) {
        TariffDTO tariff;
        tariff = TariffsModel.getInstance().getTariffById(tariffId);

        new EditTariffForm(parent, tariff).setVisible(true);
    }

    /**
     *
     * @param tariff
     * @param parent
     */
    public void showEditingTariffWindow(TariffDTO tariff, JComponent parent) {
        new EditTariffForm(parent, tariff).setVisible(true);
    }

    /**
     *
     * @param tariff
     * @return
     */
    public List<OptionDTO> getPossibleOptions(TariffDTO tariff) {
        return TariffsModel.getInstance().getPossibleOptions(tariff);
    }

    /**
     *
     * @param changedTariff
     * @param addedPossibleOptions
     * @param removedPossibleOptions
     * @return
     */
    public boolean updateTariff(TariffDTO changedTariff,
                             ArrayList<OptionDTO> addedPossibleOptions,
                             ArrayList<OptionDTO> removedPossibleOptions) {

        return TariffsModel.getInstance().updateTariff(
                changedTariff,
                addedPossibleOptions,
                removedPossibleOptions
        );
    }
}
