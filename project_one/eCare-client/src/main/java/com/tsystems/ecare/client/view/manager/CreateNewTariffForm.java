package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.common.dto.TariffDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 24.08.2014.
 */
public class CreateNewTariffForm extends JFrame {
    private JPanel mainPanel;
    private JPanel titles;
    private JTextField titleField;
    private JTextArea descriptionTextArea;
    private JButton createButton;
    private JButton backButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;

    private JComponent parent;

    public CreateNewTariffForm(JComponent parent) {
        super("eCare");

        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        addBackButtonListener();
        addCreateButtonListener();

        setVisible(true);
    }

    private void addCreateButtonListener() {
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                TariffDTO tariffInfo;

                tariffInfo = new TariffDTO(
                        null,
                        titleField.getText(),
                        descriptionTextArea.getText()
                );

                try {
                    TariffDTO createdTariff;
                    createdTariff = TariffsController.getInstance().createNewTariff(tariffInfo);
                    JOptionPane.showMessageDialog(null, "Tariff has been created", "Success", JOptionPane.INFORMATION_MESSAGE);
                    TariffsController.getInstance().showEditingTariffWindow(createdTariff, parent);
                    dispose();
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void addBackButtonListener() {
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }
}
