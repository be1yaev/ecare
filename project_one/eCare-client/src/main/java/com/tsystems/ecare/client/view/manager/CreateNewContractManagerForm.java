package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.ContractsController;
import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Andrew on 24.08.2014.
 */
public class CreateNewContractManagerForm extends JFrame implements ActionListener {
    private JPanel mainPanel;
    private JButton createButton;
    private JButton backButton;
    private JLabel logoLabel;
    private JButton selectClientButton;
    private JTextField clientNameField;
    private JTextField tariffNameField;
    private JTable selectedOptionsTable;
    private JButton addOptionButton;
    private JButton removeSelectedOptionButton;
    private JTextField phoneField;
    private JButton selectTariffButton;
    private JPanel backgroundPanel;

    private JComponent parent;

    private UserDTO currentUser;
    private TariffDTO currentTariff;
    private Set<OptionDTO> addedOptions;

    public CreateNewContractManagerForm(JComponent parent) {
        super("eCare");

        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        selectedOptionsTable.setRowHeight(25);
        selectedOptionsTable.setRowSelectionAllowed(true);

        setupAllFieldsAndButtons();

        setVisible(true);
    }

    private void setupAllFieldsAndButtons() {
        currentUser = null;
        currentTariff = null;
        addedOptions = new TreeSet<OptionDTO>();

        createButton.addActionListener(this);
        backButton.addActionListener(this);
        selectClientButton.addActionListener(this);
        selectTariffButton.addActionListener(this);
        addOptionButton.addActionListener(this);
        removeSelectedOptionButton.addActionListener(this);

        updateAllFields();
    }

    private void updateAllFields() {
        if (currentUser == null)
            clientNameField.setText("Not selected");
        else
            clientNameField.setText(UsersController.getInstance().getShortUserNameFromUser(currentUser));

        if (currentTariff == null)
            tariffNameField.setText("Not selected");
        else
            tariffNameField.setText(currentTariff.getTitle());

        updateTable();
    }

    public void updateTable() {
        selectedOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(addedOptions)
                )
        );
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }

    private boolean checkPhoneNumber(String phone) {
        if (phone.length() != 11)
            return false;
        if (phone.contains(" "))
            return false;
        try {
            Long.getLong(phone);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == backButton) {
            dispose();
            return;
        }

        if (!checkPhoneNumber(phoneField.getText())) {
            JOptionPane.showMessageDialog(
                    null,
                    "Phone number must be comtain only 11 characters and only numbers",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == selectClientButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<UserDTO> clientsList;

            clientsList = UsersController.getInstance().getAllUsers();
            tableModel = UsersController.getInstance().getShortUsersTableModelFromList(clientsList);

            selectingForm = new SelectingForm(CreateNewContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedUserId;
                    selectedUserId = selectingForm.getSelectedRowId();

                    if (selectedUserId != null) {
                        UserDTO selectedUser = null;
                        for (UserDTO user : clientsList) {
                            if (user.getId().equals(selectedUserId)) {
                                selectedUser = user;
                                break;
                            }
                        }
                        currentUser = selectedUser;
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (currentUser == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Select client before selecting tariff",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == selectTariffButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<TariffDTO> tariffsList;

            tariffsList = TariffsController.getInstance().getAllTariffs();
            tableModel = TariffsController.getInstance().getFullTariffsTableModelFromList(tariffsList);
            selectingForm = new SelectingForm(CreateNewContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        TariffDTO selectedTariff = null;
                        for (TariffDTO option : tariffsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedTariff = option;
                                break;
                            }
                        }
                        currentTariff = selectedTariff;
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (currentTariff == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Select tariff before selecting options",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == addOptionButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<OptionDTO> optionsList;

            optionsList = TariffsController.getInstance().getPossibleOptions(currentTariff);
            tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
            selectingForm = new SelectingForm(CreateNewContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        OptionDTO selectedOption = null;
                        for (OptionDTO option : optionsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedOption = option;
                                break;
                            }
                        }
                        addedOptions.add(selectedOption);
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (e.getSource() == removeSelectedOptionButton) {
            if (selectedOptionsTable.getRowCount() == 0)
                return;

            int selectedRow;
            selectedRow = selectedOptionsTable.getSelectedRow();
            if (selectedRow == -1)
                return;

            Long selectedOptionId = Long.valueOf(
                    selectedOptionsTable.getModel().getValueAt(
                            selectedOptionsTable.convertRowIndexToModel(selectedRow), 0
                    ).toString()
            );

            OptionDTO selectedOption = null;
            for (OptionDTO option : addedOptions) {
                if (option.getId().equals(selectedOptionId)) {
                    selectedOption = option;
                    break;
                }
            }

            addedOptions.remove(selectedOption);
            updateAllFields();
            return;
        }

        if (e.getSource() == createButton) {
            ContractDTO contractDTO;
            contractDTO = new ContractDTO(
                    null,
                    phoneField.getText(),
                    false,
                    false
            );

            try {
                ContractDTO createdContract;
                createdContract = ContractsController.getInstance().createNewContract(
                        contractDTO,
                        currentUser,
                        currentTariff,
                        new ArrayList<OptionDTO>(addedOptions)
                );
                JOptionPane.showMessageDialog(null, "Contract has been created", "Success", JOptionPane.INFORMATION_MESSAGE);
                ContractsController.getInstance().showEditingOptionWindow(createdContract, parent);
                dispose();
            } catch (IllegalArgumentException exception) {
                JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
