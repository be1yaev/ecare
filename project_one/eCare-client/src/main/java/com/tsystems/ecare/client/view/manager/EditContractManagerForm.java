package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.ContractsController;
import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.common.command.response.FullContractDataCommand;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Andrew on 24.08.2014.
 */
public class EditContractManagerForm extends JFrame implements ActionListener{
    private JPanel mainPanel;
    private JButton selectTariffButton;
    private JTextField tariffNameField;
    private JTextField clientNameField;
    private JButton selectClientButton;
    private JTextField phoneField;
    private JButton saveButton;
    private JButton backButton;
    private JTable selectedOptionsTable;
    private JButton addOptionButton;
    private JButton removeSelectedOptionButton;
    private JLabel isBlockedByClientField;
    private JButton blockButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;

    private JComponent parent;

    private ContractDTO editableContract;
    private UserDTO currentUser;
    private TariffDTO currentTariff;

    private Set<OptionDTO> addedOptions;
    private Set<OptionDTO> currentOptions;
    private Set<OptionDTO> removedOptions;

    public EditContractManagerForm(JComponent parent, FullContractDataCommand contractInfo) {
        super("eCare");

        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        selectedOptionsTable.setRowHeight(25);
        selectedOptionsTable.setRowSelectionAllowed(true);

        setupAllFieldsAndButtons(contractInfo);

        setVisible(true);
    }

    private void setupAllFieldsAndButtons(FullContractDataCommand contractInfo) {

        editableContract = contractInfo.getContract();
        currentUser = contractInfo.getUser();
        currentTariff = contractInfo.getTariff();
        phoneField.setText(editableContract.getPhoneNumber());

        currentOptions = new TreeSet<OptionDTO>(contractInfo.getOptions());

        addedOptions = new TreeSet<OptionDTO>();
        removedOptions = new TreeSet<OptionDTO>();

        saveButton.addActionListener(this);
        backButton.addActionListener(this);
        selectClientButton.addActionListener(this);
        selectTariffButton.addActionListener(this);
        addOptionButton.addActionListener(this);
        removeSelectedOptionButton.addActionListener(this);
        blockButton.addActionListener(this);

        updateAllFields();
    }

    private void updateAllFields() {
        if (currentUser == null)
            clientNameField.setText("Not selected");
        else
            clientNameField.setText(UsersController.getInstance().getShortUserNameFromUser(currentUser));

        if (currentTariff == null)
            tariffNameField.setText("Not selected");
        else
            tariffNameField.setText(currentTariff.getTitle());

        if (editableContract.getBlockedByManager())
            blockButton.setText("Unblock");
        else
            blockButton.setText("Block");

        if (editableContract.getBlockedByManager() && editableContract.getBlockedByClient()) {
            isBlockedByClientField.setText("Blocked by client and manager");
        } else if(editableContract.getBlockedByManager()) {
            isBlockedByClientField.setText("Blocked by manager");
        } else if(editableContract.getBlockedByClient()) {
            isBlockedByClientField.setText("Blocked by client");
        } else isBlockedByClientField.setText("Not blocked");

        updateTable();
    }

    public void updateTable() {
        selectedOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(currentOptions)
                )
        );
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }

    private boolean checkPhoneNumber(String phone) {
        if (phone.length() != 11)
            return false;
        if (phone.contains(" "))
            return false;
        try {
            Long.getLong(phone);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == backButton) {
            dispose();
            return;
        }

        if (e.getSource() == blockButton) {
            editableContract.setBlockedByManager(!editableContract.getBlockedByManager());
            updateAllFields();
            return;
        }

        if (!checkPhoneNumber(phoneField.getText())) {
            JOptionPane.showMessageDialog(
                    null,
                    "Phone number must contain only 11 characters and only numbers",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == selectClientButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<UserDTO> clientsList;

            clientsList = UsersController.getInstance().getAllUsers();
            tableModel = UsersController.getInstance().getFullUsersTableModelFromList(clientsList);

            selectingForm = new SelectingForm(EditContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedUserId;
                    selectedUserId = selectingForm.getSelectedRowId();

                    if (selectedUserId != null) {
                        UserDTO selectedUser = null;
                        for (UserDTO user : clientsList) {
                            if (user.getId().equals(selectedUserId)) {
                                selectedUser = user;
                                break;
                            }
                        }
                        currentUser = selectedUser;
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (currentUser == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Select client before selecting tariff",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == selectTariffButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<TariffDTO> tariffsList;

            tariffsList = TariffsController.getInstance().getAllTariffs();
            tableModel = TariffsController.getInstance().getFullTariffsTableModelFromList(tariffsList);
            selectingForm = new SelectingForm(EditContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        TariffDTO selectedTariff = null;
                        for (TariffDTO option : tariffsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedTariff = option;
                                break;
                            }
                        }
                        currentTariff = selectedTariff;
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (currentTariff == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Select tariff before selecting options or save",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == addOptionButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<OptionDTO> optionsList;

            optionsList = TariffsController.getInstance().getPossibleOptions(currentTariff);
            tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
            selectingForm = new SelectingForm(EditContractManagerForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        OptionDTO selectedOption = null;
                        for (OptionDTO option : optionsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedOption = option;
                                break;
                            }
                        }

                        if (!removedOptions.contains(selectedOption))
                            addedOptions.add(selectedOption);
                        else
                            removedOptions.remove(selectedOption);

                        currentOptions.add(selectedOption);
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (e.getSource() == removeSelectedOptionButton) {
            if (selectedOptionsTable.getRowCount() == 0)
                return;

            int selectedRow;
            selectedRow = selectedOptionsTable.getSelectedRow();
            if (selectedRow == -1)
                return;

            Long selectedOptionId = Long.valueOf(
                    selectedOptionsTable.getModel().getValueAt(
                            selectedOptionsTable.convertRowIndexToModel(selectedRow), 0
                    ).toString()
            );

            OptionDTO selectedOption = null;
            for (OptionDTO option : currentOptions) {
                if (option.getId().equals(selectedOptionId)) {
                    selectedOption = option;
                    break;
                }
            }

            if (!addedOptions.contains(selectedOption))
                removedOptions.add(selectedOption);
            else
                addedOptions.remove(selectedOption);

            currentOptions.remove(selectedOption);
            updateAllFields();
            return;
        }

        if (e.getSource() == saveButton) {
            editableContract = new ContractDTO(
                    editableContract.getId(),
                    phoneField.getText(),
                    editableContract.getBlockedByClient(),
                    editableContract.getBlockedByManager()
            );

            try {
                ContractsController.getInstance().updateContract(
                        editableContract,
                        currentUser,
                        currentTariff,
                        new ArrayList<OptionDTO>(addedOptions),
                        new ArrayList<OptionDTO>(removedOptions)
                );
                JOptionPane.showMessageDialog(null, "Contract has been updated", "Success", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            } catch (IllegalArgumentException exception) {
                JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}