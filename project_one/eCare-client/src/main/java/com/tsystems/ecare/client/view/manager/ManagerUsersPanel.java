package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.client.view.UserRegistrationForm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class ManagerUsersPanel extends JPanel {
    private JPanel mainPanel;
    private JTable usersTable;
    private JButton refreshButton;
    private JButton showSelectedUserInfoButton;
    private JButton registrationButton;

    public ManagerUsersPanel() {
        super();

        addRefreshButtonListener();
        addShowSelectedUserInfoButtonListener();
        addRegistrationByttonListener();

        usersTable.setRowHeight(25);
        usersTable.setRowSelectionAllowed(true);

        setVisible(false);
    }

    public void updateUsersTable() {
        usersTable.setModel(UsersController.getInstance().getUsersTableModel());
    }

    private Long getSelectedRowId() {
        if(usersTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = usersTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                usersTable.getModel().getValueAt(
                        usersTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    private void addShowSelectedUserInfoButtonListener() {
        showSelectedUserInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();

                if (selectedRowId != null)
                    UsersController.getInstance().showUserInfo(
                            null,
                            UsersController.getInstance().getUserById(selectedRowId)
                    );
            }
        });
    }

    private void addRegistrationByttonListener() {
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                UserRegistrationForm registrationForm = new UserRegistrationForm(null);
            }
        });
    }

    private void addRefreshButtonListener() {
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                updateUsersTable();
            }
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);

        if (aFlag) updateUsersTable();
    }
}
