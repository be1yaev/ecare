package com.tsystems.ecare.client.view.client;

import com.tsystems.ecare.client.controller.ContractsController;
import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.client.view.manager.SelectingForm;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Andrew on 25.08.2014.
 */
public class CreateNewContractClientForm extends JFrame implements ActionListener {
    private JPanel mainPanel;
    private JButton selectTariffButton;
    private JTextField tariffNameField;
    private JTextField phoneField;
    private JButton createButton;
    private JButton backButton;
    private JTable selectedOptionsTable;
    private JButton addOptionButton;
    private JButton removeSelectedOptionButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;
    private ShopingCartPanel shoppingCartPanel;

    private JFrame parent;

    private TariffDTO currentTariff;
    private Set<OptionDTO> addedOptions;

    public CreateNewContractClientForm(JFrame parent) {
        super("eCare");

        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        selectedOptionsTable.setRowHeight(25);
        selectedOptionsTable.setRowSelectionAllowed(true);

        setupAllFieldsAndButtons();

        setVisible(true);
    }

    private void setupAllFieldsAndButtons() {
        currentTariff = null;
        addedOptions = new TreeSet<OptionDTO>();

        createButton.addActionListener(this);
        backButton.addActionListener(this);
        selectTariffButton.addActionListener(this);
        addOptionButton.addActionListener(this);
        removeSelectedOptionButton.addActionListener(this);

        updateAllFields();
    }

    private void updateAllFields() {
        if (currentTariff == null)
            tariffNameField.setText("Not selected");
        else
            tariffNameField.setText(currentTariff.getTitle());

        shoppingCartPanel.updateContractsTable();
        updateTable();
    }

    public void updateTable() {
        selectedOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(addedOptions)
                )
        );
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }

    private boolean checkPhoneNumber(String phone) {
        if (phone.length() != 11)
            return false;
        if (phone.contains(" "))
            return false;
        try {
            Long.getLong(phone);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == backButton) {
            dispose();
            return;
        }

        if (!checkPhoneNumber(phoneField.getText())) {
            JOptionPane.showMessageDialog(
                    null,
                    "Phone number must be comtain only 11 characters and only numbers",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == selectTariffButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<TariffDTO> tariffsList;

            tariffsList = TariffsController.getInstance().getAllTariffs();
            tableModel = TariffsController.getInstance().getFullTariffsTableModelFromList(tariffsList);
            selectingForm = new SelectingForm(CreateNewContractClientForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        TariffDTO selectedTariff = null;
                        for (TariffDTO option : tariffsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedTariff = option;
                                break;
                            }
                        }
                        currentTariff = selectedTariff;
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (currentTariff == null) {
            JOptionPane.showMessageDialog(
                    null,
                    "Select tariff before selecting options",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (e.getSource() == addOptionButton) {
            final TableModel tableModel;
            final SelectingForm selectingForm;
            final List<OptionDTO> optionsList;

            optionsList = TariffsController.getInstance().getPossibleOptions(currentTariff);
            tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
            selectingForm = new SelectingForm(CreateNewContractClientForm.this, tableModel);

            ActionListener listener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Long selectedOptionId;
                    selectedOptionId = selectingForm.getSelectedRowId();

                    if (selectedOptionId != null) {
                        OptionDTO selectedOption = null;
                        for (OptionDTO option : optionsList) {
                            if (option.getId().equals(selectedOptionId)) {
                                selectedOption = option;
                                break;
                            }
                        }
                        addedOptions.add(selectedOption);
                        updateAllFields();
                    }
                }
            };

            selectingForm.setSelectButtonListener(listener);
            return;
        }

        if (e.getSource() == removeSelectedOptionButton) {
            if (selectedOptionsTable.getRowCount() == 0)
                return;

            int selectedRow;
            selectedRow = selectedOptionsTable.getSelectedRow();
            if (selectedRow == -1)
                return;

            Long selectedOptionId = Long.valueOf(
                    selectedOptionsTable.getModel().getValueAt(
                            selectedOptionsTable.convertRowIndexToModel(selectedRow), 0
                    ).toString()
            );

            OptionDTO selectedOption = null;
            for (OptionDTO option : addedOptions) {
                if (option.getId().equals(selectedOptionId)) {
                    selectedOption = option;
                    break;
                }
            }

            addedOptions.remove(selectedOption);
            updateAllFields();
            return;
        }

        if (e.getSource() == createButton) {
            ContractDTO contractDTO;
            contractDTO = new ContractDTO(
                    null,
                    phoneField.getText(),
                    false,
                    false
            );

            try {
                ContractDTO createdContract;
                if (ContractsController.getInstance().addToCartNewContract(
                        contractDTO,
                        UsersController.getInstance().getCurrentUser(),
                        currentTariff,
                        new ArrayList<OptionDTO>(addedOptions))) {

                    JOptionPane.showMessageDialog(null, "Created contract has been added into shopping cart",
                            "Success",
                            JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Created contract can not be added into shopping cart",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } catch (IllegalArgumentException exception) {
                JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}