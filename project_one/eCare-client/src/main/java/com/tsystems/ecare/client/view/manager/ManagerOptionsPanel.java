package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.OptionsController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class ManagerOptionsPanel extends JPanel {
    private JTable optionsTable;
    private JButton changeSelectedOptionButton;
    private JButton newOptionButton;
    private JPanel mainPanel;
    private JButton refreshButton;
    private JButton deleteSelectedOptionButton;

    public ManagerOptionsPanel() {
        super();

        addNewOptionButtonListener();
        addRefreshButtonListener();
        addChangeButtonListener();
        addDeleteButtonListener();

        optionsTable.setRowHeight(25);
        optionsTable.setRowSelectionAllowed(true);

        setVisible(false);
    }

    public void updateOptionsTable() {
        optionsTable.setModel(OptionsController.getInstance().getAllOptionsTableModel());
    }

    private void addNewOptionButtonListener() {
        newOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                OptionsController.getInstance().showCreateNewOptionWindow(ManagerOptionsPanel.this);
                updateOptionsTable();
                System.out.println("TRATATA METHOD");
            }
        });
    }

    private Long getSelectedRowId() {
        if(optionsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = optionsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                optionsTable.getModel().getValueAt(
                        optionsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    private void addChangeButtonListener() {
        changeSelectedOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();

                if (selectedRowId != null) {
                    OptionsController.getInstance().showEditingOptionWindow(selectedRowId, ManagerOptionsPanel.this);
                }
            }
        });
    }

    private void addDeleteButtonListener() {
        deleteSelectedOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();
                if (selectedRowId != null) {
                    int result = JOptionPane.showConfirmDialog(
                            null,
                            "Are you sure you want to remove the option with the Id " + selectedRowId +"?",
                            "Removing confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE
                    );
                    if (result == JOptionPane.YES_OPTION) {
                        if( OptionsController.getInstance().deleteOption(selectedRowId)) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Option has been deleted",
                                    "Success",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Option can not be deleted",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        updateOptionsTable();
                    }
                }
            }
        });
    }

    private void addRefreshButtonListener() {
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                updateOptionsTable();
            }
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);

        if (aFlag) updateOptionsTable();
    }
}
