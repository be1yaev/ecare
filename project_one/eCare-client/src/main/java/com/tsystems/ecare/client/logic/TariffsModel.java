package com.tsystems.ecare.client.logic;

import com.tsystems.ecare.client.Connector;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class TariffsModel {
    private static TariffsModel modelInstance = null;

    public synchronized static TariffsModel getInstance() {
        if (modelInstance == null)
            modelInstance = new TariffsModel();
        return modelInstance;
    }

    private static final Logger logger = Logger.getLogger(TariffsModel.class.getName());

    private TariffsModel() {

    }


    public TableModel getAllTariffsTableModel() {
        return getTableModelFromTariffsList(getAllTariffs(), false);
    }

    public TableModel getTableModelFromTariffsList(List<TariffDTO> tariffsList, boolean shortView) {
        final String[] columns;
        final List<TariffDTO> data;

        data = tariffsList;
        columns = ((shortView) ?
                new String[] {
                        "Id",
                        "Title",
                } : new String[]{
                        "Id",
                        "Title",
                        "Description"
                }
        );

        TableModel tableModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return columns.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                TariffDTO tariff = data.get(row);
                switch (col) {
                    case 0:
                        // return String.valueOf(row+1);
                        return tariff.getId().toString();
                    case 1:
                        return tariff.getTitle();
                    case 2:
                        return tariff.getDescription();
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columns[column];
            }

            @Override
            public Class getColumnClass(int c) {
                return (String.class);
            }
        };

        return tableModel;
    }

    public List<TariffDTO> getAllTariffs() {
        // Get Options list for all users
        GetTariffsListCommand request = new GetTariffsListCommand();
        TariffsListCommand response = (TariffsListCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getTariffs();
    }

    public boolean deleteTariff(Long tariffId) {
        DeleteTariffCommand request = new DeleteTariffCommand(tariffId);
        DeleteResultCommand response = (DeleteResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getDeletedSuccessfully();
    }

    public TariffDTO createNewTariff(TariffDTO newTariff) {
        CreateTariffCommand request = new CreateTariffCommand(newTariff);
        CreateTariffConfirmationCommand response =
                (CreateTariffConfirmationCommand) Connector.getInstance().forwardCommandToServer(request);

        if (response.getCreatedTariff() == null)
            throw new IllegalArgumentException(response.getMessage());

        return response.getCreatedTariff();
    }

    public List<OptionDTO> getPossibleOptions(TariffDTO tariff) {
        GetPossibleOptionsListCommand request = new GetPossibleOptionsListCommand(tariff);
        OptionsListCommand response = (OptionsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getOptions();
    }

    public boolean updateTariff(TariffDTO changedTariff,
                                ArrayList<OptionDTO> addedPossibleOptions,
                                ArrayList<OptionDTO> removedPossibleOptions) {

        UpdateTariffCommand request = new UpdateTariffCommand(
                changedTariff,
                addedPossibleOptions,
                removedPossibleOptions
        );
        UpdateResultCommand response = (UpdateResultCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getUpdatedSuccessfully();
    }

    public TariffDTO getTariffById(Long tariffId) {
        GetTariffByIdCommand request = new GetTariffByIdCommand(tariffId);
        GetTariffResultCommand response = (GetTariffResultCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getTariff();
    }
}
