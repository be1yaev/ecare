package com.tsystems.ecare.client.view.client;

import com.tsystems.ecare.client.controller.ContractsController;
import com.tsystems.ecare.client.logic.ShoppingCart;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 25.08.2014.
 */
public class ShopingCartPanel extends JPanel implements ActionListener {
    private JPanel cartPanel;
    private JButton cancelChangesButton;
    private JButton acceptChangesButton;
    private JTable newContractsTable;
    private JButton changeSelectedContractButton;
    private JPanel mainPanel;
    private JTable changedContractsTable;

    public ShopingCartPanel() {
        super();

        cancelChangesButton.addActionListener(this);
        acceptChangesButton.addActionListener(this);
        changeSelectedContractButton.addActionListener(this);

        newContractsTable.setRowHeight(25);
        newContractsTable.setRowSelectionAllowed(true);
        changedContractsTable.setRowHeight(25);
        changedContractsTable.setRowSelectionAllowed(true);

        updateContractsTable();
        setVisible(true);
    }

    public void updateContractsTable() {
        newContractsTable.setModel(ContractsController.getInstance().getShoppingCartNewContractsTableModel());
        changedContractsTable.setModel(ContractsController.getInstance().getShoppingCartChangedContractsTableModel());
    }

    private Long getSelectedRowId() {
        if(newContractsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = newContractsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                newContractsTable.getModel().getValueAt(
                        newContractsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == acceptChangesButton) {
            if (ShoppingCart.getInstance().acceptAllChanges()) {
                JOptionPane.showMessageDialog(
                        null,
                        "All changes has been accepted",
                        "Success",
                        JOptionPane.INFORMATION_MESSAGE);
                updateContractsTable();
            }
        }

        if (e.getSource() == cancelChangesButton) {
            int result = JOptionPane.showConfirmDialog(
                    null,
                    "Are you sure you want to remove all changes from shopping cart?",
                    "Removing confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE
            );
            if (result == JOptionPane.YES_OPTION) {
                ShoppingCart.getInstance().clear();
                updateContractsTable();
            }
        }
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);

        if (aFlag) updateContractsTable();
    }
}
