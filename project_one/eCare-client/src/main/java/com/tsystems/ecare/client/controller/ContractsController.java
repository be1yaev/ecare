package com.tsystems.ecare.client.controller;

import com.tsystems.ecare.client.logic.ContractsModel;
import com.tsystems.ecare.client.logic.ShoppingCart;
import com.tsystems.ecare.client.view.client.CreateNewContractClientForm;
import com.tsystems.ecare.client.view.client.EditContractClientForm;
import com.tsystems.ecare.client.view.manager.CreateNewContractManagerForm;
import com.tsystems.ecare.client.view.manager.EditContractManagerForm;
import com.tsystems.ecare.common.command.request.CreateContractCommand;
import com.tsystems.ecare.common.command.request.UpdateContractCommand;
import com.tsystems.ecare.common.command.response.FullContractDataCommand;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;

/**
* Created by Andrew on 22.08.2014.
*/
public class ContractsController {
    private static ContractsController controllerInstance = null;
    private static final Logger logger = Logger.getLogger(ContractsController.class.getName());

    public synchronized static ContractsController getInstance() {
        if (controllerInstance == null) {
            controllerInstance = new ContractsController();
        }
        return controllerInstance;
    }

    private ContractsController() {
    }

    /**
     *
     * @return
     */
    public TableModel getUserContractsTableModel() {
        return ContractsModel.getInstance().getAllUserContractsTableModel();
    }

    /**
     *
     * @return
     */
    public TableModel getAllContractsTableModel() {
        return ContractsModel.getInstance().getAllContractsTableModel();
    }



    public TableModel getAllContractsWithTelephoneTableModel(String likePhoneNumber) {
        return ContractsModel.getInstance().getAllContractsWithTelephoneTableModel(likePhoneNumber);
    }

    public void showCreateNewContractManagerWindow(JComponent parent) {
        new CreateNewContractManagerForm(parent).setVisible(true);
    }

    public void showEditingOptionWindow(Long contractId, JComponent parent) {
        ContractDTO contractToFind;
        contractToFind = new ContractDTO();
        contractToFind.setId((contractId));

        showEditingOptionWindow(contractToFind, parent);
    }

    public void showEditingOptionWindow(ContractDTO contract, JComponent parent) {
        FullContractDataCommand contractInfo;
        contractInfo = ContractsModel.getInstance().getFullContractData(contract);
        showEditingOptionManagerWindow(contractInfo, parent);
    }

    public void showEditingOptionManagerWindow(FullContractDataCommand contractInfo, JComponent parent) {
        new EditContractManagerForm(parent, contractInfo).setVisible(true);
    }

    public ContractDTO createNewContract(
            ContractDTO contractDTO,
            UserDTO currentUser,
            TariffDTO currentTariff,
            List<OptionDTO> addedOptions) {

        return ContractsModel.getInstance().createNewContract(
                contractDTO,
                currentUser,
                currentTariff,
                addedOptions
        );
    }

    public boolean updateContract(ContractDTO editableContract,
                               UserDTO user,
                               TariffDTO tariff,
                               List<OptionDTO> addedOptions,
                               List<OptionDTO> removedOptions) {

        return ContractsModel.getInstance().updateContract(
                editableContract,
                user,
                tariff,
                addedOptions,
                removedOptions
        );
    }

    public boolean deleteContract(Long id) {
        return ContractsModel.getInstance().deleteContract(id);
    }

    public void showClientEditingOptionWindow(Long contractId, JFrame parent) {
        ContractDTO contractToFind;
        contractToFind = new ContractDTO();
        contractToFind.setId((contractId));

        showEditingOptionClientWindow(contractToFind, parent);
    }

    public void showEditingOptionClientWindow(ContractDTO contract, JFrame parent) {
        FullContractDataCommand contractInfo;
        contractInfo = ContractsModel.getInstance().getFullContractData(contract);
        showClientEditingOptionWindow(contractInfo, parent);
    }

    public void showClientEditingOptionWindow(FullContractDataCommand contractInfo,  JFrame parent) {
        new EditContractClientForm(contractInfo, parent).setVisible(true);
    }

    public void showCreateNewContractClientWindow(JFrame parent) {
        new CreateNewContractClientForm(parent).setVisible(true);
    }

    public TableModel getShoppingCartChangedContractsTableModel() {
        return ContractsModel.getInstance().getTableModelFromContractsList(
                ShoppingCart.getInstance().getChangedContracts()
        );
    }
    public TableModel getShoppingCartNewContractsTableModel() {
        return ContractsModel.getInstance().getTableModelFromContractsList(
                ShoppingCart.getInstance().getNewContracts()
        );
    }

    public boolean addToCartNewContract(
            ContractDTO contractDTO,
            UserDTO currentUser,
            TariffDTO currentTariff,
            ArrayList<OptionDTO> addedOptions) {

        CreateContractCommand command = new CreateContractCommand(
                contractDTO,
                currentUser,
                currentTariff,
                addedOptions
        );

        return ShoppingCart.getInstance().addNewContractToCart(command);
    }

    public boolean addToCartChangedContract(
            ContractDTO contractDTO,
            UserDTO currentUser,
            TariffDTO currentTariff,
            List<OptionDTO> addedOptions,
            List<OptionDTO> removedOptions) {

        UpdateContractCommand command = new UpdateContractCommand(
                contractDTO,
                currentUser,
                currentTariff,
                addedOptions,
                removedOptions
        );

        return ShoppingCart.getInstance().addChangedContractToCart(command);
    }

    public UserDTO getClientByContractId(Long id) {
        return ContractsModel.getInstance().getClientByContractId(id);
    }
}
