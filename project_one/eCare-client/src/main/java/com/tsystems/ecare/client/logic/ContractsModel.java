package com.tsystems.ecare.client.logic;

import com.tsystems.ecare.client.Connector;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.ContractDTO;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import com.tsystems.ecare.common.dto.UserDTO;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.List;


/**
 * Created by Andrew on 22.08.2014.
 */
public class ContractsModel {
    private static ContractsModel modelInstance = null;

    public synchronized static ContractsModel getInstance() {
        if (modelInstance == null)
            modelInstance = new ContractsModel();
        return modelInstance;
    }

    private static final Logger logger = Logger.getLogger(ContractsModel.class.getName());

    private ContractsModel() {

    }


    /**
     *
     * @return
     */
    public TableModel getAllUserContractsTableModel() {
        // Get contracts list for current user
        GetUserContractsListCommand request = new GetUserContractsListCommand(
                UsersModel.getInstance().getCurrentUser().getId()
        );
        ContractsListCommand response = (ContractsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return getTableModelFromContractsList(response.getContracts());
    }

    /**
     *
     * @return
     */
    public TableModel getTableModelFromContractsList(List<ContractDTO> contractsList) {
        final String[] columns;
        final List<ContractDTO> data = contractsList;

        columns = new String[] {
                "Id",
                "Phone number",
                "Status"
        };

        TableModel contractsTableModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return columns.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                ContractDTO contract = data.get(row);
                switch (col) {
                    case 0:
                        if (contract.getId() != null)
                            return contract.getId().toString();
                        return "-";
                    case 1:
                        return contract.getPhoneNumber();
                    case 2:
                        if (contract.getBlockedByManager() && contract.getBlockedByClient())
                            return "Blocked by client and manager";
                        else if (contract.getBlockedByManager())
                            return "Blocked by manager";
                        else if (contract.getBlockedByClient())
                            return "Blocked by client";
                        return "Active";
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columns[column];
            }

            @Override
            public Class getColumnClass(int c) {
                return (String.class);
            }
        };

        return contractsTableModel;
    }

    /**
     *
     * @return
     */
    public TableModel getAllContractsTableModel() {
        // Get contracts list for all users
        GetContractsListCommand request = new GetContractsListCommand();
        ContractsListCommand response = (ContractsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return getTableModelFromContractsList(response.getContracts());
    }

    public TableModel getAllContractsWithTelephoneTableModel(String likePhoneNumber) {
        // Get contracts list for all users
        GetContractsListCommand request = new GetContractsListCommand(likePhoneNumber);
        ContractsListCommand response = (ContractsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return getTableModelFromContractsList(response.getContracts());
    }

    public ContractDTO createNewContract(
            ContractDTO contractDTO,
            UserDTO currentUser,
            TariffDTO currentTariff,
            List<OptionDTO> addedOptions) {

        CreateContractCommand request = new CreateContractCommand(
                contractDTO,
                currentUser,
                currentTariff,
                addedOptions
        );
        return createNewContract(request);
    }

    public ContractDTO createNewContract(CreateContractCommand command) {
        CreateContractCommand request = command;
        CreateContractConfirmationCommand response =
                (CreateContractConfirmationCommand) Connector.getInstance().forwardCommandToServer(request);

        if (response.getCreatedContract() == null)
            throw new IllegalArgumentException(response.getMessage());

        return response.getCreatedContract();
    }

    public FullContractDataCommand getFullContractData(ContractDTO contract) {
        GetFullContractDataRequestCommand request = new GetFullContractDataRequestCommand(contract.getId());
        FullContractDataCommand response = (FullContractDataCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response;
    }

    public boolean updateContract(ContractDTO editableContract,
                                  UserDTO user,
                                  TariffDTO tariff,
                                  List<OptionDTO> addedOptions,
                                  List<OptionDTO> removedOptions) {


        UpdateContractCommand request = new UpdateContractCommand(
                editableContract,
                user,
                tariff,
                addedOptions,
                removedOptions
        );
        return updateContract(request);
    }

    public boolean updateContract(UpdateContractCommand command) {
        UpdateContractCommand request = command;
        UpdateResultCommand response = (UpdateResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getUpdatedSuccessfully();
    }

    public boolean deleteContract(Long id) {
        DeleteContractCommand request = new DeleteContractCommand(id);
        DeleteResultCommand response = (DeleteResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getDeletedSuccessfully();
    }

    public UserDTO getClientByContractId(Long id) {
        GetUserByContractIdCommand request = new GetUserByContractIdCommand(id);
        GetUserResultCommand response = (GetUserResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getUser();
    }
}
