package com.tsystems.ecare.client.logic;

import com.tsystems.ecare.client.Connector;
import com.tsystems.ecare.common.command.request.CreateContractCommand;
import com.tsystems.ecare.common.command.request.UpdateAndCreateContractsFromCartCommand;
import com.tsystems.ecare.common.command.request.UpdateContractCommand;
import com.tsystems.ecare.common.command.response.UpdateResultCommand;
import com.tsystems.ecare.common.dto.ContractDTO;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.util.*;

/**
 * Created by Andrew on 25.08.2014.
 */
public class ShoppingCart {
    private static final Logger logger = Logger.getLogger(ShoppingCart.class.getName());
    private static ShoppingCart instance;

    private Map<Long, UpdateContractCommand> changedContracts;
    private Map<String, CreateContractCommand> newContracts;

    public synchronized static ShoppingCart getInstance() {
        if (instance == null)
            instance = new ShoppingCart();
        return instance;
    }

    private ShoppingCart() {
        changedContracts = Collections.synchronizedMap(new HashMap<Long, UpdateContractCommand>());
        newContracts = Collections.synchronizedMap(new HashMap<String, CreateContractCommand>());
    }

    public boolean addChangedContractToCart(UpdateContractCommand command) {
        if (changedContracts.containsKey(command.getUpdatedContractDTO().getId()))
            return false;
        changedContracts.put(command.getUpdatedContractDTO().getId(), command);
        return true;
    }

    public boolean addNewContractToCart(CreateContractCommand command) {
        if (newContracts.containsKey(command.getContractDTO().getPhoneNumber()))
            return false;
        newContracts.put(command.getContractDTO().getPhoneNumber(), command);
        return true;
    }

    public boolean acceptAllChanges() {
        UpdateResultCommand response = (UpdateResultCommand) Connector.getInstance().forwardCommandToServer(
                new UpdateAndCreateContractsFromCartCommand(
                        new ArrayList<CreateContractCommand>(newContracts.values()),
                        new ArrayList<UpdateContractCommand>(changedContracts.values())
                )
        );

        if (!response.getUpdatedSuccessfully()) {
            JOptionPane.showMessageDialog(
                    null,
                    response.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }

        clear();
        return true;
    }

    public void clear() {
        changedContracts.clear();
        newContracts.clear();
    }

    public List<ContractDTO> getChangedContracts() {
        List<ContractDTO> result = new ArrayList<ContractDTO>();
        for (UpdateContractCommand command : changedContracts.values()) {
            result.add(command.getUpdatedContractDTO());
        }
        return result;
    }

    public List<ContractDTO> getNewContracts() {
        List<ContractDTO> result = new ArrayList<ContractDTO>();
        for (CreateContractCommand command : newContracts.values()) {
            result.add(command.getContractDTO());
        }
        return result;
    }
}
