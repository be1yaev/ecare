package com.tsystems.ecare.client.view;

import com.tsystems.ecare.common.dto.UserDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 25.08.2014.
 */
public class UserInformationFrame extends JFrame {
    private JPanel mainPanel;
    private JPanel titles;
    private JTextField surnameField;
    private JTextField cityField;
    private JTextField countryField;
    private JTextField passportNumberField;
    private JTextField patronymicField;
    private JTextField firstnameField;
    private JTextField emailField;
    private JTextArea addressTextArea;
    private JTextArea passportDescriptionTextArea;
    private JButton editButton;
    private JButton backButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;
    private JTextField roleField;

    private JFrame parent;

    private UserDTO userInfo;

    public UserInformationFrame(JFrame parent, UserDTO userInfo) {
        super("eCare");

        this.userInfo = userInfo;
        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                return;
            }
        });

        setupAllFields(userInfo);

        setVisible(true);
    }

    public void setupAllFields(UserDTO userInfo) {
        surnameField.setText(userInfo.getSurname());
        cityField.setText(userInfo.getCity());
        countryField.setText(userInfo.getCountry());
        passportNumberField.setText(userInfo.getPassportNumber());
        patronymicField.setText(userInfo.getPatronymic());
        firstnameField.setText(userInfo.getFirstname());
        emailField.setText(userInfo.getEmail());
        addressTextArea.setText(userInfo.getAddress());
        passportDescriptionTextArea.setText(userInfo.getPassportDescription());
        roleField.setText(userInfo.getRole().toString());
    }

    @Override
    public void dispose() {
        super.dispose();
        if (parent != null) parent.setVisible(true);
    }
}
