package com.tsystems.ecare.client.view;

import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.common.dto.UserDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UserRegistrationForm extends JFrame {
    private JPanel registrationPanel;
    private JButton registrationButton;
    private JButton backButton;
    private JPanel titles;
    private JTextField surnameField;
    private JTextField firstnameField;
    private JTextField patronymicField;
    private JTextField passportNumberField;
    private JTextArea passportDescriptionTextArea;
    private JTextField countryField;
    private JTextField cityField;
    private JTextArea addressTextArea;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JLabel logoLabel;
    private JPanel mainPanel;

    private JFrame parent;

    public UserRegistrationForm(JFrame parent) {
        super("eCare");

        this.parent = parent;

        setContentPane(registrationPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        //setSize(640, 700);
        pack();
        setLocationRelativeTo(null);

        addBackButtonListener();
        addRegistrationButtonListener();

        setVisible(true);
    }

    private void addRegistrationButtonListener() {
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                UserDTO userInfo;
                String password;

                password = new String(passwordField.getPassword());
                userInfo = new UserDTO(
                        null,
                        surnameField.getText().trim(),
                        firstnameField.getText().trim(),
                        patronymicField.getText().trim(),
                        passportNumberField.getText().trim(),
                        passportDescriptionTextArea.getText().trim(),
                        countryField.getText().trim(),
                        cityField.getText().trim(),
                        addressTextArea.getText().trim(),
                        emailField.getText().trim(),
                        null
                );

                try {
                    if (UsersController.getInstance().registration(userInfo, password)) {
                        JOptionPane.showMessageDialog(null, "User has been registered", "Success", JOptionPane.INFORMATION_MESSAGE);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "User has not registered", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IllegalStateException exception) {
                    JOptionPane.showMessageDialog(null, exception.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void addBackButtonListener() {
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    @Override
    public void dispose() {
        super.dispose();
        if (parent != null)
            parent.setVisible(true);
    }
}
