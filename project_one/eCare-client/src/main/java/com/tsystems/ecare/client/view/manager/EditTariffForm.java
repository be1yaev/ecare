package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Andrew on 24.08.2014.
 */
public class EditTariffForm extends JFrame {
    private JPanel mainPanel;
    private JButton saveButton;
    private JButton cancelButton;
    private JTable possibleOptionsTable;
    private JButton addOptionButton;
    private JButton removeSelectedOptionButton;
    private JPanel titles;
    private JTextField titleField;
    private JTextArea descriptionTextArea;
    private JLabel logoLabel;
    private JPanel backgroundPanel;

    private JComponent parent;

    private TariffDTO currentTariff;

    private Set<OptionDTO> addedPossibleOptions;
    private Set<OptionDTO> removedPossibleOptions;
    private Set<OptionDTO> currentPossibleOptions;

    public EditTariffForm(JComponent parent, TariffDTO currentTariff) {
        super("eCare");

        this.currentTariff = currentTariff;
        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        addCancelButtonListener();
        addSaveButtonListener();
        addDataChangingButtonsListeners();

        possibleOptionsTable.setRowHeight(25);
        possibleOptionsTable.setRowSelectionAllowed(true);

        setupAllFields();

        setVisible(true);
    }
    private void setupAllFields() {
        titleField.setText(currentTariff.getTitle());
        descriptionTextArea.setText(currentTariff.getDescription());

        addedPossibleOptions = new TreeSet<OptionDTO>();
        removedPossibleOptions = new TreeSet<OptionDTO>();
        currentPossibleOptions = new TreeSet<OptionDTO>(
                TariffsController.getInstance().getPossibleOptions(currentTariff)
        );

        updateTable();
    }

    public void updateTable() {
//        Set<OptionDTO> posOptions = new TreeSet<OptionDTO>();
//
//        //removedPossibleOptions.removeAll(addedPossibleOptions);
//
//        posOptions.addAll(currentPossibleOptions);
//        posOptions.addAll(addedPossibleOptions);
//        posOptions.removeAll(removedPossibleOptions);

        possibleOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(currentPossibleOptions)
                )
        );

        //currentPossibleOptions = new ArrayList<OptionDTO>(posOptions);
    }

    private void addCancelButtonListener() {
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    private void addDataChangingButtonsListeners() {
        addOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                final TableModel tableModel;
                final SelectingForm selectingForm;
                final List<OptionDTO> optionsList;

                optionsList = OptionsController.getInstance().getAllOptions();
                tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
                selectingForm = new SelectingForm(EditTariffForm.this, tableModel);

                ActionListener listener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Long selectedOptionId;
                        selectedOptionId = selectingForm.getSelectedRowId();

                        if (selectedOptionId != null) {
                            OptionDTO selectedOption = null;
                            for (OptionDTO option : optionsList) {
                                if (option.getId().equals(selectedOptionId)) {
                                    selectedOption = option;
                                    break;
                                }
                            }

                            if (!removedPossibleOptions.contains(selectedOption))
                                addedPossibleOptions.add(selectedOption);
                            else
                                removedPossibleOptions.remove(selectedOption);

                            currentPossibleOptions.add(selectedOption);
                            updateTable();
                        }
                    }
                };

                selectingForm.setSelectButtonListener(listener);
            }
        });

        removeSelectedOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (possibleOptionsTable.getRowCount() == 0)
                    return;

                int selectedRow;
                selectedRow = possibleOptionsTable.getSelectedRow();
                if (selectedRow == -1)
                    return;

                Long selectedOptionId = Long.valueOf(
                        possibleOptionsTable.getModel().getValueAt(
                                possibleOptionsTable.convertRowIndexToModel(selectedRow), 0
                        ).toString()
                );

                OptionDTO selectedOption = null;
                for (OptionDTO option : currentPossibleOptions) {
                    if (option.getId().equals(selectedOptionId)) {
                        selectedOption = option;
                        break;
                    }
                }

                if (!addedPossibleOptions.contains(selectedOption))
                    removedPossibleOptions.add(selectedOption);
                else
                    addedPossibleOptions.remove(selectedOption);

                currentPossibleOptions.remove(selectedOption);
                updateTable();
            }
        });
    }

    private void addSaveButtonListener() {
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                TariffDTO changedTariff = new TariffDTO(
                        currentTariff.getId(),
                        titleField.getText(),
                        descriptionTextArea.getText()
                );

                try {
                    TariffsController.getInstance().updateTariff(
                            changedTariff,
                            new ArrayList<OptionDTO>(addedPossibleOptions),
                            new ArrayList<OptionDTO>(removedPossibleOptions)
                    );
                    JOptionPane.showMessageDialog(null, "Tariff has been updated", "Success", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }
}
