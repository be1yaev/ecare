package com.tsystems.ecare.client.view.client;

import com.tsystems.ecare.client.controller.ContractsController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 22.08.2014.
 */
public class ClientMainForm extends JFrame {
    private JPanel ClientMainPanel;
    private JLabel logoLabel;
    private JTable contractsTable;
    private JButton refreshButton;
    private JButton changeButton;
    private JButton createNewContractButton;
    private ShopingCartPanel shoppingCartPanel;

    public ClientMainForm() {
        super("eCare");

        setContentPane(ClientMainPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(true);
        pack();
        setLocationRelativeTo(null);

        addRefreshButtonListener();
        addNewContractButtonListener();
        addChangeButtonListener();

        updateData();
        contractsTable.setRowHeight(25);
        contractsTable.setRowSelectionAllowed(true);

        setVisible(true);
    }

    public void updateData() {
        contractsTable.setModel(ContractsController.getInstance().getUserContractsTableModel());
        shoppingCartPanel.updateContractsTable();
    }

    private Long getSelectedRowId() {
        if(contractsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = contractsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                contractsTable.getModel().getValueAt(
                        contractsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    private void addNewContractButtonListener() {
        createNewContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                ContractsController.getInstance().showCreateNewContractClientWindow(ClientMainForm.this);
            }
        });
    }

    private void addChangeButtonListener() {
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedId;
                selectedId = getSelectedRowId();
                if (selectedId != null)
                    ContractsController.getInstance()
                            .showClientEditingOptionWindow(selectedId, ClientMainForm.this);
            }
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (aFlag) updateData();
    }

    private void addRefreshButtonListener() {
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                updateData();
            }
        });
    }
}
