package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.TariffsController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class ManagerTariffsPanel extends JPanel {
    private JTable tariffsTable;
    private JButton deleteSelectedTariffButton;
    private JButton newTariffButton;
    private JButton refreshButton;
    private JPanel mainPanel;
    private JButton changeSelectedTariffButton;

    public ManagerTariffsPanel() {
        super();


        addNewButtonListener();
        addRefreshButtonListener();
        addChangeButtonListener();
        addDeleteButtonListener();

        tariffsTable.setRowHeight(25);
        tariffsTable.setRowSelectionAllowed(true);

        setVisible(false);
    }

    public void updateTariffsTable() {
        tariffsTable.setModel(TariffsController.getInstance().getAllTariffsTableModel());
    }

    private void addChangeButtonListener() {
        changeSelectedTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();

                if (selectedRowId != null) {
                    TariffsController.getInstance().showEditingTariffWindow(selectedRowId, ManagerTariffsPanel.this);
                }
            }
        });
    }

    private void addRefreshButtonListener() {
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                updateTariffsTable();
            }
        });
    }

    private void addNewButtonListener() {
        newTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                TariffsController.getInstance().showCreateNewTariffWindow(ManagerTariffsPanel.this);
                updateTariffsTable();
            }
        });
    }

    private Long getSelectedRowId() {
        if(tariffsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = tariffsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                tariffsTable.getModel().getValueAt(
                        tariffsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    private void addDeleteButtonListener() {
        deleteSelectedTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();
                if (selectedRowId != null) {
                    int result = JOptionPane.showConfirmDialog(
                            null,
                            "Are you sure you want to remove the tariff with the Id " + selectedRowId +"?",
                            "Removing confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE
                    );
                    if (result == JOptionPane.YES_OPTION) {
                        if( TariffsController.getInstance().deleteTariff(selectedRowId)) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Tariff has been deleted",
                                    "Success",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Tariff can not be deleted",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        updateTariffsTable();
                    }
                }
            }
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);

        if (aFlag) updateTariffsTable();
    }
}
