package com.tsystems.ecare.client.controller;

import com.tsystems.ecare.client.logic.OptionsModel;
import com.tsystems.ecare.client.view.manager.CreateNewOptionForm;
import com.tsystems.ecare.client.view.manager.EditOptionForm;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class OptionsController {
    private static OptionsController controllerInstance = null;

    public synchronized static OptionsController getInstance() {
        if (controllerInstance == null) {
            controllerInstance = new OptionsController();
        }
        return controllerInstance;
    }

    private static final Logger logger = Logger.getLogger(OptionsController.class.getName());

    private OptionsController() {

    }

    /**
     *
     * @return result table model
     */
    public TableModel getAllOptionsTableModel() {
        return OptionsModel.getInstance().getAllOptionsTableModel();
    }

    /**
     *
     * @param newOption
     * @return
     * @throws IllegalArgumentException
     */
    public OptionDTO createNewOption(OptionDTO newOption) throws IllegalArgumentException {
        return OptionsModel.getInstance().createNewOption(newOption);
    }

    /**
     *
     * @param optionId
     * @param parent
     */
    public void showEditingOptionWindow(Long optionId, JComponent parent) {
        OptionDTO option;
        option = OptionsModel.getInstance().getOptionById(optionId);
        new EditOptionForm(parent, option).setVisible(true);
    }

    /**
     *
     * @param option
     * @param parent
     */
    public void showEditingOptionWindow(OptionDTO option, JComponent parent) {
        new EditOptionForm(parent, option).setVisible(true);
    }

    /**
     *
     */
    public void showCreateNewOptionWindow(JComponent parent) {
        new CreateNewOptionForm(parent).setVisible(true);
    }

    /**
     *
     * @param options
     * @return
     */
    public TableModel getFullOptionsTableModelFromList(List<OptionDTO> options) {
        return OptionsModel.getInstance().getTableModelFromOptionsList(options, false);
    }

    /**
     *
     * @param options
     * @return
     */
    public TableModel getShortOptionsTableModelFromList(List<OptionDTO> options) {
        return OptionsModel.getInstance().getTableModelFromOptionsList(options, true);
    }

    public List<OptionDTO> getIncompatibleOptions(OptionDTO option) {
        return OptionsModel.getInstance().getIncompatibleOptions(option);
    }

    public List<OptionDTO> getDependentOptions(OptionDTO option) {
        return OptionsModel.getInstance().getDependentOptions(option);
    }

    public List<TariffDTO> getPossibleTariffs(OptionDTO option) {
        return OptionsModel.getInstance().getPossibleTariffs(option);
    }

    public boolean updateOption(OptionDTO changedOption,
                                List<OptionDTO> addedIncompatibleOptions,
                                List<OptionDTO> addedDependentOptions,
                                List<TariffDTO> addedPossibleTariffs,
                                List<OptionDTO> removedIncompatibleOptions,
                                List<OptionDTO> removedDependentOptions,
                                List<TariffDTO> removedPossibleTariffs) {

        return OptionsModel.getInstance().updateOption(
                changedOption,
                addedIncompatibleOptions,
                addedDependentOptions,
                addedPossibleTariffs,
                removedIncompatibleOptions,
                removedDependentOptions,
                removedPossibleTariffs
        );
    }

    public List<OptionDTO> getAllOptions() {
        return OptionsModel.getInstance().getAllOptions();
    }

    public boolean deleteOption(Long optionId) {
        return OptionsModel.getInstance().deleteOption(optionId);
    }
}
