package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.common.dto.OptionDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class CreateNewOptionForm extends JFrame {
    private JPanel mainPanel;
    private JPanel titles;
    private JTextField titleField;
    private JTextArea descriptionTextArea;
    private JButton createButton;
    private JButton backButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;
    private JTextField connectionCostField;
    private JTextField priceField;

    private JComponent parent;

    public CreateNewOptionForm(JComponent parent) {
        super("eCare");

        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        addBackButtonListener();
        addCreateButtonListener();

        setVisible(true);
    }

    private void addCreateButtonListener() {
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                OptionDTO optionInfo;

                try {
                    optionInfo = new OptionDTO(
                            null,
                            titleField.getText(),
                            descriptionTextArea.getText(),
                            Float.valueOf(priceField.getText()),
                            Float.valueOf(connectionCostField.getText())
                    );

                    try {
                        OptionDTO createdOption;
                        createdOption = OptionsController.getInstance().createNewOption(optionInfo);
                        JOptionPane.showMessageDialog(null, "Option has been created", "Success", JOptionPane.INFORMATION_MESSAGE);
                        OptionsController.getInstance().showEditingOptionWindow(createdOption, parent);
                        dispose();
                    } catch (IllegalArgumentException e) {
                        JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Value of \"Price\" or \"Connsection cost\" is incorrect.\n" +
                            "Fill this fields in next format: 5.30",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void addBackButtonListener() {
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }
}
