package com.tsystems.ecare.client.view;

import com.tsystems.ecare.client.controller.UsersController;
import com.tsystems.ecare.common.dto.UserDTO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class UserPanel extends JPanel {
    private JButton logoutButton;
    private JButton goToProfileButton;
    private JPanel mainPanel;
    private JLabel email;
    private JLabel role;
    private JLabel name;
    private JPanel userData;

    public UserPanel() {
        super();

        addLogoutButtonListener();
        addToProfileButtonListener();

        updateUserData();

        setVisible(true);
    }

    public void updateUserData() {
        UserDTO userInfo;
        userInfo = UsersController.getInstance().getCurrentUser();
        email.setText(userInfo.getEmail());
        role.setText(userInfo.getRole().toString());
        name.setText(UsersController.getInstance().getShortUserNameFromUser(userInfo));
    }

    private void addToProfileButtonListener() {
        goToProfileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                new UserInformationFrame(null, UsersController.getInstance().getCurrentUser());
            }
        });
    }

    private void addLogoutButtonListener() {
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                // TODO: logout
                System.exit(666);
            };
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);
        updateUserData();
    }
}
