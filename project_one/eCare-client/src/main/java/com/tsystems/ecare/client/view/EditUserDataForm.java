package com.tsystems.ecare.client.view;

import javax.swing.*;

/**
 * Created by Andrew on 25.08.2014.
 */
public class EditUserDataForm {
    private JPanel mainPanel;
    private JPanel titles;
    private JTextField surnameField;
    private JPasswordField passwordField;
    private JTextField cityField;
    private JTextField countryField;
    private JTextField passportNumberField;
    private JTextField patronymicField;
    private JTextField firstnameField;
    private JTextField emailField;
    private JTextArea addressTextArea;
    private JTextArea passportDescriptionTextArea;
    private JButton saveButton;
    private JButton backButton;
    private JLabel logoLabel;
    private JPanel backgroundPanel;
}
