package com.tsystems.ecare.client;

import com.tsystems.ecare.common.command.Command;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

/**
 * Created by Andrew on 21.08.2014.
 */
public class Connector {
    private static final Logger logger = Logger.getLogger(Connector.class.getName());
    private static Connector connectorInstance;

    public final String ADDRESS = "localhost";
    public final int PORT = 9999;

    private Connector() {

    }

    public synchronized static Connector getInstance() {
        if (connectorInstance == null)
            connectorInstance = new Connector();
        return connectorInstance;
    }

    public Command forwardCommandToServer(Command command) {
        logger.info("Request to server: " + command);
        Command response = null;

        try {
            Socket client = new Socket(ADDRESS, PORT);

            // Request to server
            ObjectOutputStream requestStream = new ObjectOutputStream(new BufferedOutputStream(client.getOutputStream()));
            requestStream.writeObject(command);
            requestStream.flush();

            // Response from server
            ObjectInputStream responseStream = new ObjectInputStream(new BufferedInputStream(client.getInputStream()));
            response = (Command) responseStream.readObject();

            client.close();
        } catch (IOException e) {
            logger.error("Error message exchange with the server: " + e.getMessage());
//            JOptionPane.showMessageDialog(
//                    null,
//                    "Error message exchange with the server: " + e.getMessage(),
//                    "Error",
//                    JOptionPane.ERROR_MESSAGE
//            );
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        }

        logger.info("Server response: " + response);
        if (response == null)
            throw new IllegalStateException("Server is not responding");

        return response;
    }
}
