package com.tsystems.ecare.client.view.manager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class ManagerMainForm extends JFrame {
    private JLabel logoLabel;
    private JButton usersButton;
    private JButton optionsButton;
    private JButton contractsButton;
    private JButton tariffsButton;
    private JPanel buttonsPanel;
    private JPanel managerMainPanel;
    private JPanel dataPanel;

    private ManagerUsersPanel managerUsersPanel;
    private ManagerContractsPanel managerContractsPanel;
    private ManagerOptionsPanel managerOptionsPanel;
    private ManagerTariffsPanel managerTariffsPanel;

    private JPanel currentPanel = null;
    private JButton pressedButton = null;

    private enum PanelType {
        USERS,
        CONTRACTS,
        OPTIONS,
        TARIFFS
    }

    public ManagerMainForm() {
        super("eCare");

        setContentPane(managerMainPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(true);
        pack();
        setLocationRelativeTo(null);

        addByttonsListeners();

        setVisibleDataPanel(PanelType.USERS);
        setVisible(true);
    }

    private void setVisibleDataPanel(PanelType newPanelType) {
        if (pressedButton != null)
            pressedButton.setEnabled(true);
        if (currentPanel != null)
            currentPanel.setVisible(false);

        switch (newPanelType) {
            case USERS:
                pressedButton = usersButton;
                currentPanel = managerUsersPanel;
                break;
            case CONTRACTS:
                pressedButton = contractsButton;
                currentPanel = managerContractsPanel;
                break;
            case OPTIONS:
                pressedButton = optionsButton;
                currentPanel = managerOptionsPanel;
                break;
            case TARIFFS:
                pressedButton = tariffsButton;
                currentPanel = managerTariffsPanel;
                break;
        }

        pressedButton.setEnabled(false);
        currentPanel.setVisible(true);
    }

    private void addByttonsListeners() {
        usersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisibleDataPanel(PanelType.USERS);
            }
        });

        contractsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisibleDataPanel(PanelType.CONTRACTS);
            }
        });

        tariffsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisibleDataPanel(PanelType.TARIFFS);
            }
        });

        optionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                setVisibleDataPanel(PanelType.OPTIONS);
            }
        });
    }
}
