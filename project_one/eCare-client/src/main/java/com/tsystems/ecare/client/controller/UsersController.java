package com.tsystems.ecare.client.controller;

import com.tsystems.ecare.client.logic.UsersModel;
import com.tsystems.ecare.client.view.UserInformationFrame;
import com.tsystems.ecare.client.view.client.ClientMainForm;
import com.tsystems.ecare.client.view.manager.ManagerMainForm;
import com.tsystems.ecare.common.dto.UserDTO;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UsersController {
    private static UsersController controllerInstance = null;

    public synchronized static UsersController getInstance() {
        if (controllerInstance == null) {
            controllerInstance = new UsersController();
        }
        return controllerInstance;
    }

    private static final Logger logger = Logger.getLogger(UsersController.class.getName());

    private UsersController() {

    }

    /**
     *
     * @param userEmail
     * @param userPassword
     * @throws IllegalArgumentException if some fields are filled incorrectly
     * @throws VerifyError if user with such email and password has not found
     */
    public void login(String userEmail, String userPassword) throws IllegalArgumentException, VerifyError {
        boolean isUserVerified = UsersModel.getInstance().login(userEmail, userPassword);

        if (isUserVerified) {
            switch (UsersModel.getInstance().getCurrentUser().getRole()) {
                case CLIENT:
                    ClientMainForm clientForm = new ClientMainForm();
                    clientForm.setVisible(true);
                    break;
                case MANAGER:
                    ManagerMainForm managerForm = new ManagerMainForm();
                    managerForm.setVisible(true);
                    break;
                case ADMIN:
                    // TODO: go to admin main page
                    break;
            }
        } else {
            throw new VerifyError("User with such email and password has not found.");
        }
    }

    /**
     *
     * @param userInfo
     * @param password
     * @return id of new user or null, if user has not registered
     */
    public boolean registration(UserDTO userInfo, String password) {
        return UsersModel.getInstance().registration(userInfo, password);
    }

    /**
     *
     * @return
     */
    public TableModel getUsersTableModel() {
        return UsersModel.getInstance().getFullUsersTableModel();
    }

    /**
     *
     * @return
     */
    public UserDTO getCurrentUser() {
        return UsersModel.getInstance().getCurrentUser();
    }

    public List<UserDTO> getAllUsers() {
        return UsersModel.getInstance().getAllUsers();
    }

    public TableModel getFullUsersTableModelFromList(List<UserDTO> clientsList) {
        return UsersModel.getInstance().getFullUsersTableModelFromList(clientsList);
    }
    public TableModel getShortUsersTableModelFromList(List<UserDTO> clientsList) {
        return UsersModel.getInstance().getShortUsersTableModelFromList(clientsList);
    }

    public String getShortUserNameFromUser(UserDTO userInfo) {
        return UsersModel.getInstance().getShortUserNameFromUser(userInfo);
    }

    public void showUserInfo(JFrame parent, UserDTO clientByContractId) {
        if (clientByContractId != null)
            new UserInformationFrame(parent, clientByContractId);
    }

    public UserDTO getUserById(Long id) {
        return UsersModel.getInstance().getUserById(id);
    }
}
