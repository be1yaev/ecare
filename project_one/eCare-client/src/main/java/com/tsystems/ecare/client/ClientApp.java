package com.tsystems.ecare.client;

import com.tsystems.ecare.client.view.LoginForm;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.IOException;

/**
 * Hello world!
 *
 */
public class ClientApp {
    private static final Logger logger = Logger.getLogger(ClientApp.class.getName());


    public static void main(String[] args) throws IOException {

        // Set "Nimbus Look and Feel" frames style
        String nimbusLookAndFeelClassName = getLookAndFeelClassName("Nimbus");
        if (nimbusLookAndFeelClassName != null) {
            try {
                UIManager.setLookAndFeel(nimbusLookAndFeelClassName);
            } catch (Exception e) {
                logger.error("Error applying Swing theme: " + e.getMessage());
            }
        }

        LoginForm loginForm = new LoginForm();
    }

    public static String getLookAndFeelClassName(String nameSnippet) {
        UIManager.LookAndFeelInfo[] plafs = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo info : plafs) {
            if (info.getName().contains(nameSnippet)) {
                return info.getClassName();
            }
        }
        return null;
    }
}