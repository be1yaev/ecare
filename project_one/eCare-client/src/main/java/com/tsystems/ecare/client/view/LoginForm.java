package com.tsystems.ecare.client.view;

import com.tsystems.ecare.client.controller.UsersController;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 21.08.2014.
 */
public class LoginForm extends JFrame {
    private JPanel LoginPanel;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JButton registrationButton;

    private static final Logger logger = Logger.getLogger(LoginForm.class);

    public LoginForm() {
        super("eCare");

        setContentPane(LoginPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(350, 250);
        setLocationRelativeTo(null);
        //pack();

        addLoginButtonListener();
        addRegistrationByttonListener();

        setVisible(true);
    }

    private void addLoginButtonListener() {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String emailText = emailField.getText();
                String passwordText = new String(passwordField.getPassword());

                try {
                    UsersController.getInstance().login(emailText, passwordText);
                    dispose();
                } catch (IllegalArgumentException exception) {
                    logger.error(exception.getMessage());
                    JOptionPane.showMessageDialog(null, exception.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
                } catch (VerifyError exception) {
                    logger.error(exception.getMessage());
                    JOptionPane.showMessageDialog(null, exception.getMessage(), "Authorization error", JOptionPane.ERROR_MESSAGE);
                } catch (IllegalStateException exception) {
                    logger.error(exception.getMessage());
                    JOptionPane.showMessageDialog(null, exception.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void addRegistrationByttonListener() {
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                UserRegistrationForm registrationForm = new UserRegistrationForm(LoginForm.this);
                LoginForm.this.setVisible(false);
                registrationForm.setVisible(true);
            }
        });
    }
}
