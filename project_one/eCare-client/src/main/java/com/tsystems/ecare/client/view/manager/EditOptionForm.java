package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.OptionsController;
import com.tsystems.ecare.client.controller.TariffsController;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Andrew on 23.08.2014.
 */
public class EditOptionForm extends JFrame {
    private JPanel mainPanel;
    private JPanel titles;
    private JTextField titleField;
    private JTextArea descriptionTextArea;
    private JTextField priceField;
    private JTextField connectionCostField;
    private JButton saveButton;
    private JButton cancelButton;
    private JLabel logoLabel;
    private JButton addIncompatibleOptionButton;
    private JButton addPossibleTariffButton1;
    private JButton addDependentOptionButton;
    private JButton removeSelectedIncompatibleOptionButton;
    private JButton removeSelectedPossibleTariffButton;
    private JPanel backgroundPanel;
    private JTable incompatibleOptionsTable;
    private JScrollPane dependentOptionTable;
    private JTable possibleTariffsTable;
    private JTable dependentOptionsTable;
    private JButton removeSelectedDependentOptionButton;

    private JComponent parent;

    private OptionDTO currentOption;

    private Set<OptionDTO> addedIncompatibleOptions;
    private Set<OptionDTO> addedDependentOptions;
    private Set<TariffDTO> addedPossibleTariffs;

    private Set<OptionDTO> removedIncompatibleOptions;
    private Set<OptionDTO> removedDependentOptions;
    private Set<TariffDTO> removedPossibleTariffs;

    private List<OptionDTO> currentIncompatibleOptions;
    private List<OptionDTO> currentDependentOptions;
    private List<TariffDTO> currentPossibleTariffs;

    public EditOptionForm(JComponent parent, OptionDTO currentOption) {
        super("eCare");

        this.currentOption = currentOption;
        this.parent = parent;

        setContentPane(backgroundPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        addCancelButtonListener();
        addSaveButtonListener();
        addDataChangingButtonsListeners();

        dependentOptionsTable.setRowHeight(25);
        incompatibleOptionsTable.setRowHeight(25);
        possibleTariffsTable.setRowHeight(25);

        dependentOptionsTable.setRowSelectionAllowed(true);
        incompatibleOptionsTable.setRowSelectionAllowed(true);
        possibleTariffsTable.setRowSelectionAllowed(true);

        setupAllFields();

        setVisible(true);
    }

    private void setupAllFields() {
        titleField.setText(currentOption.getTitle());
        descriptionTextArea.setText(currentOption.getDescription());
        priceField.setText(currentOption.getPrice().toString());
        connectionCostField.setText(currentOption.getConnectionCost().toString());

        addedIncompatibleOptions = new TreeSet<OptionDTO>();
        addedDependentOptions = new TreeSet<OptionDTO>();
        addedPossibleTariffs = new TreeSet<TariffDTO>();

        removedIncompatibleOptions = new TreeSet<OptionDTO>();
        removedDependentOptions = new TreeSet<OptionDTO>();
        removedPossibleTariffs = new TreeSet<TariffDTO>();

        currentIncompatibleOptions = OptionsController.getInstance().getIncompatibleOptions(currentOption);
        currentDependentOptions = OptionsController.getInstance().getDependentOptions(currentOption);
        currentPossibleTariffs = OptionsController.getInstance().getPossibleTariffs(currentOption);

        updateTables();
    }

    public void updateTables() {
        Set<OptionDTO> incOptions = new TreeSet<OptionDTO>();
        Set<OptionDTO> depOptions = new TreeSet<OptionDTO>();
        Set<TariffDTO> posTariffs = new TreeSet<TariffDTO>();

        incOptions.addAll(currentIncompatibleOptions);
        incOptions.addAll(addedIncompatibleOptions);
        incOptions.removeAll(removedIncompatibleOptions);

        depOptions.addAll(currentDependentOptions);
        depOptions.addAll(addedDependentOptions);
        depOptions.removeAll(removedDependentOptions);

        posTariffs.addAll(currentPossibleTariffs);
        posTariffs.addAll(addedPossibleTariffs);
        posTariffs.removeAll(removedPossibleTariffs);

        incompatibleOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(incOptions)
                )
        );
        dependentOptionsTable.setModel(
                OptionsController.getInstance().getShortOptionsTableModelFromList(
                        new ArrayList<OptionDTO>(depOptions)
                )
        );

        possibleTariffsTable.setModel(
                TariffsController.getInstance().getShortTariffsTableModelFromList(
                        new ArrayList<TariffDTO>(posTariffs)
                )
        );
    }

    private void addSaveButtonListener() {
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    OptionDTO changedOption = new OptionDTO(
                            currentOption.getId(),
                            titleField.getText(),
                            descriptionTextArea.getText(),
                            Float.valueOf(priceField.getText()),
                            Float.valueOf(connectionCostField.getText())
                    );

                    try {
                        OptionsController.getInstance().updateOption(
                                changedOption,
                                new ArrayList<OptionDTO>(addedIncompatibleOptions),
                                new ArrayList<OptionDTO>(addedDependentOptions),
                                new ArrayList<TariffDTO>(addedPossibleTariffs),
                                new ArrayList<OptionDTO>(removedIncompatibleOptions),
                                new ArrayList<OptionDTO>(removedDependentOptions),
                                new ArrayList<TariffDTO>(removedPossibleTariffs)
                        );
                        JOptionPane.showMessageDialog(null, "Option has been updated", "Success", JOptionPane.INFORMATION_MESSAGE);
                    } catch (IllegalArgumentException e) {
                        JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Value of \"Price\" or \"Connsection cost\" is incorrect.\n" +
                                    "Fill this fields in next format: 5.30",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void addCancelButtonListener() {
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    @Override
    public void dispose() {
        if (parent != null) parent.setVisible(true);
        super.dispose();
    }

    private void addDataChangingButtonsListeners() {
        addIncompatibleOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                final TableModel tableModel;
                final SelectingForm selectingForm;
                final List<OptionDTO> optionsList;

                optionsList = OptionsController.getInstance().getAllOptions();
                tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
                selectingForm = new SelectingForm(EditOptionForm.this, tableModel);

                ActionListener listener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Long selectedOptionId;
                        selectedOptionId = selectingForm.getSelectedRowId();

                        if (selectedOptionId != null) {
                            OptionDTO selectedOption = null;
                            for (OptionDTO option : optionsList) {
                                if (option.getId().equals(selectedOptionId)) {
                                    selectedOption = option;
                                    break;
                                }
                            }
                            addedIncompatibleOptions.add(selectedOption);
                            updateTables();
                        }
                    }
                };

                selectingForm.setSelectButtonListener(listener);
            }
        });

        addDependentOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                final TableModel tableModel;
                final SelectingForm selectingForm;
                final List<OptionDTO> optionsList;

                optionsList = OptionsController.getInstance().getAllOptions();
                tableModel = OptionsController.getInstance().getFullOptionsTableModelFromList(optionsList);
                selectingForm = new SelectingForm(EditOptionForm.this, tableModel);

                ActionListener listener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Long selectedOptionId;
                        selectedOptionId = selectingForm.getSelectedRowId();

                        if (selectedOptionId != null) {
                            OptionDTO selectedOption = null;
                            for (OptionDTO option : optionsList) {
                                if (option.getId().equals(selectedOptionId)) {
                                    selectedOption = option;
                                    break;
                                }
                            }
                            addedDependentOptions.add(selectedOption);
                            updateTables();
                        }
                    }
                };

                selectingForm.setSelectButtonListener(listener);
            }
        });

        addPossibleTariffButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                final TableModel tableModel;
                final SelectingForm selectingForm;
                final List<TariffDTO> tariffsList;

                tariffsList = TariffsController.getInstance().getAllTariffs();
                tableModel = TariffsController.getInstance().getFullTariffsTableModelFromList(tariffsList);
                selectingForm = new SelectingForm(EditOptionForm.this, tableModel);

                ActionListener listener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Long selectedOptionId;
                        selectedOptionId = selectingForm.getSelectedRowId();

                        if (selectedOptionId != null) {
                            TariffDTO selectedTariff = null;
                            for (TariffDTO option : tariffsList) {
                                if (option.getId().equals(selectedOptionId)) {
                                    selectedTariff = option;
                                    break;
                                }
                            }
                            addedPossibleTariffs.add(selectedTariff);
                            updateTables();
                            selectingForm.dispose();
                        }
                    }
                };

                selectingForm.setSelectButtonListener(listener);
            }
        });

        removeSelectedIncompatibleOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if(incompatibleOptionsTable.getRowCount() == 0)
                    return;

                int selectedRow;
                selectedRow = incompatibleOptionsTable.getSelectedRow();
                if (selectedRow == -1)
                    return;

                Long selectedOptionId = Long.valueOf(
                        incompatibleOptionsTable.getModel().getValueAt(
                                incompatibleOptionsTable.convertRowIndexToModel(selectedRow), 0
                        ).toString()
                );

                OptionDTO selectedOption = null;
                for (OptionDTO option : currentIncompatibleOptions) {
                    if (option.getId().equals(selectedOptionId)) {
                        selectedOption = option;
                        break;
                    }
                }

                removedIncompatibleOptions.add(selectedOption);
                updateTables();
            }
        });

        removeSelectedDependentOptionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if(dependentOptionsTable.getRowCount() == 0)
                    return;

                int selectedRow;
                selectedRow = dependentOptionsTable.getSelectedRow();

                Long selectedOptionId = Long.valueOf(
                        dependentOptionsTable.getModel().getValueAt(
                                dependentOptionsTable.convertRowIndexToModel(selectedRow), 0
                        ).toString()
                );

                OptionDTO selectedOption = null;
                for (OptionDTO option : currentDependentOptions) {
                    if (option.getId().equals(selectedOptionId)) {
                        selectedOption = option;
                        break;
                    }
                }

                removedDependentOptions.add(selectedOption);
                updateTables();
            }
        });

        removeSelectedPossibleTariffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if(possibleTariffsTable.getRowCount() == 0)
                    return;

                int selectedRow;
                selectedRow = possibleTariffsTable.getSelectedRow();

                Long selectedTariffId = Long.valueOf(
                        possibleTariffsTable.getModel().getValueAt(
                                possibleTariffsTable.convertRowIndexToModel(selectedRow), 0
                        ).toString()
                );

                TariffDTO selectedTariff = null;
                for (TariffDTO tariff : currentPossibleTariffs) {
                    if (tariff.getId().equals(selectedTariffId)) {
                        selectedTariff = tariff;
                        break;
                    }
                }

                removedPossibleTariffs.add(selectedTariff);
                updateTables();
            }
        });
    }
}
