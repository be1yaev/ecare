package com.tsystems.ecare.client.logic;

import com.tsystems.ecare.client.Connector;
import com.tsystems.ecare.common.command.request.*;
import com.tsystems.ecare.common.command.response.*;
import com.tsystems.ecare.common.dto.OptionDTO;
import com.tsystems.ecare.common.dto.TariffDTO;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class OptionsModel {
    private static OptionsModel modelInstance = null;

    public synchronized static OptionsModel getInstance() {
        if (modelInstance == null)
            modelInstance = new OptionsModel();
        return modelInstance;
    }

    private static final Logger logger = Logger.getLogger(OptionsModel.class.getName());

    private OptionsModel() {

    }

    /**
     *
     * @return
     */
    public TableModel getAllOptionsTableModel() {
        return getTableModelFromOptionsList(getAllOptions(), false);
    }

    /**
     *
     * @param optionsList
     * @return
     */
    public TableModel getTableModelFromOptionsList(List<OptionDTO> optionsList, boolean shortView) {
        final String[] columns;
        final List<OptionDTO> data;

        data = optionsList;
        columns = ((shortView) ?
                new String[] {
                        "Id",
                        "Title",
                } : new String[] {
                        "Id",
                        "Title",
                        "Price",
                        "Connection cost",
                        "Description",
                }
        );

        TableModel tableModel = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return columns.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                OptionDTO option = data.get(row);
                switch (col) {
                    case 0:
                        // return String.valueOf(row+1);
                        return option.getId().toString();
                    case 1:
                        return option.getTitle();
                    case 2:
                        return option.getDescription();
                    case 3:
                        return option.getPrice().toString();
                    case 4:
                        return option.getConnectionCost().toString();
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columns[column];
            }

            @Override
            public Class getColumnClass(int c) {
                return (String.class);
            }
        };

        return tableModel;
    }

    /**
     *
     * @param option
     * @return
     * @throws IllegalArgumentException
     */
    public OptionDTO createNewOption(OptionDTO option) throws IllegalArgumentException{
        CreateOptionCommand request = new CreateOptionCommand(option);
        CreateOptionConfirmationCommand response =
                (CreateOptionConfirmationCommand) Connector.getInstance().forwardCommandToServer(request);

        if (response.getNewOption() == null)
            throw new IllegalArgumentException(response.getMessage());

        return response.getNewOption();
    }

    /**
     *
     * @param option
     * @return
     */
    public List<OptionDTO> getIncompatibleOptions(OptionDTO option) {
        GetIncompatibleOptionsListCommand request = new GetIncompatibleOptionsListCommand(option);
        OptionsListCommand response = (OptionsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getOptions();
    }

    /**
     *
     * @param option
     * @return
     */
    public List<OptionDTO> getDependentOptions(OptionDTO option) {
        GetDependentOptionsListCommand request = new GetDependentOptionsListCommand(option);
        OptionsListCommand response = (OptionsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getOptions();
    }

    public List<TariffDTO> getPossibleTariffs(OptionDTO option) {
        GetPossibleTariffsListCommand request = new GetPossibleTariffsListCommand(option);
        TariffsListCommand response = (TariffsListCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getTariffs();
    }

    /**
     *
     * @param optionId
     * @return
     */
    public OptionDTO getOptionById(Long optionId) {
        GetOptionByIdCommand request = new GetOptionByIdCommand(optionId);
        GetOptionResultCommand response = (GetOptionResultCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getOption();
    }

    public boolean updateOption(OptionDTO changedOption,
                                List<OptionDTO> addedIncompatibleOptions,
                                List<OptionDTO> addedDependentOptions,
                                List<TariffDTO> addedPossibleTariffs,
                                List<OptionDTO> removedIncompatibleOptions,
                                List<OptionDTO> removedDependentOptions,
                                List<TariffDTO> removedPossibleTariffs) {

        UpdateOptionCommand request = new UpdateOptionCommand(
                changedOption,
                addedIncompatibleOptions,
                addedDependentOptions,
                addedPossibleTariffs,
                removedIncompatibleOptions,
                removedDependentOptions,
                removedPossibleTariffs
        );
        UpdateResultCommand response = (UpdateResultCommand)
                Connector.getInstance().forwardCommandToServer(request);

        return response.getUpdatedSuccessfully();
    }

    public List<OptionDTO> getAllOptions() {
        // Get Options list for all users
        GetOptionsListCommand request = new GetOptionsListCommand();
        OptionsListCommand response = (OptionsListCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getOptions();
    }

    public boolean deleteOption(Long optionId) {
        DeleteOptionCommand request = new DeleteOptionCommand(optionId);
        DeleteResultCommand response = (DeleteResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getDeletedSuccessfully();
    }
}
