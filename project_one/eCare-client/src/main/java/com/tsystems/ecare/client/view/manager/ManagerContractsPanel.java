package com.tsystems.ecare.client.view.manager;

import com.tsystems.ecare.client.controller.ContractsController;
import com.tsystems.ecare.client.controller.UsersController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 23.08.2014.
 */
public class ManagerContractsPanel extends JPanel {
    private JPanel mainPanel;
    private JTable contractsTable;
    private JButton refreshButton;
    private JButton newContractButton;
    private JTextField phoneNumberField;
    private JButton changeButton;
    private JButton deleteSelectedContractButton;
    private JButton showSelectedContractClientButton;

    public ManagerContractsPanel() {
        super();

        addRefreshButtonListener();
        addChangeButtonListener();
        addNewContractButtonListener();
        addDeleteButtonListener();
        addShowSelectedContractButtonListener();

        contractsTable.setRowHeight(25);
        contractsTable.setRowSelectionAllowed(true);

        setVisible(false);
    }

    public void updateContractsTable() {
        if (!phoneNumberField.getText().equals("")) {
            contractsTable.setModel(ContractsController.getInstance().getAllContractsWithTelephoneTableModel(
                            phoneNumberField.getText())
            );
        } else {
            contractsTable.setModel(ContractsController.getInstance().getAllContractsTableModel());
        }
    }

    private Long getSelectedRowId() {
        if(contractsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = contractsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        return Long.valueOf(
                contractsTable.getModel().getValueAt(
                        contractsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );
    }

    private void addDeleteButtonListener() {
        deleteSelectedContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();
                if (selectedRowId != null) {
                    int result = JOptionPane.showConfirmDialog(
                            null,
                            "Are you sure you want to remove the contract with the Id " + selectedRowId + "?",
                            "Removing confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE
                    );
                    if (result == JOptionPane.YES_OPTION) {
                        if (ContractsController.getInstance().deleteContract(selectedRowId)) {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Contract has been deleted",
                                    "Success",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(
                                    null,
                                    "Contract can not be deleted",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
                        }

                        updateContractsTable();
                    }
                }
            }
        });
    }

    private void addNewContractButtonListener() {
        newContractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                ContractsController.getInstance().showCreateNewContractManagerWindow(ManagerContractsPanel.this);
            }
        });
    }

    private void addChangeButtonListener() {
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();

                if (selectedRowId != null) {
                    ContractsController.getInstance().showEditingOptionWindow(selectedRowId, ManagerContractsPanel.this);
                }
            }
        });
    }

    private void addShowSelectedContractButtonListener() {
        showSelectedContractClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                Long selectedRowId;
                selectedRowId = getSelectedRowId();

                if (selectedRowId != null) {
                    UsersController.getInstance().showUserInfo(
                            null,
                            ContractsController.getInstance().getClientByContractId(selectedRowId)
                    );
                }
            }
        });
    }

    private void addRefreshButtonListener() {
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                updateContractsTable();
            }
        });
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mainPanel.setVisible(aFlag);

        if (aFlag) updateContractsTable();
    }
}
