package com.tsystems.ecare.client.logic;

import com.tsystems.ecare.client.Connector;
import com.tsystems.ecare.common.command.request.GetUserByIdCommand;
import com.tsystems.ecare.common.command.request.GetUsersListCommand;
import com.tsystems.ecare.common.command.request.UserRegistrationCommand;
import com.tsystems.ecare.common.command.request.UserVerificationCommand;
import com.tsystems.ecare.common.command.response.GetUserResultCommand;
import com.tsystems.ecare.common.command.response.LoginConfirmationCommand;
import com.tsystems.ecare.common.command.response.UserRegistrationConfirmationCommand;
import com.tsystems.ecare.common.command.response.UsersListCommand;
import com.tsystems.ecare.common.dto.UserDTO;
import org.apache.log4j.Logger;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by Andrew on 22.08.2014.
 */
public class UsersModel {
    private static UsersModel modelInstance = null;

    public synchronized static UsersModel getInstance() {
        if (modelInstance == null)
            modelInstance = new UsersModel();
        return modelInstance;
    }

    private static final Logger logger = Logger.getLogger(UsersModel.class.getName());
    private UserDTO currentUser;

    private UsersModel() {
    }

    /**
     *
     * @param userEmail
     * @param userPassword
     * @return
     * @throws IllegalArgumentException
     */
    public boolean login(String userEmail, String userPassword) throws IllegalArgumentException {
        if (userEmail.equals(""))
            throw new IllegalArgumentException("Login is empty");

        if (userPassword.equals(""))
            throw new IllegalArgumentException("Password is empty");

        if (userEmail.contains(" "))
            throw new IllegalArgumentException("Incorrect symbols in email");

        if (userPassword.contains(" "))
            throw new IllegalArgumentException("Incorrect symbols in password");


        UserVerificationCommand request = new UserVerificationCommand(
                userEmail, userPassword
        );

        LoginConfirmationCommand response = (LoginConfirmationCommand)
                Connector.getInstance().forwardCommandToServer(request);

        if (response != null && response.isLoginConfirm()) {
            this.currentUser = response.getUser();
            return true;
        }

        return false;
    }

    /**
     *
     * @param userInfo
     * @param password
     * @return
     */
    public boolean registration(UserDTO userInfo, String password) {
        UserRegistrationCommand request = new UserRegistrationCommand(userInfo, password);
        UserRegistrationConfirmationCommand response =
                (UserRegistrationConfirmationCommand) Connector.getInstance().forwardCommandToServer(request);

        if (response.isRegistrationConfirm())
            return true;
        return false;
    }

    public UserDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserDTO user) {
        this.currentUser = user;
    }

    /**
     *
     * @return
     */
    public TableModel getFullUsersTableModel() {
        return getFullUsersTableModelFromList(getAllUsers());
    }
    /**
     *
     * @return
     */
    public TableModel getTableModelFromUsersList(List<UserDTO> clientsList, boolean shortView) {
        final String[] columns;
        final List<UserDTO> data;

        data = clientsList;
        columns = ((shortView) ?
                new String[] {
                        "Id",
                        "Firstname",
                        "Surname",
                        "Patronymic",
                        "Email",
                        "PassportNumber",
                } : new String[] {
                        "Id",
                        "Firstname",
                        "Surname",
                        "Patronymic",
                        "Email",
                        "PassportNumber",
                        "PassportDescription",
                        "Country",
                        "City",
                        "Address",
                        "Role"
                }
        );

        TableModel allUsers = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return columns.length;
            }

            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public Object getValueAt(int row, int col) {
                UserDTO user = data.get(row);
                switch (col) {
                    case 0:
                        return user.getId().toString();
                    case 1:
                        return user.getFirstname();
                    case 2:
                        return user.getSurname();
                    case 3:
                        return user.getPatronymic();
                    case 4:
                    return user.getEmail();
                    case 5:
                        return user.getPassportNumber();
                    case 6:
                        return user.getPassportDescription();
                    case 7:
                        return user.getCountry();
                    case 8:
                        return user.getCity();
                    case 9:
                        return user.getAddress();
                    case 10:
                        return user.getRole().toString();
                    default:
                        return "";
                }
            }

            @Override
            public String getColumnName(int column) {
                return columns[column];
            }

            @Override
            public Class getColumnClass(int c) {
                return (String.class);
            }
        };

        return allUsers;
    }

    /**
     *
     * @return
     */
    public List<UserDTO> getAllUsers() {
        // Get users list
        GetUsersListCommand request = new GetUsersListCommand();
        UsersListCommand response = (UsersListCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getUsers();
    }

    /**
     *
     * @param clientsList
     * @return
     */
    public TableModel getFullUsersTableModelFromList(List<UserDTO> clientsList) {
        return getTableModelFromUsersList(clientsList, false);
    }

    /**
     *
     * @param clientsList
     * @return
     */
    public TableModel getShortUsersTableModelFromList(List<UserDTO> clientsList) {
        return getTableModelFromUsersList(clientsList, true);
    }

    public String getShortUserNameFromUser(UserDTO userInfo) {
        return userInfo.getSurname() + " " +
                userInfo.getFirstname().substring(0, 1) + ". " +
                ((userInfo.getPatronymic() != null && userInfo.getPatronymic().length() > 0)
                        ? userInfo.getPatronymic().substring(0, 1) + "."
                        : "");
    }

    public UserDTO getUserById(Long id) {
        GetUserByIdCommand request = new GetUserByIdCommand(id);
        GetUserResultCommand response = (GetUserResultCommand)
                Connector.getInstance().forwardCommandToServer(request);
        return response.getUser();
    }
}
