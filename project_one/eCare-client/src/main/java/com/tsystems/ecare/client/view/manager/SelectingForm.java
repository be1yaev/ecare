package com.tsystems.ecare.client.view.manager;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andrew on 24.08.2014.
 */
public class SelectingForm extends JFrame {

    private JTable optionsTable;
    private JButton cancelButton;
    private JButton selectButton;
    private JPanel mainPanel;

    private JFrame parent;

    public SelectingForm(JFrame parent, TableModel tableModel) {
        super("eCare");

        this.parent = parent;

        setContentPane(mainPanel);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        addCancelButtonListener();

        optionsTable.setModel(tableModel);
        optionsTable.setRowHeight(25);
        optionsTable.setRowSelectionAllowed(true);

        setVisible(true);
    }

    public Long getSelectedRowId() {
        if(optionsTable.getRowCount() == 0)
            return null;

        int selectedRow;
        selectedRow = optionsTable.getSelectedRow();
        if (selectedRow == -1)
            return null;

        Long selectedOptionId = Long.valueOf(
                optionsTable.getModel().getValueAt(
                        optionsTable.convertRowIndexToModel(selectedRow), 0
                ).toString()
        );

        if (parent != null)
            parent.setVisible(true);
        dispose();

        return selectedOptionId;
    }

    public void setSelectButtonListener(ActionListener listener) {
        selectButton.addActionListener(listener);
    }

    private void addCancelButtonListener() {
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
    }

    @Override
    public void dispose() {
        if (parent != null)
            parent.setVisible(true);
        super.dispose();
    }
}
