-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Авг 22 2014 г., 05:14
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ecare`
--

-- --------------------------------------------------------

--
-- Структура таблицы `relation_contract_and_options`
--

CREATE TABLE IF NOT EXISTS `relation_contract_and_options` (
  `contract_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  PRIMARY KEY (`contract_id`,`option_id`),
  KEY `option_id` (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `relation_dependent_options`
--

CREATE TABLE IF NOT EXISTS `relation_dependent_options` (
  `option_id` int(11) NOT NULL,
  `dependent_option_id` int(11) NOT NULL,
  PRIMARY KEY (`option_id`,`dependent_option_id`),
  KEY `dependent_option_id` (`dependent_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `relation_incompatible_options`
--

CREATE TABLE IF NOT EXISTS `relation_incompatible_options` (
  `option_id` int(11) NOT NULL,
  `incompatible_option_id` int(11) NOT NULL,
  PRIMARY KEY (`option_id`,`incompatible_option_id`),
  KEY `incompatible_option_id` (`incompatible_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `relation_tariff_and_possible_options`
--

CREATE TABLE IF NOT EXISTS `relation_tariff_and_possible_options` (
  `tariff_id` int(11) NOT NULL,
  `possible_option_id` int(11) NOT NULL,
  PRIMARY KEY (`tariff_id`,`possible_option_id`),
  KEY `possible_option_id` (`possible_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `table_contract`
--

CREATE TABLE IF NOT EXISTS `table_contract` (
  `contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `tariff_id` int(11) DEFAULT NULL,
  `blocked_by_client` tinyint(1) NOT NULL DEFAULT '0',
  `blocked_by_manager` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contract_id`),
  UNIQUE KEY `phone_number` (`phone_number`),
  KEY `user_id` (`user_id`),
  KEY `tariff_id` (`tariff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Структура таблицы `table_option`
--

CREATE TABLE IF NOT EXISTS `table_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `connection_cost` float DEFAULT NULL,
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Структура таблицы `table_tariff`
--

CREATE TABLE IF NOT EXISTS `table_tariff` (
  `tariff_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`tariff_id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Структура таблицы `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(25) DEFAULT NULL,
  `name` varchar(25) DEFAULT NULL,
  `patronymic` varchar(25) DEFAULT NULL,
  `passport_number` varchar(20) DEFAULT NULL,
  `passport_description` varchar(50) DEFAULT NULL,
  `country` varchar(25) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` enum('MANAGER','CLIENT','ADMIN') NOT NULL DEFAULT 'CLIENT',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `passport_number` (`passport_number`),
  UNIQUE KEY `passport_number_2` (`passport_number`),
  UNIQUE KEY `passport_number_3` (`passport_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `table_user`
--

INSERT INTO `table_user` (`user_id`, `surname`, `name`, `patronymic`, `passport_number`, `passport_description`, `country`, `city`, `address`, `email`, `password`, `role`) VALUES
(17, 'Belyaev', 'Andrey', 'Aleksandrovich', '1234567890', 'Passport RF', 'Russia', 'SPb', 'Torzhkovskaya st., 15 h.', 'admin', 'admin', 'ADMIN'),
(19, 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'CLIENT');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `relation_contract_and_options`
--
ALTER TABLE `relation_contract_and_options`
  ADD CONSTRAINT `relation_contract_and_options_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `table_contract` (`contract_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relation_contract_and_options_ibfk_2` FOREIGN KEY (`option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `relation_dependent_options`
--
ALTER TABLE `relation_dependent_options`
  ADD CONSTRAINT `relation_dependent_options_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relation_dependent_options_ibfk_2` FOREIGN KEY (`dependent_option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `relation_incompatible_options`
--
ALTER TABLE `relation_incompatible_options`
  ADD CONSTRAINT `relation_incompatible_options_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relation_incompatible_options_ibfk_2` FOREIGN KEY (`incompatible_option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `relation_tariff_and_possible_options`
--
ALTER TABLE `relation_tariff_and_possible_options`
  ADD CONSTRAINT `relation_tariff_and_possible_options_ibfk_1` FOREIGN KEY (`tariff_id`) REFERENCES `table_tariff` (`tariff_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relation_tariff_and_possible_options_ibfk_2` FOREIGN KEY (`possible_option_id`) REFERENCES `table_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `table_contract`
--
ALTER TABLE `table_contract`
  ADD CONSTRAINT `table_contract_ibfk_1` FOREIGN KEY (`tariff_id`) REFERENCES `table_tariff` (`tariff_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `table_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
